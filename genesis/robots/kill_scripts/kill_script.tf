;
; Shared file for kill scripts
;

; variables

/set kill_target none
/set kill_script_step 0
/set kill_script_total_steps 0
/set kill_script_running 0
/set room_occupied 0
/set low_health 0

; definitions

/def kill_script_reset = \
  /set kill_target none %;\
  /set kill_script_step 0 %;\
  /set kill_script_running 0 %;\
  /e @ kill script reset

/def kill_script_run = \
  /e @ kill script running %;\
  /set kill_script_running 1 %;\
  /check_next_step

/def kill_script_stop = \
  /set kill_script_running 0 %;\
  /e @ kill script stopped

/def kill_script_advance_step = \
  /set kill_script_step $[kill_script_step + 1]

/def check_next_step = \
  /if (kill_script_running) \
    /kill_script_advance_step %;\
    /e @ kill script step %kill_script_step out of %kill_script_total_steps %;\
	/kill_script_continue %;\
	/repeat -2 1 /kill_script_target_check %;\
  /endif

/def kill_script_target_check = \
  /if (room_occupied) \
    /e @ room occupied - moving on %;\
    /set room_occupied 0 %;\
	/check_next_step %;\
  /else \
    k %kill_target %;\
  /endif
 
; triggers

/def -F -mregexp -t'fighting each other' someone_fighting = /set room_occupied 1

/def -F -mregexp -t'You find no such living creature\.$' no_more_targets = \
    /if (kill_script_running) \
        /check_next_step %;\
    /endif

/def -F -mregexp -t'^A dark (area|room)\.$' dark_room = \
    /if (kill_script_running) \
        /check_next_step %;\
    /endif

/def -F -mregexp -t'Your legs run away' panicked = \
    /if (kill_script_running) \
        kstop %;\
    /endif

/def -F -mregexp -t'^You are physically (very hurt|in a bad shape|in agony)' low_health_pause = \
    /set low_health 1 %;\
    kstop

/def -F -mregexp -t'^You are physically feeling very well' low_health_resume = \
    /if (low_health) \
        /set low_health 0 %;\
        kstart %;\
    /endif

; aliases

/alias kstart /kill_script_run
/alias kstop /kill_script_stop
/alias kreset /kill_script_reset

; loading scripts

/alias kgtower \
    /set kill_target guard %;\
    /set atk 1 %;\
    /load ~/scripts/genesis/robots/kill_scripts/gont_tower.tf %;\
    /e @ Gont guard tower started %;\
    /repeat -1 1 kstart

/alias kkcamp \
    /set kill_target warrior %;\
    /set atk 1 %;\
    /load ~/scripts/genesis/robots/kill_scripts/karg_camp.tf %;\
    /e @ Gont karg camp started %;\
    /repeat -1 1 kstart

/alias kdcamp \
    /set kill_target dwarf %;\
    /set atk 1 %;\
    /load ~/scripts/genesis/robots/kill_scripts/dwarf_camp.tf %;\
    /e @ Krynn dwarf camp started %;\
    /repeat -1 1 kstart

/alias kdelves \
    /set kill_target dark elf %;\
    /set atk 1 %;\
    /load ~/scripts/genesis/robots/kill_scripts/dark_elves.tf %;\
    /e @ Terel dark elves started %;\
    /repeat -1 1 kstart

/alias kshaws \
    /set kill_target troll %;\
    /set atk 1 %;\
    /load ~/scripts/genesis/robots/kill_scripts/troll_shaws.tf %;\
    /e @ Troll Shaws started %;\
    /repeat -1 1 kstart
