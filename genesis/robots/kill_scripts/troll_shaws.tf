;
; Troll Shaws
;

/set kill_script_total_steps 123

/def kill_script_continue = \
    /if (kill_script_step == 1) \
        w%;\
    /elseif (kill_script_step == 2) \
        w%;\
    /elseif (kill_script_step == 3) \
        s%;\
    /elseif (kill_script_step == 4) \
        sw%;\
    /elseif (kill_script_step == 5) \
        e%;\
    /elseif (kill_script_step == 6) \
        w%;\
    /elseif (kill_script_step == 7) \
        w%;\
    /elseif (kill_script_step == 8) \
        n%;\
    /elseif (kill_script_step == 9) \
        n%;\
    /elseif (kill_script_step == 10) \
        w%;\
    /elseif (kill_script_step == 11) \
        sw%;\
    /elseif (kill_script_step == 12) \
        sw%;\
    /elseif (kill_script_step == 13) \
        se%;\
    /elseif (kill_script_step == 14) \
        nw%;\
    /elseif (kill_script_step == 15) \
        nw%;\
    /elseif (kill_script_step == 16) \
        nw%;\
    /elseif (kill_script_step == 17) \
        w%;\
    /elseif (kill_script_step == 18) \
        ne%;\
    /elseif (kill_script_step == 19) \
        e%;\
    /elseif (kill_script_step == 20) \
        e%;\
    /elseif (kill_script_step == 21) \
        se%;\
    /elseif (kill_script_step == 22) \
        nw%;\
    /elseif (kill_script_step == 23) \
        w%;\
    /elseif (kill_script_step == 24) \
        w%;\
    /elseif (kill_script_step == 25) \
        n%;\
    /elseif (kill_script_step == 26) \
        nw%;\
    /elseif (kill_script_step == 27) \
        s%;\
    /elseif (kill_script_step == 28) \
        n%;\
    /elseif (kill_script_step == 29) \
        w%;\
    /elseif (kill_script_step == 30) \
        e%;\
    /elseif (kill_script_step == 31) \
        ne%;\
    /elseif (kill_script_step == 32) \
        e%;\
    /elseif (kill_script_step == 33) \
        ne%;\
    /elseif (kill_script_step == 34) \
        e%;\
    /elseif (kill_script_step == 35) \
        e%;\
    /elseif (kill_script_step == 36) \
        se%;\
    /elseif (kill_script_step == 37) \
        n%;\
    /elseif (kill_script_step == 38) \
        s%;\
    /elseif (kill_script_step == 39) \
        sw%;\
    /elseif (kill_script_step == 40) \
        sw%;\
    /elseif (kill_script_step == 41) \
        w%;\
    /elseif (kill_script_step == 42) \
        e%;\
    /elseif (kill_script_step == 43) \
        ne%;\
    /elseif (kill_script_step == 44) \
        ne%;\
    /elseif (kill_script_step == 45) \
        e%;\
    /elseif (kill_script_step == 46) \
        ne%;\
    /elseif (kill_script_step == 47) \
        e%;\
    /elseif (kill_script_step == 48) \
        se%;\
    /elseif (kill_script_step == 49) \
        n%;\
    /elseif (kill_script_step == 50) \
        s%;\
    /elseif (kill_script_step == 51) \
        s%;\
    /elseif (kill_script_step == 52) \
        w%;\
    /elseif (kill_script_step == 53) \
        sw%;\
    /elseif (kill_script_step == 54) \
        s%;\
    /elseif (kill_script_step == 55) \
        w%;\
    /elseif (kill_script_step == 56) \
        w%;\
    /elseif (kill_script_step == 57) \
        n%;\
    /elseif (kill_script_step == 58) \
        n%;\
    /elseif (kill_script_step == 59) \
        e%;\
    /elseif (kill_script_step == 60) \
        w%;\
    /elseif (kill_script_step == 61) \
        s%;\
    /elseif (kill_script_step == 62) \
        s%;\
    /elseif (kill_script_step == 63) \
        e%;\
    /elseif (kill_script_step == 64) \
        e%;\
    /elseif (kill_script_step == 65) \
        e%;\
    /elseif (kill_script_step == 66) \
        e%;\
    /elseif (kill_script_step == 67) \
        e%;\
    /elseif (kill_script_step == 68) \
        e%;\
    /elseif (kill_script_step == 69) \
        se%;\
    /elseif (kill_script_step == 70) \
        n%;\
    /elseif (kill_script_step == 71) \
        s%;\
    /elseif (kill_script_step == 72) \
        s%;\
    /elseif (kill_script_step == 73) \
        n%;\
    /elseif (kill_script_step == 74) \
        e%;\
    /elseif (kill_script_step == 75) \
        e%;\
    /elseif (kill_script_step == 76) \
        nw%;\
    /elseif (kill_script_step == 77) \
        nw%;\
    /elseif (kill_script_step == 78) \
        w%;\
    /elseif (kill_script_step == 79) \
        w%;\
    /elseif (kill_script_step == 80) \
        n%;\
    /elseif (kill_script_step == 81) \
        e%;\
    /elseif (kill_script_step == 82) \
        w%;\
    /elseif (kill_script_step == 83) \
        ne%;\
    /elseif (kill_script_step == 84) \
        e%;\
    /elseif (kill_script_step == 85) \
        e%;\
    /elseif (kill_script_step == 86) \
        s%;\
    /elseif (kill_script_step == 87) \
        e%;\
    /elseif (kill_script_step == 88) \
        w%;\
    /elseif (kill_script_step == 89) \
        se%;\
    /elseif (kill_script_step == 90) \
        e%;\
    /elseif (kill_script_step == 91) \
        sw%;\
    /elseif (kill_script_step == 92) \
        s%;\
    /elseif (kill_script_step == 93) \
        nw%;\
    /elseif (kill_script_step == 94) \
        nw%;\
    /elseif (kill_script_step == 95) \
        w%;\
    /elseif (kill_script_step == 96) \
        w%;\
    /elseif (kill_script_step == 97) \
        n%;\
    /elseif (kill_script_step == 98) \
        ne%;\
    /elseif (kill_script_step == 99) \
        ne%;\
    /elseif (kill_script_step == 100) \
        e%;\
    /elseif (kill_script_step == 101) \
        e%;\
    /elseif (kill_script_step == 102) \
        e%;\
    /elseif (kill_script_step == 103) \
        se%;\
    /elseif (kill_script_step == 104) \
        s%;\
    /elseif (kill_script_step == 105) \
        s%;\
    /elseif (kill_script_step == 106) \
        s%;\
    /elseif (kill_script_step == 107) \
        w%;\
    /elseif (kill_script_step == 108) \
        e%;\
    /elseif (kill_script_step == 109) \
        sw%;\
    /elseif (kill_script_step == 110) \
        sw%;\
    /elseif (kill_script_step == 111) \
        e%;\
    /elseif (kill_script_step == 112) \
        w%;\
    /elseif (kill_script_step == 113) \
        w%;\
    /elseif (kill_script_step == 114) \
        nw%;\
    /elseif (kill_script_step == 115) \
        w%;\
    /elseif (kill_script_step == 116) \
        sw%;\
    /elseif (kill_script_step == 117) \
        sw%;\
    /elseif (kill_script_step == 118) \
        w%;\
    /elseif (kill_script_step == 119) \
        nw%;\
    /elseif (kill_script_step == 120) \
        ne%;\
    /elseif (kill_script_step == 121) \
        n%;\
    /elseif (kill_script_step == 122) \
        e%;\
    /else \
        e%;\
        kstop %;\
        /e @ troll shaws complete %;\
        /kill_script_reset %;\
    /endif

