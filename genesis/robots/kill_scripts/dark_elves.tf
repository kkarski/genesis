;
; Terel dark elves
;

/set kill_script_total_steps 68

/def kill_script_continue = \
    /if (kill_script_step == 1) \
        s%;\
    /elseif (kill_script_step == 2) \
        s%;\
    /elseif (kill_script_step == 3) \
        w%;\
    /elseif (kill_script_step == 4) \
        w%;\
    /elseif (kill_script_step == 5) \
        nw%;\
    /elseif (kill_script_step == 6) \
        se%;\
    /elseif (kill_script_step == 7) \
        w%;\
    /elseif (kill_script_step == 8) \
        e%;\
    /elseif (kill_script_step == 9) \
        s%;\
    /elseif (kill_script_step == 10) \
        s%;\
    /elseif (kill_script_step == 11) \
        s%;\
    /elseif (kill_script_step == 12) \
        s%;\
    /elseif (kill_script_step == 13) \
        w%;\
    /elseif (kill_script_step == 14) \
        w%;\
    /elseif (kill_script_step == 15) \
        e%;\
    /elseif (kill_script_step == 16) \
        e%;\
    /elseif (kill_script_step == 17) \
        e%;\
    /elseif (kill_script_step == 18) \
        e%;\
    /elseif (kill_script_step == 19) \
        e%;\
    /elseif (kill_script_step == 20) \
        e%;\
    /elseif (kill_script_step == 21) \
        e%;\
    /elseif (kill_script_step == 22) \
        e%;\
    /elseif (kill_script_step == 23) \
        e%;\
    /elseif (kill_script_step == 24) \
        w%;\
    /elseif (kill_script_step == 25) \
        w%;\
    /elseif (kill_script_step == 26) \
        s%;\
    /elseif (kill_script_step == 27) \
        w%;\
    /elseif (kill_script_step == 28) \
        w%;\
    /elseif (kill_script_step == 29) \
        s%;\
    /elseif (kill_script_step == 30) \
        s%;\
    /elseif (kill_script_step == 31) \
        se%;\
    /elseif (kill_script_step == 32) \
        se%;\
    /elseif (kill_script_step == 33) \
        se%;\
    /elseif (kill_script_step == 34) \
        s%;\
    /elseif (kill_script_step == 35) \
        n%;\
    /elseif (kill_script_step == 36) \
        e%;\
    /elseif (kill_script_step == 37) \
        e%;\
    /elseif (kill_script_step == 38) \
        ne%;\
    /elseif (kill_script_step == 39) \
        sw%;\
    /elseif (kill_script_step == 40) \
        w%;\
    /elseif (kill_script_step == 41) \
        w%;\
    /elseif (kill_script_step == 42) \
        nw%;\
    /elseif (kill_script_step == 43) \
        nw%;\
    /elseif (kill_script_step == 44) \
        nw%;\
    /elseif (kill_script_step == 45) \
        w%;\
    /elseif (kill_script_step == 46) \
        w%;\
    /elseif (kill_script_step == 47) \
        s%;\
    /elseif (kill_script_step == 48) \
        n%;\
    /elseif (kill_script_step == 49) \
        n%;\
    /elseif (kill_script_step == 50) \
        n%;\
    /elseif (kill_script_step == 51) \
        s%;\
    /elseif (kill_script_step == 52) \
        s%;\
    /elseif (kill_script_step == 53) \
        w%;\
    /elseif (kill_script_step == 54) \
        e%;\
    /elseif (kill_script_step == 55) \
        e%;\
    /elseif (kill_script_step == 56) \
        s%;\
    /elseif (kill_script_step == 57) \
        s%;\
    /elseif (kill_script_step == 58) \
        n%;\
    /elseif (kill_script_step == 59) \
        n%;\
    /elseif (kill_script_step == 60) \
        n%;\
    /elseif (kill_script_step == 61) \
        n%;\
    /elseif (kill_script_step == 62) \
        n%;\
    /elseif (kill_script_step == 63) \
        n%;\
    /elseif (kill_script_step == 64) \
        n%;\
    /elseif (kill_script_step == 65) \
        n%;\
    /elseif (kill_script_step == 66) \
        n%;\
    /elseif (kill_script_step == 67) \
        n%;\
    /else \
        n%;\
        kstop %;\
        /e @ dark elves complete %;\
        /kill_script_reset %;\
    /endif

