;
; Gont karg camp
;

/set kill_script_total_steps 20

/def kill_script_continue = \
    /if (kill_script_step == 1) \
        w%;w%;w%;w%;\
    /elseif (kill_script_step == 2) \
        w%;\
    /elseif (kill_script_step == 3) \
        enter tent%;\
    /elseif (kill_script_step == 4) \
        out%;\
    /elseif (kill_script_step == 5) \
        n%;\
    /elseif (kill_script_step == 6) \
        w%;\
    /elseif (kill_script_step == 7) \
        enter tent%;\
    /elseif (kill_script_step == 8) \
        out%;\
    /elseif (kill_script_step == 9) \
        w%;\
    /elseif (kill_script_step == 10) \
        s%;\
    /elseif (kill_script_step == 11) \
        enter tent%;\
    /elseif (kill_script_step == 12) \
        out%;\
    /elseif (kill_script_step == 13) \
        s%;\
    /elseif (kill_script_step == 14) \
        e%;\
    /elseif (kill_script_step == 15) \
        enter tent%;\
    /elseif (kill_script_step == 16) \
        out%;\
    /elseif (kill_script_step == 17) \
        e%;\
    /elseif (kill_script_step == 18) \
        n%;w%;\
    /elseif (kill_script_step == 19) \
        enter tent%;\
    /else \
        out%;e%;e%;e%;e%;e%;\
        kstop %;\
        /e @ karg camp complete %;\
        /kill_script_reset %;\
    /endif

