;
; Krynn dwarf camp
;

/set kill_script_total_steps 73

/def kill_script_continue = \
    /if (kill_script_step == 1) \
        s%;\
    /elseif (kill_script_step == 2) \
        enter tent%;\
    /elseif (kill_script_step == 3) \
        out%;\
    /elseif (kill_script_step == 4) \
        se%;\
    /elseif (kill_script_step == 5) \
        enter tent%;\
    /elseif (kill_script_step == 6) \
        out%;\
    /elseif (kill_script_step == 7) \
        w%;\
    /elseif (kill_script_step == 8) \
        enter tent%;\
    /elseif (kill_script_step == 9) \
        out%;\
    /elseif (kill_script_step == 10) \
        w%;\
    /elseif (kill_script_step == 11) \
        enter tent%;\
    /elseif (kill_script_step == 12) \
        out%;\
    /elseif (kill_script_step == 13) \
        sw%;\
    /elseif (kill_script_step == 14) \
        enter tent%;\
    /elseif (kill_script_step == 15) \
        out%;\
    /elseif (kill_script_step == 16) \
        e%;\
    /elseif (kill_script_step == 17) \
        enter tent%;\
    /elseif (kill_script_step == 18) \
        out%;\
    /elseif (kill_script_step == 19) \
        e%;\
    /elseif (kill_script_step == 20) \
        enter tent%;\
    /elseif (kill_script_step == 21) \
        out%;\
    /elseif (kill_script_step == 22) \
        e%;\
    /elseif (kill_script_step == 23) \
        enter tent%;\
    /elseif (kill_script_step == 24) \
        out%;\
    /elseif (kill_script_step == 25) \
        e%;\
    /elseif (kill_script_step == 26) \
        enter tent%;\
    /elseif (kill_script_step == 27) \
        out%;\
    /elseif (kill_script_step == 28) \
        se%;\
    /elseif (kill_script_step == 29) \
        enter tent%;\
    /elseif (kill_script_step == 30) \
        out%;\
    /elseif (kill_script_step == 31) \
        w%;\
    /elseif (kill_script_step == 32) \
        enter tent%;\
    /elseif (kill_script_step == 33) \
        out%;\
    /elseif (kill_script_step == 34) \
        w%;\
    /elseif (kill_script_step == 35) \
        enter tent%;\
    /elseif (kill_script_step == 36) \
        out%;\
    /elseif (kill_script_step == 37) \
        w%;\
    /elseif (kill_script_step == 38) \
        enter tent%;\
    /elseif (kill_script_step == 39) \
        out%;\
    /elseif (kill_script_step == 40) \
        w%;\
    /elseif (kill_script_step == 41) \
        enter tent%;\
    /elseif (kill_script_step == 42) \
        out%;\
    /elseif (kill_script_step == 43) \
        w%;\
    /elseif (kill_script_step == 44) \
        enter tent%;\
    /elseif (kill_script_step == 45) \
        out%;\
    /elseif (kill_script_step == 46) \
        w%;\
    /elseif (kill_script_step == 47) \
        enter tent%;\
    /elseif (kill_script_step == 48) \
        out%;\
    /elseif (kill_script_step == 49) \
        se%;\
    /elseif (kill_script_step == 50) \
        enter tent%;\
    /elseif (kill_script_step == 48) \
        out%;\
    /elseif (kill_script_step == 49) \
        e%;\
    /elseif (kill_script_step == 50) \
        enter tent%;\
    /elseif (kill_script_step == 51) \
        out%;\
    /elseif (kill_script_step == 52) \
        e%;\
    /elseif (kill_script_step == 53) \
        enter tent%;\
    /elseif (kill_script_step == 54) \
        out%;\
    /elseif (kill_script_step == 55) \
        e%;\
    /elseif (kill_script_step == 56) \
        enter tent%;\
    /elseif (kill_script_step == 57) \
        out%;\
    /elseif (kill_script_step == 58) \
        e%;\
    /elseif (kill_script_step == 59) \
        enter tent%;\
    /elseif (kill_script_step == 60) \
        out%;\
    /elseif (kill_script_step == 61) \
        sw%;\
    /elseif (kill_script_step == 62) \
        enter tent%;\
    /elseif (kill_script_step == 63) \
        out%;\
    /elseif (kill_script_step == 64) \
        w%;\
    /elseif (kill_script_step == 65) \
        enter tent%;\
    /elseif (kill_script_step == 66) \
        out%;\
    /elseif (kill_script_step == 67) \
        w%;\
    /elseif (kill_script_step == 68) \
        enter tent%;\
    /elseif (kill_script_step == 69) \
        out%;\
    /elseif (kill_script_step == 70) \
        se%;\
    /elseif (kill_script_step == 71) \
        enter tent%;\
    /elseif (kill_script_step == 72) \
        out%;\
    /else \
        n%;n%;n%;n%;n%;n%;n%;\
        kstop %;\
        /e @ dwarf camp complete %;\
        /kill_script_reset %;\
    /endif

