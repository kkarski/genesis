;
; Gont tower
;

/set kill_script_total_steps 14

/def kill_script_continue = \
    /if (kill_script_step == 1) \
        s%;\
    /elseif (kill_script_step == 2) \
        /e @ floor 1 %;\
        s%;nw%;\
    /elseif (kill_script_step == 3) \
        se%;ne%;\
    /elseif (kill_script_step == 4) \
        sw%;se%;\
    /elseif (kill_script_step == 5) \
        nw%;sw%;\
    /elseif (kill_script_step == 6) \
        /e @ floor 2 %;\
        ne%;u%;nw%;\
    /elseif (kill_script_step == 7) \
        se%;ne%;\
    /elseif (kill_script_step == 8) \
        sw%;se%;\
    /elseif (kill_script_step == 9) \
        nw%;sw%;\
    /elseif (kill_script_step == 10) \
        /e @ floor 3 %;\
        ne%;u%;nw%;\
    /elseif (kill_script_step == 11) \
        se%;ne%;\
    /elseif (kill_script_step == 12) \
        sw%;se%;\
    /elseif (kill_script_step == 13) \
        nw%;sw%;\
    /else \
        ne%;d%;d%;n%;n%;\
        kstop %;\
        /e @ gont tower complete %;\
        /kill_script_reset %;\
    /endif

