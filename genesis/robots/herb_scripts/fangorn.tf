;
; Herbing in Fangorn Forest
;

/set fangorn_script 0
/set fangorn_step 0
/set attanars 0
/set suranies 0

; aliases

/alias fangorn_reset \
    /e @ resetting Fangorn script %;\
    /set fangorn_script 0 %;\
    /set fangorn_step 0 %;\
    /set attanars 0 %;\
    /set suranies 0

/alias fangorn_start \
    /set fangorn_script 1 %;\
    fangorn_continue
    

/alias fangorn_continue \
    /if (fangorn_step == 0) \
        /e @ Fangorn start %;\
        hfind suranie %;\
        hs %;\
    /elseif (fangorn_step == 1) \
        ne %;\
        hs %;\
    /elseif (fangorn_step == 2) \
        se %;\
        ne %;\
        se %;\
        ne %;\
        hs %;\
    /elseif (fangorn_step == 3) \
        s %;\
        hs %;\
    /elseif (fangorn_step == 4) \
        n %;\
        se %;\
        hs %;\
    /elseif (fangorn_step == 5) \
        ne %;\
        s %;\
        hs %;\
    /elseif (fangorn_step == 6) \
        n %;\
        nw %;\
        nw %;\
        ne %;\
        hs %;\
    /elseif (fangorn_step == 7) \
        nw %;\
        sw %;\
        hs %;\
    /elseif (fangorn_step == 8) \
        s %;\
        hs %;\
    /elseif (fangorn_step == 9) \
        n %;\
        sw %;\
        nw %;\
        nw %;\
        sw %;\
        sw %;\
        nw %;\
        hs %;\
    /elseif (fangorn_step == 10) \
        ne %;\
        n %;\
        hs %;\
    /elseif (fangorn_step == 11) \
        s %;\
        ne %;\
        hs %;\
    /elseif (fangorn_step == 12) \
        se %;\
        ne %;\
        se %;\
        n %;\
        hs %;\
    /elseif (fangorn_step == 13) \
        hfind attanar %;\
        s %;\
        ne %;\
        hs %;\
    /elseif (fangorn_step == 14) \
        sw %;\
        nw %;\
        hs %;\
    /elseif (fangorn_step == 15) \
        sw %;\
        nw %;\
        sw %;\
        nw %;\
        s %;\
        hs %;\
    /elseif (fangorn_step == 16) \
        n %;\
        se %;\
        sw %;\
        se %;\
        ne %;\
        se %;\
        n %;\
        hs %;\
    /elseif (fangorn_step == 17) \
        s %;\
        hs %;\
    /elseif (fangorn_step == 18) \
        se %;\
        ne %;\
        n %;\
        hs %;\
    /elseif (fangorn_step == 19) \
        s %;\
        se %;\
        hs %;\
    /elseif (fangorn_step == 20) \
        se %;\
        sw %;\
        nw %;\
        s %;\
        hs %;\
    /elseif (fangorn_step == 21) \
        n %;\
        hs %;\
    /elseif (fangorn_step == 22) \
        sw %;\
        nw %;\
        s %;\
        hs %;\
    /elseif (fangorn_step = 23) \
        n %;\
        sw %;\
        /e @ Fangorn end %;\
        /e @ found %suranies suranies and %attanars attanars %;\
        /repeat -2 1 fangorn_reset %;\
    /else \
        fangorn_reset %;\
    /endif

; triggers

/def -F -mregexp -t'^You search everywhere, but find no herbs\.$' fangorn_no_herbs = \
    /if (fangorn_script == 1) \
        /set fangorn_step $[fangorn_step+1] %;\
        /repeat -1 1 fangorn_continue %;\
    /endif

/def -F -mregexp -t'^You find a little blue berry\!$' fangorn_attanar = \
    /if (fangorn_script == 1) \
        /set attanars $[attanars+1] %;\
    /endif

/def -F -mregexp -t'^You find a small red berry\!$' fangorn_suranie = \
    /if (fangorn_script == 1) \
        /set suranies $[suranies+1] %;\
    /endif
