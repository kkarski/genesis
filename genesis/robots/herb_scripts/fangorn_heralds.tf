;
; Herbing in Fangorn Forest for Heralds of the Valar
;

/set hfangorn_script 0
/set hfangorn_step 0
/set attanars 0
/set suranies 0
/set athelases 0
/set lothores 0
/set huckleberries 0
/set pawnroses 0
/set parsleys 0
/set tyelkas 0

; aliases

/alias hfangorn_reset \
    /e @ resetting Fangorn script %;\
    /set hfangorn_script 0 %;\
    /set hfangorn_step 0 %;\
    /set attanars 0 %;\
    /set suranies 0 %;\
    /set athelases 0 %;\
    /set lothores 0 %;\
    /set huckleberries 0 %;\
    /set pawnroses 0 %;\
    /set parsleys 0 %;\
    /set tyelkas 0

/alias hfangorn_start \
    /set hfangorn_script 1 %;\
    hfangorn_continue

/alias hfangorn_continue \
    /e @ Fangorn step %hfangorn_step of 43 %;\
    /if (hfangorn_step == 0) \
        /e @ Fangorn start %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 1) \
        nw %;\
        hfind tyelka %;\
        hh %;\
    /elseif (hfangorn_step == 2) \
        se %;\
        ne %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 3) \
        s %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 4) \
        n %;\
        se %;\
        hfind lothore %;\
        hh %;\
    /elseif (hfangorn_step == 5) \
        n %;\
        hfind parsley %;\
        hh %;\
    /elseif (hfangorn_step == 6) \
        s %;\
        ne %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 7) \
        s %;\
        hh %;\
    /elseif (hfangorn_step == 8) \
        n %;\
        se %;\
        n %;\
        hfind lothore %;\
        hh %;\
    /elseif (hfangorn_step == 9) \
        s %;\
        ne %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 10) \
        s %;\
        hh %;\
    /elseif (hfangorn_step == 11) \
        n %;\
        se %;\
        hh %;\
    /elseif (hfangorn_step == 12) \
        ne %;\
        s %;\
        hh %;\
    /elseif (hfangorn_step == 13) \
        n %;\
        nw %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 14) \
        ne %;\
        hfind huckleberry %;\
        hh %;\
    /elseif (hfangorn_step == 15) \
        ne %;\
        hfind tyelka %;\
        hh %;\
    /elseif (hfangorn_step == 16) \
        sw %;\
        n %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 17) \
        s %;\
        nw %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 18) \
        sw %;\
        n %;\
        hfind pawnrose %;\
        hh %;\
    /elseif (hfangorn_step == 19) \
        s %;\
        sw %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 20) \
        nw %;\
        ne %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 21) \
        s %;\
        hh %;\
    /elseif (hfangorn_step == 22) \
        n %;\
        sw %;\
        n %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 23) \
        s %;\
        sw %;\
        hfind athelas %;\
        hh %;\
    /elseif (hfangorn_step == 24) \
        nw %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 25) \
        n %;\
        hh %;\
    /elseif (hfangorn_step == 26) \
        s %;\
        nw %;\
        sw %;\
        hfind pawnrose %;\
        hh %;\
    /elseif (hfangorn_step == 27) \
        n %;\
        hfind athelas %;\
        hh %;\
    /elseif (hfangorn_step == 28) \
        s %;\
        nw %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 29) \
        ne %;\
        nw %;\
        hh %;\
    /elseif (hfangorn_step == 30) \
        s %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 31) \
        n %;\
        se %;\
        n %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 32) \
        s %;\
        ne %;\
        hh %;\
    /elseif (hfangorn_step == 33) \
        s %;\
        hh %;\
    /elseif (hfangorn_step == 34) \
        n %;\
        se %;\
        n %;\
        hfind tyelka %;\
        hh %;\
    /elseif (hfangorn_step == 35) \
        s %;\
        ne %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 36) \
        se %;\
        n %;\
        hfind suranie %;\
        hh %;\
    /elseif (hfangorn_step == 37) \
        s %;\
        ne %;\
        hfind attanar %;\
        hh %;\
    /elseif (hfangorn_step == 38) \
        s %;\
        hfind huckleberry %;\
        hh %;\
    /elseif (hfangorn_step == 39) \
        n %;\
        se %;\
        hfind lothore %;\
        hh %;\
    /elseif (hfangorn_step == 40) \
        n %;\
        hh %;\
    /elseif (hfangorn_step == 41) \
        s %;\
        ne %;\
        se %;\
        hfind parsley %;\
        hh %;\
    /elseif (hfangorn_step == 42) \
        ne %;\
        s %;\
        hh %;\
    /elseif (hfangorn_step = 43) \
        n %;\
        sw %;\
        nw %;\
        sw %;\
        sw %;\
        sw %;\
        sw %;\
        sw %;\
        sw %;\
        /e @ Fangorn end %;\
        /e @ athelases    : %athelases %;\
        /e @ attanars     : %attanars %;\
        /e @ huckleberries: %huckleberries %;\
        /e @ lothores     : %lothores %;\
        /e @ parsleys     : %parsleys %;\
        /e @ pawnroses    : %pawnroses %;\
        /e @ suranies     : %suranies %;\
        /e @ tyelkas      : %tyelkas %;\
        /repeat -2 1 hfangorn_reset %;\
    /else \
        hfangorn_reset %;\
    /endif

; triggers

/def -F -mregexp -t'^You search everywhere, but find no herbs\.$' hfangorn_no_herbs = \
    /if (hfangorn_script == 1) \
        /set hfangorn_step $[hfangorn_step+1] %;\
        /repeat -1 1 hfangorn_continue %;\
    /endif
    
/def -F -mregexp -t'^Your search reveals nothing special\.$' hfangorn_not_found = \
    /if (hfangorn_script == 1) \
        /set hfangorn_step $[hfangorn_step+1] %;\
        /repeat -1 1 hfangorn_continue %;\
    /endif

/def -F -mregexp -t'^You find a little blue berry\!$' hfangorn_attanar = \
    /if (hfangorn_script == 1) \
        /set attanars $[attanars+1] %;\
    /endif

/def -F -mregexp -t'^You find a small red berry\!$' hfangorn_suranie = \
    /if (hfangorn_script == 1) \
        /set suranies $[suranies+1] %;\
    /endif

/def -F -mregexp -t'^You find a brown broad-headed mushroom\!$' hfangorn_tyelka = \
    /if (hfangorn_script == 1) \
        /set tyelkas $[tyelkas+1] %;\
    /endif

/def -F -mregexp -t'^You find a green leaf\!$' hfangorn_athelas_parsley = \
    /if (hfangorn_script == 1) \
        /if (hfangorn_step == 5 | hfangorn_step > 40) \
            /set parsleys $[parsleys+1] %;\
        /else \
            /set athelases $[athelases+1] %;\
        /endif %;\
    /endif

/def -F -mregexp -t'^You find a green rose\!$' hfangorn_pawnrose = \
    /if (hfangorn_script == 1) \
        /set pawnroses $[pawnroses+1] %;\
    /endif

/def -F -mregexp -t'^You find a green berry\!$' hfangorn_huckleberry = \
    /if (hfangorn_script == 1) \
        /set huckleberries $[huckleberries+1] %;\
    /endif

/def -F -mregexp -t'^You find a red heart-shaped flower\!$' hfangorn_lothore = \
    /if (hfangorn_script == 1) \
        /set lothores $[lothores+1] %;\
    /endif
