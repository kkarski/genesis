;
; Blademasters of Khalakhor
;

; variables

/set battacking 1
/set decapitation 0

; aliases

/alias z /send battack %{*}

; definitions

/def battacking = \
    /if (battacking) \
        /set battacking 0 %;\
        /e @ automatic battack OFF %;\
    /else \
        /set battacking 1 %;\
        /e @ automatic battack ON %;\
    /endif

/def battack_check = \
    /if (battacking) \
        /set battacking 0 %;\
        /send battack %;\
        /repeat -2 1 /set battacking 1 %;\
    /endif

/def battack_msg = \
    /echo %;\
    /echo -aCCyan < battack: %{*} > %;\
    /echo

; triggers

/def -F -mregexp -t'^Glory thy Blade, Blademaster\.$' bm_start = \
    /if (!decapitation) \
        /repeat -1 1 /send bdecapitate off %;\
    /endif

/def -F -mregexp -t'^You (attack|assist|turn to attack)' battack_attack = \
    /battack_check

/def -F -mregexp -t'attacks you\!$' battack_defend = \
    /repeat -1 1 /battack_check

/def -F -mregexp -t'^You have no swords to attack with\!$' = \
    /set battacking 0

/def -F -PBCwhite -mregexp -t'^You begin preparations to use a special attack form\.' battack_prepare = \
    /if (battacking) \
        /set battacking 0 %;\
        /repeat -2 1 /set battacking 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to use a special attack form again\.' = \
    /battack_check

/def -F -mregexp -t'^Attack who\?$' = \
    /set battacking 1

/def -F -mregexp -t'(^You|^> You) do not feel ready to battack again\.$' = \
    /set battacking 1

/def -F -PCred -mregexp -t'^Your movement has prevented completion of your attack form\!$' = \
    /set battacking 1

/def -F -mregexp -t'^It appears your quarry has escaped before you could attack\.$' = \
    /set battacking 1

/def -F -mregexp -t'^The target of your attack seems to have slipped away\.$' = \
    /set battacking 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set battacking 1

/def -F -t'*breath return*' = /send battack
/def -F -t'*releases his grip on you*' = /send battack
/def -F -t'*recovered your senses*' = /send battack
/def -F -t'*cannot gather your concentration*' = /repeat -3 1 /send battack
/def -F -t'*You regain consciousness*' = /send battack
/def -F -t'*You recover your balance again*' = /send battack
/def -F -t'*You no longer feel paralyzed*' = /send battack
/def -F -t'*You are unable to attack your target*' = /send battack

; hilites

/def -F -PBCwhite -mregexp -t'you miss by a mile\!$' = \
    /battack_msg miss

/def -F -P1BCwhite -mregexp -t'^Your attack (barely scratches|lightly wounds|\
    mildly cuts|roughly cuts|badly slashes|horribly slices|viciously mauls|\
    near-fatally wounds|fatally wounds)' = \
    /battack_msg %{P1}

/def -F -P1BCwhite -mregexp -t'(fatal wound) left by the flash of your blade\.$' = \
    /battack_msg fatally wounds
