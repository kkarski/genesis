;
; Scripts for the Minstrels
;

; variables

/set instrument recorder
/set mfocus perform
/set mtarget me
/set mrepeat 0
/set mupkeep 1
/set mperformed 0

; hilites

/YELLOW Somewhere in the distance you hear the Bells (.*)
/YELLOW heralding (.*)
/YELLOW arrival of a Minstrel, (.*)
/YELLOW arrival of a Minstrel.
/YELLOW As if from a great distance, you hear (.*)

/WHITE You .* attempt (.*)\.$
/WHITE ^(The .*|[^ ]+) .* attempts a Song of Power(.*)
/WHITE ^(The .*|[^ ]+) .* melody (.*)
/WHITE ^(The .*|[^ ]+) .* attempts a Song of Power .*
/WHITE An uplifting melody drifts lightly upon the air, (.*)
/WHITE The sweet tune rushes over your body, (.*)
/WHITE ^(The .*|[^ ]+) plays an uplifting melody, and .* seems refreshed by the music\.
/WHITE ^You are .* performer of the realms\.
/WHITE ^(He|She|It) is .* performer of the realms\.

; definitions

/def minstrel_msg = \
    /echo %;\
    /echo -aCcyan < %{*} > %;\
    /echo

/def minstrel_fail = \
    /echo %;\
    /echo -aCred << %{*} >> %;\
    /echo

/def minstrel_performance = \
    /if (mperformed == 0) \
        /send perform ballad 35 %;\
    /elseif (mperformed == 1) \
        /send perform ballad 30 %;\
    /elseif (mperformed == 2) \
        /send perform ballad 32 %;\
    /else \
        /send perform ballad 33 %;\
    /endif %;\
    /set mperformed $[mperformed+1]

; triggers

/def -F -PBCwhite -mregexp -t'^You feel you have the crowd\'s attention\. It is time to perform\!$' = \
    /minstrel_msg Time to perform! %;\
    /minstrel_performance

/def -F -PBCwhite -mregexp -t'^The patrons expectantly wait for your next rendition in your performance\.$' = \
    /minstrel_msg Next rendition %;\
    /minstrel_performance

/def -F -PBCwhite -mregexp -t'^With your performance over, the patrons of .* go back to their drinks and conversations\.$' = \
    /minstrel_msg Performance over %;\
    /set mperformed 0

/def -F -PBCwhite -mregexp -t'^Your reputation as a performer has increased\!$' = \
    /minstrel_msg Reputation increased!

/def -F -PCred -mregexp -t'^You fail in your efforts to perform the Song of Power\.$' = \
    /minstrel_fail Song of Power failed!

/def -F -PCred -mregexp -t'^The Song of Power fails\!$' = \
    /minstrel_fail Song of Power failed!

/def -F -PCred -mregexp -t'^Nobody here to perform that Song of Power on\!$' = \
    /minstrel_fail Song of Power target gone!

/def -F -PCred -mregexp -t'^You are mentally drained.*' = \
    /minstrel_fail out of mana!

/def -F -PCred -mregexp -t'^You have lost concentration while moving\!$' = \
    /minstrel_fail lost concentration!

/def -F -PCred -mregexp -t'^Your (.*) sounds like it needs tuning\.$' = \
    /minstrel_fail instrument out of tune! %;\
    /set instrument %{P1} %;\
    /send tune %instrument

/def -F -PCred -mregexp -t'^You notice you\'ve damaged your beautiful Minstrel\'s ([a-z]+) .*\!$' = \
    /minstrel_fail instrument damage! %;\
    /set instrument %{P1} %;\
    /send tune %instrument

/def -F -PCred -mregexp -t'^You try your best, but find your skills unequal to the task\.$' = \
    /minstrel_fail tuning failed! %;\
    /send tune %instrument

/def -F -PBCwhite -mregexp -t'^You successfully bring your (.*) back into pefrect pitch\.$' = \
    /minstrel_msg %{P1} tuned

/def -F -mregexp -t'^You begin performing (.*)[.!]$' = \
    /minstrel_msg performing %{P1}

/def -F -mregexp -t'^You .* attempt (.*),' = \
    /minstrel_msg performing %{P1}

/def -F -PBCwhite -mregexp -t'^The melodious tune rushes over .* body, (.* wounds)\.$' = \
    /minstrel_msg soothe effect: $[strip_attr({P1})] %;\
    /if (mrepeat) \
        soothe %;\
    /endif

/def -F -PBCwhite -mregexp -t'^The uplifting melody (.*refreshes .* body) and soul\.$' = \
    /minstrel_msg refresh effect: $[strip_attr({P1})] %;\
    /if (mrepeat) \
        refresh %;\
    /endif

/def -F -PBCwhite -mregexp -t'^The uplifting melody seems to (.*refresh .* body) and soul\.$' = \
    /minstrel_msg refresh effect: $[strip_attr({P1})] %;\
    /if (mrepeat) \
        refresh %;\
    /endif

/def -F -PBCwhite -mregexp -t'^A blast of discordant music hurts .*' = \
    /minstrel_msg vibrato effect: hurts %;\
    /if (mrepeat) \
        vibrato %;\
    /endif

/def -F -PBCwhite -mregexp -t'^(The .*|[^ ]+)( is|\'s) (.*) by (a|the) .*(blast|wave) of (music|sound)\.$' = \
    /minstrel_msg vibrato effect: $[strip_attr({P3})] %;\
    /if (mrepeat) \
        vibrato %;\
    /endif
    
/def -F -PBCwhite -mregexp -t'^You feel your body quicken to the pace of the rapid Song of Celerity\.$' = \
    /minstrel_msg accelerando: up

/def -F -PBCwhite -mregexp -t'^You feel the Song of Celerity is getting close to ending\.$' = \
    /minstrel_msg accelerando: waning %;\
    /if (mupkeep) \
        accelerando %;\
    /endif

/def -F -PBCwhite -mregexp -t'^You renew your performance of the Song of Celerity\!$' = \
    /minstrel_msg accelerando: renewed

/def -F -PBCwhite -mregexp -t'^You feel your pace slow as the Song of Celerity ends\.$' = \
    /minstrel_msg accelerando: down %;\
    /if (mupkeep) \
        accelerando %;\
    /endif

; aliases

/alias mfocus \
    /if ({*} =~ none) \
        /e @ current Minstrel focus: %mfocus %;\
    /else \
        /set mfocus %{*} %;\
        /e @ current Minstrel focus: %mfocus %;\
    /endif

/alias mtarget \
    /if ({*} =~ none) \
        /e @ current Minstrel target: %mtarget %;\
    /else \
        /set mtarget %{*} %;\
        /e @ current Minstrel target: %mtarget %;\
    /endif

/alias mrepeat \
    /if (mrepeat) \
        /set mrepeat 0 %;\
        /e @ Minstrel song repeat: OFF %;\
    /else \
        /set mrepeat 1 %;\
        /e @ Minstrel song repeat: ON %;\
    /endif

/alias mupkeep \
    /if (mupkeep) \
        /set mupkeep 0 %;\
        /e @ Minstrel song upkeep: OFF %;\
    /else \
        /set mupkeep 1 %;\
        /e @ Minstrel song upkeep: ON %;\
    /endif

/alias soothe \
    /if ({*} =~ none) \
        /send %mfocus soothe for %mtarget %;\
    /else \
        mtarget %{*} %;\
        /send %mfocus soothe for %mtarget %;\
    /endif

/alias refresh \
    /if ({*} =~ none) \
        /send %mfocus refresh for %mtarget %;\
    /else \
        mtarget %{*} %;\
        /send %mfocus refresh for %mtarget %;\
    /endif

/alias vibrato /send %mfocus vibrato

/alias accelerando /send %mfocus accelerando

/alias z vibrato
/alias zz soothe %{*}
/alias zzz refresh %{*}
