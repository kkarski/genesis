;
; Worshippers of the Elementals
;

; aliases

/alias etell pray anemos %{*}
/alias eteleport pray apodosi
/alias eburden elefrinisi
/alias elight pyrofosti

; triggers

/def -F -mregexp -t'^Welcome, (Seeker|Worshipper) .*\!$' ele_start = \
    /send whang %;\
    /send dsheathe

; hilites

/def -F -PCred -mregexp -t'^You need the proper ritual component to invoke .*'

/def -F -PBCmagenta -mregexp -t'iridescent (opals|opal)|striped chalcedon(y|ies)|rose (quartzes|quartz)'

/def -F -PBCmagenta -mregexp -t'^You .* the (air|earth|fire|water) around you\.$'

/def -F -PBCyellow -mregexp -t'^A soft wind blows around your ears, carrying the voice of .* whispering:'
/def -F -PBCyellow -mregexp -t'^The voice of .* echoes through your mind announcing that .*'
/def -F -PBCyellow -mregexp -t'^You sense the stirring of the elements .*'
