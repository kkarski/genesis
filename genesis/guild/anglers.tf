;
; Anglers Club
;

; variables

/set fishing 0
/set bait_count 0
/set fish_caught 0
/set bait_type worm

; aliases

/alias os /send open satchel
/alias cs /send close satchel
/alias gs /send get %{*} from satchel
/alias ps /send put %{*} in satchel

/alias fish_here \
    /set fishing 1 %;\
    /e @ fishing ON %;\
    prepare_rod %;\
    /send cast rod

/alias prepare_rod \
    os %;\
    gs %bait_type %;\
    cs %;\
    /send bait hook with %bait_type 

/alias bait_decrease \
    /bc $[bait_count-1]
    
/alias display_bait_count \
    /e @ bait count: %bait_count

/alias bt \
    /set bait_type %{*} %;\
    /e @ bait type: %bait_type

; definitions

/def fishing = \
    /if (fishing) \
        /set fishing 0 %;\
        /e @ fishing OFF %;\
    /else \
        /set fishing 1 %;\
        /e @ fishing ON %;\
    /endif

/def bc = \
    /set bait_count %{*} %;\
    display_bait_count

; triggers

/def -F -mregexp -t'^Your line is cast\. You should \<reel rod\> to try and catch something\.$' line_cast = \
    /send reel rod

/def -F -mregexp -t'^You can \<reel rod\> to try again\.$' line_slack = \
    /send reel rod

/def -F -mregexp -t'^You reel in your line but there is nothing on it but your bait\.' line_empty = \
    /if (fishing & bait_count > 0) \
        /send cast rod %;\
    /endif

/def -F -mregexp -t'^You reel in your line but there is nothing on it\. Not even your bait\. Drats\!$' line_empty_no_bait = \
    bait_decrease %;\
    /if (fishing & bait_count > 0) \
        prepare_rod %;\
        /send cast rod %;\
    /endif

/def -F -mregexp -t'^You reel in your line, pulling in (.*)\!' line_catch = \
    /set fish_caught=$[fish_caught+1] %;\
    /echo %;\
    /e @ caught: %P1 %;\
    /echo %;\
    /e @ total fish caught: %fish_caught %;\
    bait_decrease %;\
    /if (fishing & bait_count > 0) \
        prepare_rod %;\
        /send cast rod %;\
    /endif

/def -F -mregexp -t'^You must be holding the rod to cast it\.' no_rod = \
    /send unwield all %;\
    /send remove shields %;\
    /send hold rod %;\
    /send cast rod

/def -F -mregexp -t'^Your rod jerks down a bit and you yank up hard\.\.\. but the line snaps and your catch is lost\!$' line_snap = \
    bait_decrease %;\
    prepare_rod %;\
    /send cast rod
