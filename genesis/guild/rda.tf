;
; Red Dragonarmy
;

; variables

/set impaling 1
/set impale_miss 0
/set impale_no_damage 0
/set impale_weak 0
/set impale_moderate 0
/set impale_severe 0
/set impale_kill 0
/set total_impales 0

; aliases

/alias x /send impale %{*}
/alias dstat /dstat

; definitions

/def impaling = \
    /if (impaling) \
        /set impaling 0 %;\
        /da_msg automatic impale OFF %;\
    /else \
        /set impaling 1 %;\
        /da_msg  automatic impale ON %;\
    /endif

/def impale_check = \
    /if (impaling) \
        /set impaling 0 %;\
        /send impale %;\
        /repeat -2 1 /set impaling 1 %;\
    /endif

/def impale_msg = \
    /da_msg impale: %{*} %;\
    /send health all

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' rda_attack = \
    /impale_check

/def -F -mregexp -t'attacks you\!$' rda_defend = \
    /repeat -1 1 /impale_check

/def -F -PBCwhite -mregexp -t'^Your muscles tense as you prepare to impale\.$' impale_prepare = \
    /if (impaling) \
        /set impaling 0 %;\
        /repeat -2 1 /set impaling 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to go on the offensive again\.$' = \
    /impale_check

/def -F -mregexp -t'^(You|> You) are not yet ready to impale\. Patience\!$' = \
    /set impaling 1

/def -F -mregexp -t'^The target for your impale seems to have slipped away\.$' = \
    /set impaling 1

/def -F -mregexp -t'^You have lost concentration while moving\!$' = \
    /set impaling 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set impaling 1
  
/def -F -mregexp -t'^You snarl in anger as your well-timed thrust' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_miss $[impale_miss+1] %;\
    /impale_msg miss

/def -F -mregexp -t'^Crouching low, you set your' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_no_damage $[impale_no_damage+1] %;\
    /impale_msg no damage

/def -F -mregexp -t'^You thrust forward at' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_weak $[impale_weak+1] %;\
    /impale_msg weak

/def -F -mregexp -t'^With a wild laugh, you fake a thrust with your' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_weak $[impale_weak+1] %;\
    /impale_msg weak

/def -F -mregexp -t'^You swiftly strike out at' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_weak $[impale_weak+1] %;\
    /impale_msg weak

/def -F -mregexp -t'^Breaking through .*\'s guard, you bash' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_weak $[impale_weak+1] %;\
    /impale_msg weak

/def -F -mregexp -t'^Reversing your .*, you craftily strike' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_weak $[impale_weak+1] %;\
    /impale_msg weak

/def -F -mregexp -t'^You lunge towards' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_moderate $[impale_moderate+1] %;\
    /impale_msg moderate

/def -F -mregexp -t'^You drive forward at' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_moderate $[impale_moderate+1] %;\
    /impale_msg moderate

/def -F -mregexp -t'^Lunging powerfully with your' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_severe $[impale_severe+1] %;\
    /impale_msg severe

/def -F -mregexp -t'^Avoiding a desperate attack by .*, you thrust' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_severe $[impale_severe+1] %;\
    /impale_msg severe

/def -F -mregexp -t'^In shock, .* clutches .* as you deal' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_severe $[impale_severe+1] %;\
    /impale_msg severe

/def -F -mregexp -t'^In triumph, you raise your' = \
    /set total_impales $[total_impales+1] %;\
    /set impale_kill $[impale_kill+1] %;\
    /impale_msg death blow

/def -F -t'*breath return*' = /send impale
/def -F -t'*releases*grip on you*' = /send impale
/def -F -t'*recovered your senses*' = /send impale
/def -F -t'*regain consciousness*' = /send impale
/def -F -t'*recover your balance again*' = /send impale
/def -F -t'*no longer feel paralyzed*' = /send impale
/def -F -t'*unable to attack your target*' = /repeat -2 1 /send impale
/def -F -t'*wrestle back control of your mind*' = /send impale
/def -F -t'*you managed to shake off the terror*' = /send impale

; hilites

/WHITE ancient dragon-coiled halberd
/WHITE lethal flame-bladed glaive
/WHITE sleek steel-bladed poleaxe
/WHITE serrated obsidian halberd
/WHITE unholy halberd
/WHITE strange halberd
/WHITE excellent halberd
/WHITE rust-red long naginata
/WHITE giant black maul
/WHITE great war trident
/WHITE blackened mithril lance
/WHITE tempered spear
/WHITE complex medicine stick

; overrides

/set anti_undead 0
/set spare halberd

/def -F -mregexp -t'^(You|> You) wield the blackened mithril lance' = \
    /set anti_undead 1

/def -F -mregexp -t'^(You|> You) stop wielding the blackened mithril lance' = \
    /set anti_undead 0

/alias sswap \
    /if (anti_undead) \
        op %;\
        gp %{spare} %;\
        /send unwield lance %;\
        /send wield %{spare} %;\
        pp lance %;\
        cp %;\
    /else \
        op %;\
        gp lance %;\
        /send unwield %{spare} %;\
        /send wield lance %;\
        pp %{spare} %;\
        cp %;\
    /endif

; statistics

/def dreset = \
    /set impale_miss 0 %;\
    /set impale_no_damage 0 %;\
    /set impale_weak 0 %;\
    /set impale_moderate 0 %;\
    /set impale_severe 0 %;\
    /set impale_kill 0 %;\
    /set total_impales 0

/def count_impale_ratio = \
    /if ({*} == 0) \
      /set impale_ratio 0 %;\
    /else \
      /set impale_ratio $[100-(((total_impales-{*})*100)/total_impales)] %;\
    /endif

/def prepare_impale_graph = \
    /if (impale_ratio = 0) \
        /set impale_graph .............................. %;\
    /elseif ((impale_ratio >0) & (impale_ratio < 4)) \
        /set impale_graph #............................. %;\
    /elseif ((impale_ratio > 3) & (impale_ratio < 7)) \
        /set impale_graph ##............................ %;\
    /elseif ((impale_ratio > 6) & (impale_ratio < 11)) \
        /set impale_graph ###........................... %;\
    /elseif ((impale_ratio > 10) & (impale_ratio < 14)) \
        /set impale_graph ####.......................... %;\
    /elseif ((impale_ratio > 13) & (impale_ratio < 17)) \
        /set impale_graph #####......................... %;\
    /elseif ((impale_ratio > 16) & (impale_ratio < 21)) \
        /set impale_graph ######........................ %;\
    /elseif ((impale_ratio > 20) & (impale_ratio < 24)) \
        /set impale_graph #######....................... %;\
    /elseif ((impale_ratio > 23) & (impale_ratio < 27)) \
        /set impale_graph ########...................... %;\
    /elseif ((impale_ratio > 26) & (impale_ratio < 31)) \
        /set impale_graph #########..................... %;\
    /elseif ((impale_ratio > 30) & (impale_ratio < 34)) \
        /set impale_graph ##########.................... %;\
    /elseif ((impale_ratio > 33) & (impale_ratio < 37)) \
        /set impale_graph ###########................... %;\
    /elseif ((impale_ratio > 36) & (impale_ratio < 41)) \
        /set impale_graph ############.................. %;\
    /elseif ((impale_ratio > 40) & (impale_ratio < 44)) \
        /set impale_graph #############................. %;\
    /elseif ((impale_ratio > 43) & (impale_ratio < 47)) \
        /set impale_graph ##############................ %;\
    /elseif ((impale_ratio > 46) & (impale_ratio < 51)) \
        /set impale_graph ###############............... %;\
    /elseif ((impale_ratio > 50) & (impale_ratio < 54)) \
        /set impale_graph ################.............. %;\
    /elseif ((impale_ratio > 53) & (impale_ratio < 57)) \
        /set impale_graph #################............. %;\
    /elseif ((impale_ratio > 56) & (impale_ratio < 61)) \
        /set impale_graph ##################............ %;\
    /elseif ((impale_ratio > 60) & (impale_ratio < 64)) \
        /set impale_graph ###################........... %;\
    /elseif ((impale_ratio > 63) & (impale_ratio < 67)) \
        /set impale_graph ####################.......... %;\
    /elseif ((impale_ratio > 66) & (impale_ratio < 71)) \
        /set impale_graph #####################......... %;\
    /elseif ((impale_ratio > 70) & (impale_ratio < 74)) \
        /set impale_graph ######################........ %;\
    /elseif ((impale_ratio > 73) & (impale_ratio < 77)) \
        /set impale_graph #######################....... %;\
    /elseif ((impale_ratio > 76) & (impale_ratio < 81)) \
        /set impale_graph ########################...... %;\
    /elseif ((impale_ratio > 80) & (impale_ratio < 84)) \
        /set impale_graph #########################..... %;\
    /elseif ((impale_ratio > 83) & (impale_ratio < 87)) \
        /set impale_graph ##########################.... %;\
    /elseif ((impale_ratio > 86) & (impale_ratio < 91)) \
        /set impale_graph ###########################... %;\
    /elseif ((impale_ratio > 90) & (impale_ratio < 94)) \
        /set impale_graph ############################.. %;\
    /elseif ((impale_ratio > 93) & (impale_ratio < 97)) \
        /set impale_graph #############################. %;\
    /elseif ((impale_ratio > 96) & (impale_ratio < 101)) \
        /set impale_graph ############################## %;\
    /else \
        /echo -aBCred *** ERROR: value out of range *** %;\
    /endif

/def dstat = \
    /if (total_impales > 0) \
        /let impale_hit $[total_impales-impale_miss] %;\
    /else \
        /let impale_hit 0 %;\
    /endif %;\
    /echo -aBCcyan @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %;\
    /echo -aBCcyan @@@@ ---------------------------------------------- @@@@ %;\
    /echo -aBCcyan @@@@ |     I M P A L E    S T A T I S T I C S     | @@@@ %;\
    /echo -aBCcyan @@@@ ---------------------------------------------- @@@@ %;\
    /echo -aBCcyan @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ Total impales                                   : %total_impales %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /count_impale_ratio %impale_hit %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ Hit  $[pad(impale_ratio, 3)]%%       %impale_graph  : %impale_hit %;\
    /count_impale_ratio %impale_miss %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ Miss $[pad(impale_ratio, 3)]%%       %impale_graph  : %impale_miss %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /count_impale_ratio %impale_no_damage %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ No damage       %impale_graph  : %impale_no_damage %;\
    /count_impale_ratio %impale_weak %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ Weak            %impale_graph  : %impale_weak %;\
    /count_impale_ratio %impale_moderate %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ Moderate        %impale_graph  : %impale_moderate %;\
    /count_impale_ratio %impale_severe %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ Severe          %impale_graph  : %impale_severe %;\
    /count_impale_ratio %impale_kill %;\
    /prepare_impale_graph %;\
    /echo -aBCcyan @ Death blow      %impale_graph  : %impale_kill %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ Statistics generated $[ftime("%a %b %d %X %Y", time())] %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @
