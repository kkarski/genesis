;
; Blue Dragonarmy
;

; variables

/set slashing 1

; aliases

/alias x /send slash %{*}

; definitions

/def slashing = \
    /if (slashing) \
        /set slashing 0 %;\
        /da_msg automatic slash OFF %;\
    /else \
        /set slashing 1 %;\
        /da_msg automatic slash ON %;\
    /endif

/def slash_check = \
    /if (slashing) \
        /set slashing 0 %;\
        /send slash %;\
        /repeat -3 1 /set slashing 1 %;\
    /endif

/def slash_msg = \
    /da_msg slash: %{*} %;\
    /send health all

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' bda_attack = \
    /slash_check
    
/def -F -mregexp -t'attacks you\!$' bda_defend = \
    /repeat -1 1 /slash_check

/def -F -PBCwhite -mregexp -t'^Your muscles tense as you prepare to slash\.$' slash_prepare = \
    /if (slashing) \
        /set slashing 0 %;\
        /repeat -2 1 /set slashing 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to go on the offensive again\.$' = \
    /slash_check

/def -F -mregexp -t'^(You|> You) are not yet ready to slash\. Patience\!$' = \
    /set slashing 1

/def -F -mregexp -t'^The target for your slash seems to have slipped away\.$' = \
    /set slashing 1

/def -F -mregexp -t'^You have lost concentration while moving\!$' = \
    /set slashing 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set slashing 1

; 0
/def -F -mregexp -t'^Crouching low, you swing your' = \
    /slash_msg no damage

; 1-2
/def -F -mregexp -t'^With a careless swing of your' = \
    /slash_msg weak

; 3-5
/def -F -mregexp -t'^With a wild laugh, you fake a thrust with your' = \
    /slash_msg weak

/def -F -mregexp -t'^With a swift swing, you strike' = \
    /slash_msg weak

; 6-10
/def -F -mregexp -t'^Breaking through .*\'s guard, you bash' = \
    /slash_msg weak

/def -F -mregexp -t'^Reversing your .*, you craftily strike' = \
    /slash_msg weak

; 11-20
/def -F -mregexp -t'^You slash at .*, scoring' = \
    /slash_msg moderate

; 21-30
/def -F -mregexp -t'^You slash at .*, cutting' = \
    /slash_msg moderate

; 31-50
/def -F -mregexp -t'^With a powerful swing of your .*, you slash' = \
    /slash_msg severe

; 51-75
/def -F -mregexp -t'^Avoiding a desperate attack by .*, you bring' = \
    /slash_msg severe

; 76-99
/def -F -mregexp -t'^In shock, .* clutches .* as you deal' = \
    /slash_msg severe

; kill
/def -F -mregexp -t'^In triumph, you raise your' = \
    /slash_msg death blow

; miss
/def -F -mregexp -t'^You snarl in anger as your well-timed slash' = \
    /slash_msg miss

/def -F -t'*breath return*' = /send slash
/def -F -t'*releases*grip on you*' = /send slash
/def -F -t'*recovered your senses*' = /send slash
/def -F -t'*You regain consciousness*' = /send slash
/def -F -t'*You recover your balance again*' = /send slash
/def -F -t'*You no longer feel paralyzed*' = /send slash
/def -F -t'*You are unable to attack your target*' = /repeat -2 1 send slash
/def -F -t'*wrestle back control of your mind*' = /send slash
/def -F -t'*you managed to shake off the terror*' = /send slash
