;
; Order of the Stars
;

; variables

/set oupkeep 1
/set orepeat 0
/set ooffensive 0
/set ocontainer sash
/set oheal_tgt none
/set autoheal 0

; definitions

/def omsg = \
    /echo %;\
    /echo -aCcyan < %{*} > %;\
    /echo %;\
    /send health all

; aliases

/alias oprep %;\
    /send open %ocontainer %;\
    /send get %{1} from %ocontainer %;\
    /send get %{2} from %ocontainer %;\
    /send close %ocontainer

/alias oupkeep \
    /if (oupkeep) \
        /set oupkeep 0 %;\
        /omsg oots: spell upkeep disabled %;\
    /else \
        /set oupkeep 1 %;\
        /omsg oots: spell upkeep enabled %;\
    /endif

/alias autoheal \
    /if (autoheal) \
        /set autoheal 0 %;\
        /omsg automatic healing disabled %;\
    /else \
        /set autoheal 1 %;\
        /omsg automatic healing enabled %;\
    /endif

/alias ooffensive \
    /if (ooffensive) \
        /set ooffensive 0 %;\
        /omsg oots: offensive spellcasting disabled %;\
    /else \
        /set ooffensive 1 %;\
        /omsg oots: offensive spellcasting enabled %;\
    /endif

/alias orepeat_forever \
    /send %orepeat_action %;\
    /if (orepeat) \
        /repeat -00:00:15 1 %orepeat_command %;\
    /endif

/alias orepeat \
    /if ({*} !~ "") \
        /set orepeat 1 %;\
        /set orepeat_action %{*} %;\
        /set orepeat_command orepeat_forever %;\
        /omsg repeating: %orepeat_action %;\
        orepeat_forever %;\
    /else \
        /omsg disabled repeating: %orepeat_action %;\
        /set orepeat 0 %;\
    /endif

/alias oheroism \
    oprep onoclea phial %;\
    /send oheroism

/alias ocure \
    oprep cranberry elidross %;\
    /send ocure %{*}

/alias ocalm \
    oprep dandelion %;\
    /send ocalm %{*}

/alias z \
    /if (ooffensive) \
        /send oheal enemy %;\
    /else \
        /if ({*} =~ none) \
            /send oheal %oheal_tgt %;\
        /else \
            /send oheal %{*} %;\
            /set oheal_tgt %{*} %;\
        /endif %;\
    /endif

; triggers

/def -F -mregexp -t'^You are physically (.*) and mentally' = \
    /if ({P1} !~ "feeling very well" & {P1} !~ "feeling well" & {P1} !~ "sore") \
        /if (autoheal) \
            /send oheal %;\
        /endif %;\
    /endif

/def -F -mregexp -t'^You (attack|assist|turn to attack)' = \
    /if (ooffensive) \
        /repeat -1 1 /send oheal enemy %;\
    /endif

/def -F -mregexp -t'died\.$' = \
    /if (ooffensive) \
        /repeat -2 1 /send oheal enemy %;\
    /endif

/def -F -PCcyan -mregexp -t'^You feel the blessings of .* wash over you .*, (.*)\.$' = \
    /omsg %{P1}

/def -F -PCcyan -mregexp -t'^You place your hands on (.*), and .* shivers as (his|her|its) (.*)\.$' = \
    /omsg %{P1}'s %{P3}

/def -F -PCcyan -mregexp -t'^Your medallion of faith blazes with purifying azure light, (.*)\.$' = \
    /omsg %{P1} %;\
    /if (ooffensive) \
        /send oheal enemy %;\
    /endif

/def -F -PCcyan -mregexp -t'^Your prayer of healing is unheard\.$' = \
    /omsg prayer of healing unheard %;\
    /if (ooffensive) \
        /send oheal enemy %;\
    /endif

/def -F -PCcyan -mregexp -t'^Your medallion of faith blazes with a divine azure light\!$' = \
    /omsg radiance up

/def -F -PCcyan -mregexp -t'^You renew the light that you are maintaining\!$' = \
    /omsg radiance renewed

/def -F -PCcyan -mregexp -t'^You sense the intensity of the light surrounding your medallion of faith begin to wane\.$' = \
    /omsg radiance waning %;\
    /if (oupkeep) \
        /send oradiance %;\
    /endif

/def -F -PCcyan -mregexp -t'^The light surrounding your medallion of faith dims and fades away\.$' = \
    /omsg radiance down %;\
    /if (oupkeep) \
        /send oradiance %;\
    /endif

/def -F -PCcyan -mregexp -t'^Your prayers are granted as you solemnly swallow the fronds .*' = \
    /omsg oheroism up

/def -F -PCcyan -mregexp -t'^You feel the righteous blessings of .* begin to wane\.$' = \
    /omsg oheroism waning %;\
    /if (oupkeep) \
       oheroism %;\
    /endif

/def -F -PCcyan -mregexp -t'^You feel the righteous blessings of .* leave you\.$' = \
    /omsg oheroism down %;\
    /if (oupkeep) \
       oheroism %;\
    /endif

/def -F -PBCwhite -mregexp -t'^You are newly ordained to your position in the Holy Order of the Stars\.$' = \
    /omsg progress: 0/10

/def -F -PBCwhite -mregexp -t'^You are just beginning your journey to your next ordainment\.$' = \
    /omsg progress: 1/10

/def -F -PBCwhite -mregexp -t'^You are early in your journey to to your next ordainment\.$' = \
    /omsg progress: 2/10

/def -F -PBCwhite -mregexp -t'^You are far from progressing to your next ordainment\.$' = \
    /omsg progress: 3/10

/def -F -PBCwhite -mregexp -t'^You are almost halfway to your next ordainment\.$' = \
    /omsg progress: 4/10

/def -F -PBCwhite -mregexp -t'^You are over halfway to your next ordainment\.$' = \
    /omsg progress: 5/10

/def -F -PBCwhite -mregexp -t'^You are journeying well towards your next ordainment\.$' = \
    /omsg progress: 6/10

/def -F -PBCwhite -mregexp -t'^You are journeying very well towards your next ordainment\.$' = \
    /omsg progress: 7/10

/def -F -PBCwhite -mregexp -t'^You are close to your next ordainment\.$' = \
    /omsg progress: 8/10

/def -F -PBCwhite -mregexp -t'^You are very close to your next ordainment\.$' = \
    /omsg progress: 9/10

; hilites

/def -F -PCred -mregexp -t'^You have lost concentration while moving\!$' = \
    /omsg spell failed

/def -F -PCred -mgregexp -t'You are missing an ingredient .*\!$' = \
    /omsg spell failed - missing components

/def -F -PCcyan -mregexp -t'^You raise your medallion of faith and call upon .* to disperse the darkness\!$'
/def -F -PCcyan -mregexp -t'^You whisper a prayer, supplicating .* for the blessing of divine healing\.$'
/def -F -PCcyan -mregexp -t'^You raise your medallion of faith and call upon .* for righteous aid in smiting the unliving\!$'
/def -F -PCcyan -mregexp -t'^You .* chant a prayer to .*, beseeching a blessing of heroism\!$'
/def -F -PCcyan -mregexp -t'^The divine blessings of Kiri\-Jolith calm .*\.$'
