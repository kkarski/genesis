;
; Rangers of the Westlands
;

; variables

/set brawling 1
/set disarm hide
/set rfocus disarm
/set rfocus_cycle 1

; aliases

/alias x /send %rfocus %{*}

/alias 1 /rfocus disarm
/alias 2 /rfocus blind
/alias 3 /rfocus rknee
/alias 4 /rfocus brawl

/alias rest cast rest %{*}
/alias heal cast heal %{*}
/alias cleanse cast cleanse %{*}
/alias nv cast nightvision

/alias rpotions \
    /e @ ----- Ranger potions ----- %;\
    /e @ Avarywalme (poison resist): Alfirin, Foxglove, 2x alcohol %;\
    /e @ Este             (healing): Athelas, water %;\
    /e @ Wenya           (strength): Suranie, Tuo, 2x alcohol

; definitions

/def brawling = \
    /if (brawling) \
        /set brawling 0 %;\
        /e @ brawling OFF %;\
    /else \
        /set brawling 1 %;\
        /e @ brawling ON %;\
    /endif

/def brawl_hold = \
    /set brawling 0 %;\
    /send %rfocus %;\
    /repeat -3 1 /set brawling 1

/def rfocus = \
    /set rfocus=%* %;\
    /e @ brawling focus set to: %rfocus

/def disarm = \
    /set disarm=%* %;\
    /e @ disarm reaction set to: %disarm

/def rcycle = \
    /if (rfocus_cycle) \
        /set rfocus_cycle 0 %;\
        /e @ brawl cycling OFF %;\
    /else \
        /set rfocus_cycle 1 %;\
        /e @ brawl cycling ON %;\
    /endif

; triggers

/def -F -mregexp -t'^You attack' brawl_attack = \
    /if (brawling) \
        /if (rfocus_cycle) \
            /rfocus disarm %;\
        /endif %;\
        /brawl_hold %;\
    /endif

/def -F -mregexp -t'^You assist' brawl_assist = \
    /if (brawling) \
        /if (rfocus_cycle) \
            /rfocus disarm %;\
        /endif %;\
        /brawl_hold %;\
    /endif

/def -F -mregexp -t'attacks you!$' brawl_defend = \
    /if (brawling) \
        /if (rfocus_cycle) \
            /rfocus disarm %;\
        /endif %;\
        /brawl_hold %;\
    /endif

/def -F -mregexp -t'^You turn to attack' brawl_next = \
    /if (brawling) \
        /if (rfocus_cycle) \
            /rfocus disarm %;\
        /endif %;\
        /brawl_hold %;\
    /endif

/def -F -mregexp -t'^You prepare to brawl' brawl_underway = \
    /if (brawling) \
        /set brawling 0 %;\
        /repeat -3 1 /set brawling 1 %;\
    /endif

/def -F -mregexp -t'^You once again feel ready to try a brawl\.$' \
    autobrawl = \
    /send %rfocus

/def -F -mregexp -t'^Brawl who\?$' = /set brawling 1
/def -F -mregexp -t'^Blind who\?$' = /set brawling 1
/def -F -mregexp -t'^Disarm who\?$' = /set brawling 1
/def -F -mregexp -t'^Rknee who\?$' = /set brawling 1

/def -F -t'*breath return*' = /send %rfocus
/def -F -t'*releases his grip on you*' = /send %rfocus
/def -F -t'*recovered your senses*' = /send %rfocus

/def -F -mregexp -t'completely!' disarm_reaction = \
    /repeat -1 1 /send %disarm unwielded weapon

/def -F -mregexp -t'^You (.*) sand (.*) the eyes' rcycle1 = \
    /if (rfocus_cycle) \
        /rfocus rknee %;\
    /endif

/def -F -mregexp -t'^You skillfully deliver a painful blow' rcycle2 = \
    /if (rfocus_cycle) \
        /rfocus blind %;\
    /endif

/def -F -mregexp -t'^You raise your knee' rcycle3 = \
    /if (rfocus_cycle) \
        /rfocus blind %;\
    /endif

; hilites

/HILITE *hand signals*
/HILITE *in Adunaic*

/GREEN Something about the bushes along the road catches your eye.

/WHITE Your attempt to (.*) fails
/WHITE You (.*) sand into the eyes
/WHITE You (.*) sand in the eyes
/WHITE seems to have cleared them
/WHITE You raise your knee
/WHITE causing (.*) pain.
/WHITE causing (.*) damage.
/WHITE it causes (.*) pain.
/WHITE it seems to hurt a little.
/WHITE it hurts really bad.
/WHITE it really hurts bad.
/WHITE rather painfully.
/WHITE inflicting severe pain.
/WHITE with a devastating result.
/WHITE has recovered (his|her|its) balance
/WHITE You skillfully deliver a painful blow
/WHITE stops wielding
/WHITE loses the weapon
/WHITE manages to wield (.*) weapon again!
/WHITE You hide (.*)

/MAGENTA You start concentrating, focusing on the essence of the Valar
/MAGENTA You use the virtues of (.*) heal
/MAGENTA You call upon (.*) rest
/MAGENTA Strange energies flow through your wounds, and you feel healthier!
/MAGENTA A tingling sensation spreads through your weary limbs
/MAGENTA Your eyes feel strange and more perceptive.
/MAGENTA Your nightvision slowly fades away.

/RED Your spell seems to have no effect.
/RED You feel that your spell had no effect on (.*)
/RED Your spell fails.
