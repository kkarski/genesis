;
; Dragon scripts for the Dragonarmies
;

; variables

/set dragon_attack 1
/set dragonfear 0
/set dragon_in_air 0
/set dattack dbreath
/set dragon_destination none

; aliases

/alias dhelp help dragonarmy %{*}
/alias dlist list dragonarmy members
/alias dcw dcheck weapons
/alias dca dcheck armours
/alias c \
    /if (dragon_in_air) \
        /send %dattack %;\
    /else \
        /dfear_check %;\
    /endif
/alias dattack /dattack
/alias dragonfear /dragonfear
/alias fly \
    /if ({*} =~ "back") \
        /set dragon_destination back %;\
        /send fly back %;\
    /else \
        /set dragon_destination $[toupper(substr({*}, 3), 1)] %;\
        /send fly %{*} %;\
    /endif %;\
    /if (team) \
        j Destination: %dragon_destination %;\
    /endif

; definitions

/def da_msg = \
    /echo %;\
    /echo -aCcyan < $[strip_attr({*})] > %;\
    /echo

/def dragonfear = \
    /if (dragonfear) \
        /set dragonfear 0 %;\
        /da_msg automatic dragonfear OFF %;\
    /else \
        /set dragonfear 1 %;\
        /da_msg automatic dragonfear ON %;\
    /endif

/def dfear_check = \
    /if (dragonfear & !dragon_in_air) \
        /set dragonfear 0 %;\
        /send dfear %;\
        /repeat -2 1 /set dragonfear 1 %;\
    /endif
    
/def dattacking = \
    /if (dragon_attack) \
        /set dragon_attack 0 %;\
        /da_msg automatic dragon attack OFF %;\
    /else \
        /set dragon_attack 1 %;\
        /da_msg automatic dragon attack ON %;\
    /endif

/def dattack_check = \
    /if (dragon_attack) \
        /set dragon_attack 0 %;\
        /send %dattack %;\
        /repeat -2 1 /set dragon_attack 1 %;\
    /endif

/def dattack = \
    /if (dattack =~ "dbreath") \
        /set dattack dswoop %;\
    /else \
        /set dattack dbreath %;\
    /endif %;\
    /da_msg dragon attack set to: %dattack

/def dattack_msg = \
    /da_msg dragon attack: %{*} %;\
    /send health all

; triggers

/def -F -mregexp -t'^You conquer the .* in the name' = \
    /send dcheer

/def -F -mregexp -t'^You (attack|assist|turn to attack)' dragonarmy_attack = \
    /dfear_check %;\
    /if (dragon_in_air) \
        /dattack_check %;\
    /endif

/def -F -mregexp -t'attacks you\!$' dragonarmy_defend = \
    /repeat -1 1 /dfear_check %;\
    /if (dragon_in_air) \
        /repeat -1 1 /dattack_check %;\
    /endif

/def -F -mregexp -t'^(A|An) .* dragon swoops out of the clouds above with a shriek, circling over the battlefield with cruel interest\.$' dragon_arrival = \
    /if (!dragon_in_air) \
        /da_msg dragon arrived %;\
    /endif %;\
    /set dragon_in_air 1 %;\
    /repeat -1 1 /dattack_check

/def -F -mregexp -t'^You see the flying .* dragon hovering high above you, ready to make another attack\.$' dragon_ready = \
    /set dragon_in_air 1 %;\
    /dattack_check

/def -F -mregexp -t'^You have yet to catch your breath from your last summoning\.$' dfear_not_ready = \
    /set dragon_in_air 1 %;\
    /repeat -2 1 /set dragon_in_air 0

/def -F -mregexp -t'^You signal towards the flying .* dragon in the sky above you, and you see .* begin to make a dive towards you\.$' dattack_prepare = \
    /if (dragon_attack) \
        /set dragon_attack 0 %;\
        /repeat -2 1 /set dragon_attack 1 %;\
    /endif

/def -F -mregexp -t'^The flying .* dragon swoops down but it seems that your foe has slipped away\.$' = \
    /set dragon_in_air 1 %;\
    /set dragon_attack 1

/def -F -mregexp -t'^You are too busy focusing on guiding the flying .* dragon to do that\.$' = \
    /set dragon_in_air 1 %;\
    /set dragon_attack 1

/def -F -mregexp -t'^There aren\'t any enemies present\!$' = \
    /set dragon_in_air 1 %;\
    /set dragon_attack 1

/def -F -mregexp -t'^You feel you are once again able to summon dragons to your aid\.$' = \
    /dfear_check

/def -F -mregexp -t'^The .* dragon soars up into the sky and disappears\.$' = \
    /set dragon_in_air 0 %;\
    /dfear_check %;\
    /da_msg dragon left

/def -F -PCred -mregexp -t'^You are not outdoors\! The dragons would not be able to reach you\.$' = \
    /set dragon_in_air 0

/def -F -PCred -mregexp -t'^(> You|You) don\'t see a dragon present that can be called upon\!$' = \
    /set dragon_in_air 0 %;\
    /dfear_check

/def -F -mregexp -t'^With a final beat of its leathery wings, (a|an) .* dragon lands here\.$' = \
    /da_msg dragon mount arrived %;\
    /if (boarding) \
        /send mount dragon %;\
    /endif

/def -F -mregexp -t'^With a powerful beat of its wings, (a|an) .* dragon lands here\.$' = \
    /da_msg arrived at destination %;\
    /send dismount

/def -F -mregexp -t'^You are too stunned to summon a dragon\.$' = \
    /repeat -3 1 /dfear_check

/def -F -mregexp -t'^You are too exhausted to be able to summon a dragon\.$' = \
    /da_msg exhausted!

/def -F -mregexp -t'^You are not able to concentrate enough to summon dragon\.$' = \
    /da_msg out of mana!

/def -F -mregexp -t'^You mount the .* dragon\.$' = \
    /da_msg mounted on dragon

/def -F -mregexp -t'^The .* dragon beats its massive wings, and takes off into the sky\!$' = \
    /da_msg destination: %dragon_destination

/def -F -mregexp -t'^Your dragon is growing restless\.$' = \
    /da_msg dragon growing restless!

/def -F -mregexp -t'both claws extended and (.*) as (he|she) flies by' = \
    /dattack_msg %{P1}

/def -F -mregexp -t'both claws extended and .*misses' = \
    /dattack_msg miss

/def -F -mregexp -t'^(The .*|[^ ]+) .*is (.*) by the dragon breath\!' = \
    /dattack_msg %{P2}

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set dragon_attack 1

; hilites

/RED ^You have a sinking feeling nothing is going to answer your call\.
/RED ^(The .*|[^ ]+) .*summoned them without even entering combat\!

; gagging

/gag *stiffens*
