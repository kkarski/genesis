;
; Army of Angmar
;

; variables

/set angmar_special smash
/set defend_target none
/set smashing 1
/set defending 0
/set retreating 0

; aliases

/alias x /send %angmar_special %{*}
/alias c \
    /if ({*} =~ none) \
        /send scare %atkvictim %;\
    /else \
        /send scare %{*} %;\
        /set atkvictim %{*} %;\
    /endif
/alias df /send defend %{*}
/alias fd /send focus on defence
/alias br /send break concentration

; definitions

/def amsg = \
    /echo %;\
    /echo -aCcyan < %{*} > %;\
    /echo %;\
    /send health all
   
/def smack = \
    /aspec smack

/def smash = \
    /aspec smash

/def aspec = \
    /set angmar_special=%* %;\
    /e @ angmar special set to: %angmar_special

/def angamar_retreat = \
    /set retreating 0 %;\
    /send retreat %;\
    /repeat -2 1 /set retreating 1

/def aspec_hold = \
    /set smashing 0 %;\
    /send %angmar_special %;\
    /repeat -2 1 /set smashing 1

/def defend = \
    /set defend_target %{*} %;\
    /if (defend_target =~ "") \
        /set defend_target none %;\
        /set defending 0 %;\
        /e @ defending OFF %;\
    /else \
        /set defend_target %{*} %;\
        /set defending 1 %;\
        /e @ defending %defend_target %;\
    /endif

/def areset = \
    /e @ resetting Angmar variables %;\
    /set angmar_special smash %;\
    /set defend_target none %;\
    /set smashing 1 %;\
    /set defending 0

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' angmar_attack = \
    /if (smashing) \
        /aspec_hold %;\
    /endif

/def -F -mregexp -t'attacks you\!$' angmar_retreat = \
    /if (retreating) \
        /repeat -2 1 /angmar_retreat %;\
    /endif

/def -F -mregexp -t'^You feel ready to retreat again\.$' = \
    /if (retreating) \
        /angmar_retreat %;\
    /endif

/def -F -PBCwhite -mregexp -t'^You prepare to (smash|smack|scare) .*\.$' smash_underway = \
    /if (smashing) \
        /set smashing 0 %;\
        /repeat -2 1 /set smashing 1 %;\
    /endif

/def -F -mregexp -t'^You feel relaxed again\.$' = \
    /send %angmar_special

/def -F -mregexp -t'^Whom do you wish to (smash|smack)\?$' = \
    /set smashing 1

/def -F -mregexp -t'^You do not feel ready to (smash|smack) again\.$' = \
    /set smashing 1

/def -F -mregexp -t'^Your target has escaped\.$' = \
    /set smashing 1

/def -F -mregexp -t'^You killed (.*)\.$' = \
    /set smashing 1

/def -F -mregexp -t'^You have lost concentration while moving\!$' = \
    /set smashing 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set smashing 1

/def -F -mregexp -t'^You stop defending (.*)\.$' auto_defend = \
    /if (defending) \
        /send defend %defend_target %;\
    /endif

; smash

/def -F -mregexp -t'^You try to smash .* but miss\.$' = \
    /amsg smash: miss

/def -F -mregexp -t'^You only scared' = \
    /amsg smash: weak

/def -F -mregexp -t'^You bonk .* with your' = \
    /amsg smash: weak

/def -F -mregexp -t'^You got a good swing at the' = \
    /amsg smash: weak 

/def -F -mregexp -t'^You skillfully hit the' = \
    /amsg smash: moderate

/def -F -mregexp -t'^You smash .* with a powerful swing' = \
    /amsg smash: moderate

/def -F -mregexp -t'^You smash .* with your .*, nearly' = \
    /amsg smash: severe

/def -F -mregexp -t'^You smash .* to a pulp\!' = \
    /amsg smash: death blow

; smack

/def -F -mregexp -t'^You cover your head .* and run past .*.$' = \
    /amsg smack: miss

/def -F -mregexp -t'^You push your .* into .* but .* pushes it back\.$' = \
    /amsg smack: weak

/def -F -mregexp -t'^You sidestep .* attack and hit .* flat' = \
    /amsg smack: weak

/def -F -mregexp -t'^You whack .* over the head' = \
    /amsg smack: weak 

/def -F -mregexp -t'^With a sharp turn you drive the edge of your' = \
    /amsg smack: moderate

/def -F -mregexp -t'^Using both hands you drive the edge of your' = \
    /amsg smack: moderate

/def -F -mregexp -t'^You charge into .* with your' = \
    /amsg smack: severe

/def -F -mregexp -t'^You run into .*\. Badly shaken' = \
    /amsg smack: death blow

; scare

/def -F -mregexp -t'laughs at you without any fear\.$' = \
    /amsg scare: weak

/def -F -mregexp -t'steps back, but otherwise you didn\'t make big impression\.$' = \
    /amsg scare: weak

/def -F -mregexp -t'jumps in fear of you\.$' = \
    /amsg scare: weak 

/def -F -mregexp -t'loses .* breath, looking at you with fear\.$' = \
    /amsg scare: moderate

/def -F -mregexp -t'staggers in shock, gazing in fear at you\.$' = \
    /amsg scare: moderate

/def -F -mregexp -t'collapses in complete terror of you\.$' = \
    /amsg scare: severe

/def -F -mregexp -t'dies in terror of you\!$' = \
    /amsg scare: death blow

; retreat

/def -F -mregexp -t'^Retreat from whom\?$' = \
    /amsg retreat: no combat

/def -F -mregexp -t'^Taking too much heat, you cry out to .* and then move aside' = \
    /amsg retreat: success

/def -F -mregexp -t'^You are taking too much heat and find yourself unable to retreat' = \
    /amsg retreat: fail %;\
    /repeat -2 1 /send retreat

/def -F -mregexp -t'^You are still too far out in front\. You begin to maneuver yourself' = \
    /amsg retreat: on cooldown %;\
    /repeat -2 1 /send retreat

; recovery

/def -F -t'*pays too much attention to you to be scared*' = /send %angmar_special
/def -F -t'*breath return*' = /send %angmar_special
/def -F -t'*releases his grip on you*' = /send %angmar_special
/def -F -t'*recovered your senses*' = /send %angmar_special
/def -F -t'*You regain consciousness*' = /send %angmar_special
/def -F -t'*recover your balance again*' = /send %angmar_special
/def -F -t'*no longer feel paralyzed*' = /send %angmar_special
/def -F -t'*unable to attack your target*' = /repeat -2 1 /send %angmar_special
/def -F -t'*wrestle back control of your mind*' = /send %angmar_special
/def -F -t'*you managed to shake off the terror*' = /send %angmar_special
