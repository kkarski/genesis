;
; Calian Warriors
;

; variables

/set swarming 1
/set moving_behind 1
/set mb_target none

; aliases

/alias x /send swarm %{*}
/alias c \
    /if (mb_target !~ "none") \
        /send move behind %mb_target %;\
    /endif

/alias cprepare \
    /if ({*} =~ "") \
        /if (team) \
            /set mb_target %leader %;\
        /else \
            /set mb_target none %;\
        /endif %;\
    /else \
        /set mb_target %{*} %;\
    /endif %;\
    /e < move behind target: %mb_target >

; definitions

/def swarming = \
    /if (swarming) \
        /set swarming 0 %;\
        /e @ automatic swarm OFF %;\
    /else \
        /set swarming 1 %;\
        /e @ automatic swarm ON %;\
    /endif

/def moving_behind = \
    /if (moving_behind) \
        /set moving_behind 0 %;\
        /e @ automatic move behind OFF %;\
    /else \
        /set moving_behind 1 %;\
        /e @ automatic move behind ON %;\
    /endif

/def mb = /moving_behind

/def swarm_check = \
    /if (team & swarming) \
        /set swarming 0 %;\
        /send swarm %;\
        /repeat -2 1 /set swarming 1 %;\
    /endif

/def mb_check = \
    /if (team & moving_behind) \
        /set moving_behind 0 %;\
        /send move behind %mb_target %;\
        /repeat -2 1 /set moving_behind 1 %;\
    /endif

/def swarm_msg = \
    /echo %;\
    /echo -aCcyan < swarm: %* > %;\
    /echo

/def mb_msg = \
    /echo %;\
    /echo -aCcyan < move behind: %* > %;\
    /echo

/def riposte_msg = \
    /echo %;\
    /echo -aCcyan < riposte: %* > %;\
    /echo

; triggers

/def -F -mregexp -t'^Your leader is now ([A-Za-z]+)\.$' = \
    cprepare %P1

/def -F -mregexp -t'^([A-Z][a-z]+) leaves (the|your) team' = \
    /if ({P1} =~ mb_target) \
        cprepare %leader %;\
    /endif

/def -F -mregexp -t'^You (leave|disband) (the|your) team' = \
    cprepare none
    
/def -F -mregexp -t'forces you to leave .* team\.$' = \
    cprepare none

/def -F -mregexp -t'disbands .* team and forces you to leave\.$' = \
    cprepare none

/def -F -mregexp -t'^You (attack|assist|turn to attack)' swarm_attack = \
    /swarm_check

/def -F -mregexp -t'attacks you\!$' calian_defend = \
    /repeat -1 1 /mb_check %;\
    /repeat -2 1 /swarm_check

/def -F -mregexp -t'blinks out of the material realm, reappearing behind you\. .* enemies move to attack you\!$' calian_blink_defend = \
    /repeat -1 1 /mb_check %;\
    /repeat -2 1 /swarm_check

/def -F -mregexp -t'^You need to be wielding a weapon in order to swarm\!$' = \
    /swarm_msg no weapon!

/def -F -PBCwhite -mregexp -t'You prepare to swarm\.$' swarm_prepare = \
    /if (swarming) \
        /set swarming 0 %;\
        /repeat -2 1 /set swarming 1 %;\
    /endif

/def -F -PBCwhite -mregexp -t'You prepare to move behind (.*)\.$' mb_prepare = \
    /if (moving_behind) \
        /set moving_behind 0 %;\
        /repeat -2 1 /set moving_behind 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to swarm again\.$' = \
    /swarm_check

/def -F -mregexp -t'^You feel ready to move behind again\.$' = \
    /mb_check

/def -F -mregexp -t'^Whom do you wish to target\?$' = \
    /mb_msg no target %;\
    /set moving_behind 0 %;\
    /repeat -2 1 /set moving_behind 1

/def -F -mregexp -t'^No\-one is attacking you\!$' = \
    /mb_msg not under attack %;\
    /set moving_behind 0 %;\
    /repeat -2 1 /set moving_behind 1

/def -F -mregexp -t'^Attack who\?$' = \
    /set swarming 1

/def -F -mregexp -t'(^You|^> You) do not feel ready to swarm again\.$' = \
    /set swarming 1

/def -F -PCred -mregexp -t'^You have lost concentration while moving\!$' = \
    /set swarming 1

/def -F -mregexp -t'^Your swarm target is not in this room\.$' = \
    /set swarming 1

/def -F -mregexp -t'^The target of your attack seems to have slipped away\.$' = \
    /set swarming 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set swarming 1

/def -F -mregexp -t'^You need to be on a team to swarm\!$' = \
    /swarm_msg no team! %;\
    /if (swarming) \
        /swarming %;\
    /endif

/def -F -mregexp -t'^Your enemies are paying too much attention for you to swarm' = \
    /if (team) \
        /send move behind %mb_target %;\
    /endif

/def -F -mregexp -t'^You can\'t swarm because none of your present team members are fighting\!$' = \
    /set swarming 1 %;\
    /if (team & assisting) \
        /check_assist %;\
    /endif

/def -F -mregexp -t'^Having recently launched an attack you are not yet ready to move behind\.$' = \
    /repeat -2 1 /send move behind %mb_target

/def -F -mregexp -t'^The enemy onslaught is too overwhelming for you to maneuver\.$' = \
    /repeat -1 1 /send move behind %mb_target %;\
    /mb_msg failed

/def -F -PBCwhite -mregexp -t'^You .* move behind (.*) and your enemies .*' = \
    /mb_msg %P1 %;\
    /send swarm

/def -F -mregexp -t'^([A-Z][a-z]+) .* moves behind' = \
    /if (team) \
        /check_assist %;\
    /endif

/def -F -t'*breath return*' = /send swarm
/def -F -t'*releases his grip on you*' = /send swarm
/def -F -t'*recovered your senses*' = /send swarm
/def -F -t'*cannot gather your concentration*' = /repeat -3 1 /send swarm
/def -F -t'*You regain consciousness*' = /send swarm
/def -F -t'*You recover your balance again*' = /send swarm
/def -F -t'*You no longer feel paralyzed*' = /send swarm
/def -F -t'*You are unable to attack your target*' = /send swarm

; hilites

/def -F -PBCyellow -mregexp -t'^([A-Z][a-z]+) projects a thought to you:'

/def -F -P1BCwhite -mregexp -t'With your opponent out of balance, .* (glances off harmlessly|surface wound|painful wound|deep wound|grievous wound|poke a hole)' = \
    /riposte_msg %P1

/def -F -PBCwhite -mregexp -t'^You pounce .*, (.*)\.$' = \
    /swarm_msg %P1

/def -F -PBCwhite -mregexp -t'^You fail to sneak around .* defences\.$' = \
    /swarm_msg miss

/def -F -PBCwhite -mregexp -t'^You hit .* with a thunderous blow of your .*\!$' = \
    /swarm_msg thunderous blow
