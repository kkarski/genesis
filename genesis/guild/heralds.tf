;
; Heralds of the Valar
;

; variables

/set vupkeep 1

; aliases

/alias vcast \
    oh %;\
    /send cast %{*} %;\
    /repeat -15 1 ch
/alias vbuff vcast noleme
/alias vcheck vcast khil %{*}
/alias vcure vcast engwagalad %{*}
/alias vfind vcast rada %{*}
/alias vgarden vcast riel
/alias vhaste vcast hortale
/alias vheal vcast lissinen %{*}
/alias vlight vcast silme
/alias vlist vcast nornore
/alias vmessenger cast ramar
/alias vprot vcast ondo
/alias vres vcast namotir
/alias vscry vcast haetir %{*}
/alias vspells \
    /echo -aCcyan @ Heralds of the Valar spells %;\
    /echo -aCcyan @ vbuff      : increase wisdom %;\
    /echo -aCcyan @ vcheck     : check align %;\
    /echo -aCcyan @ vcure      : cure poison %;\
    /echo -aCcyan @ vfind      : locate herb %;\
    /echo -aCcyan @ vgarden    : grow herb %;\
    /echo -aCcyan @ vhaste     : haste %;\
    /echo -aCcyan @ vheal      : healing %;\
    /echo -aCcyan @ vlight     : light %;\
    /echo -aCcyan @ vlist      : list members %;\
    /echo -aCcyan @ vmessenger : summon messenger %;\
    /echo -aCcyan @ vprot      : harden robe %;\
    /echo -aCcyan @ vres       : resist death spells %;\
    /echo -aCcyan @ vscry      : scry person
/alias vcomponents \
    /echo -aCcyan @ Heralds of the Valar spell components %;\
    /echo -aCcyan @ vbuff      : adillyp weed, river lily %;\
    /echo -aCcyan @ vcheck     : - %;\
    /echo -aCcyan @ vcure      : pawnrose, athelas %;\
    /echo -aCcyan @ vfind      : silver coin %;\
    /echo -aCcyan @ vgarden    : water, opal, ashes %;\
    /echo -aCcyan @ vhaste     : tyelka, evendim grass %;\
    /echo -aCcyan @ vheal      : huckleberry, lothore %;\
    /echo -aCcyan @ vlight     : - %;\
    /echo -aCcyan @ vlist      : - %;\
    /echo -aCcyan @ vmessenger : - %;\
    /echo -aCcyan @ vprot      : fungiarbo, rock %;\
    /echo -aCcyan @ vres       : nasturtium, blackroot %;\
    /echo -aCcyan @ vscry      : parsley

/alias pcomp \
    oh %;\
    ph attanars %;\
    ph suranies %;\
    ph athlys %;\
    ph fungiarbos %;\
    ph parsleys %;\
    ph blackroots %;\
    ph tyelkas %;\
    ph huckleberries %;\
    ph lothores %;\
    ph pawnroses %;\
    ph athelases %;\
    ph evendim grasses %;\
    ph adillyp weeds %;\
    ch

; definitions

/def vupkeep = \
    /if (vupkeep) \
        /set vupkeep 0 %;\
        /e @ herald spell upkeep OFF %;\
    /else \
        /set vupkeep 1 %;\
        /e @ herald spell upkeep ON %;\
    /endif

/def vmessage = \
    /echo %;\
    /e < %{*} > %;\
    /echo

; triggers

/def -F -PCcyan -mregexp -t'^You feel the indomitable strength of the earth surge through your body\.$' = \
    /vmessage valar stoneskin: up

/def -F -PCcyan -mregexp -t'^You feel you won\'t be able to maintain your channeling of Aule for much longer\.$' = \
    /vmessage valar stoneskin: failing %;\
    /if (vupkeep) \
        vprot %;\
    /endif

/def -F -PCcyan -mregexp -t'^You cease channelling Aule, and your body loses its hardness\.$' = \
    /vmessage valar stoneskin: down

/def -F -mregexp -t'^You channel healing energies .* (wounds are|wounds) (.* mended|mended)\.$' = \
    /vmessage valar heal: %{P2}

; hilites

/def -F -PCred -mregexp -t'^You are missing .* to channel .*'
/def -F -PCred -mregexp -t'^You don\'t find anything on the ground that you can plant\.$'
/def -F -PCred -mregexp -t'^To channel the swiftness of the Mearas ou must be mounted on a steed\.$'
/def -F -PCred -mregexp -t'^You are unable to summon your messenger from indoors\.$'
/def -F -PCred -mregexp -t'^Your radiant aura fades and disappears\.$'
/def -F -PCred -mregexp -t'^The glimmering path fades, leaving no traces behind\.$'
/def -F -PCred -mregexp -t'^You fail in your attempt to channel the powers of the Valar\.$'
/def -F -PCred -mregexp -t'^You feel a discordant thrum through your being'

/def -F -PCcyan -mregexp -t'^You sacrifice .*\.$'
/def -F -PCcyan -mregexp -t'^You kneel to the ground and call upon Vana, the Mistress of Flowers and Song'
/def -F -PCcyan -mregexp -t'^The glimmering path of light motes leads .*\.$'
/def -F -PCcyan -mregexp -t'^You close your eyes and channel Manwe, ruler of the sky and Lord of the Valar'
/def -F -PCcyan -mregexp -t'^You beseech Vaire the Weaver for access to her Loom of Time'
/def -F -PCcyan -mregexp -t'^You begin channelling the bonds of friendship and loyalty of Astaldo'
/def -F -PCcyan -mregexp -t'^You chant a prayer to Elbereth, the embodiment of light' 
/def -F -PCcyan -mregexp -t'^Your flawless flowing white robe begins to glow brightly with a radiant aura\!$'
/def -F -PCcyan -mregexp -t'^You raise your hands and call upon Manwe, Lord of the Sky'
/def -F -PCcyan -mregexp -t'^You channel the farsight of Manwe'
/def -F -PCcyan -mregexp -t'^You whisper a prayer, supplicating Lorien to cure the afflicted\.$'
/def -F -PCcyan -mregexp -t'^You channel Lorien to cure .* of poisons and afflictions\.$'
/def -F -PCcyan -mregexp -t'^You begin channelling the blessings of Este, calling upon her powers of renewal and healing'
/def -F -PCcyan -mregexp -t'^You call upon Mandos, Keeper of the Dead, for aid in sending the unliving to the Halls of Awaiting'
/def -F -PCcyan -mregexp -t'^You channel healing energies .*'

; additional scripts and macros
/load ~/scripts/genesis/robots/fangorn_heralds.tf
