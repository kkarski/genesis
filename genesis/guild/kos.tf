;
; Knights of Solamnia
;

; variables

/set kattacking 1
/set charging 0
/set mounted 0
/set drac_disarm 0
/set rescue_target none

; aliases

/alias rr \
    /if ({*} !~ "") \
        /set rescue_target %{*} %;\
        /echo -aCcyan @ rescue target set to: %rescue_target %;\
    /else \
        /echo -aCcyan @ rescuing: %rescue_target %;\
    /endif %;\
    /send rescue %rescue_target

/alias x /send kattack %{*}
/alias c /send charge %{*}
/alias b /send block %{*}
/alias kswitch \
    /kattacking %;\
    /charging
/alias kkey \
    d %;\
    s %; s %; s %; s %; s %; s %; s %; s %;\
    nw %; nw %; nw %; nw %; nw %; nw %; nw %; nw %; nw %; nw %;\
    u %; u %;\
    /send open door %;\
    w %; w %; w %; w %;\
    /send enter alcove %;\
    /send get candle %;\
    /send put candle on shelf %;\
    /repeat -1 1 /send search pallet
/alias kw k _warfare_npc_

; definitions

/def kmsg = \
    /echo %;\
    /echo -aCcyan < %{*} > %;\
    /echo %;\
    /send health all

/def kattacking = \
    /if (kattacking) \
        /set kattacking 0 %;\
        /kmsg kattack: disabled %;\
    /else \
        /set kattacking 1 %;\
        /kmsg kattack: enabled %;\
    /endif

/def charging = \
    /if (charging) \
        /set charging 0 %;\
        /kmsg charge: disabled %;\
    /else \
        /set charging 1 %;\
        /kmsg charge: enabled %;\
    /endif

/def kos_special_check = \
    /if (kattacking) \
        /kattack_check %;\
    /elseif (charging) \
        /charge_check %;\
    /endif

/def kattack_check = \
    /if (kattacking) \
        /set kattacking 0 %;\
        /send kattack %;\
        /repeat -2 1 /set kattacking 1 %;\
    /endif

/def charge_check = \
    /if (charging) \
        /set charging 0 %;\
        /send charge %;\
        /repeat -2 1 /set charging 1 %;\
    /endif

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' = \
    /kos_special_check

/def -F -mregexp -t'attacks you\!$' = \
    /repeat -1 1 /kos_special_check

/def -F -mregexp -t'(turn|turns) to attack you' = \
    /repeat -1 1 /kos_special_check

/def -F -PBCwhite -mregexp -t'^You feel able to focus yourself towards your opponent again\.$' = \
    /kos_special_check

/def -F -PBCwhite -mregexp -t'^([A-Z][a-z]+) is not under attack\.$' = \
    /echo %;\
    /echo -aCcyan < %{P1} is not under attack > %;\
    /echo

/def -F -mregexp -t'^You swing your .* evades the attack\.$' = \
    /kmsg kattack: miss

/def -F -mregexp -t'^You lash out quickly .* and lightly cut' = \
    /kmsg kattack: weak

/def -F -mregexp -t'^(The .*|.*) grabs your wrist' = \
    /kmsg kattack: weak

/def -F -mregexp -t'^You bash .* with the hilt of your' = \
    /kmsg kattack: weak

/def -F -mregexp -t'^You slash deeply into' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'^Spotting an opening in .* defence,' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'^You slash into .* with your' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'^Side-stepping a wayward blow from .*, you' = \
    /kmsg kattack: severe

/def -F -mregexp -t'^With a skillful swing, you open a large gash' = \
    /kmsg kattack: severe

/def -F -mregexp -t'^You confidently lunge forward, driving the blade' = \
    /kmsg kattack: severe

/def -F -mregexp -t'and quickly lash out with your' = \
    /kmsg kattack: weak

/def -F -mregexp -t'^You bash .* with your' = \
    /kmsg kattack: weak

/def -F -mregexp -t'and slice into .* with your' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'^You send .* sprawling as you fiercely slam' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'swoons under the tremendous impact of your' = \
    /kmsg kattack: severe

/def -F -mregexp -t'tear a gaping wound' = \
    /kmsg kattack: severe

/def -F -mregexp -t'and slash deeply into' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'^You whirl your .* around' = \
    /kmsg kattack: weak

/def -F -mregexp -t'^You spur your .* forward' = \
    /kmsg kattack: weak

/def -F -mregexp -t'^Your .* rears back on .* hindlegs' = \
    /kmsg kattack: moderate

/def -F -mregexp -t'^You bring your .* around' = \
    /kmsg kattack: severe

/def -F -t'*breath return*' = /kos_special_check
/def -F -t'*releases*grip on you*' = /kos_special_check
/def -F -t'*recovered your senses*' = /kos_special_check
/def -F -t'*You regain consciousness*' = /kos_special_check
/def -F -t'*You recover your balance again*' = /kos_special_check
/def -F -t'*You no longer feel paralyzed*' = /kos_special_check
/def -F -t'*You are unable to attack your target*' = /kos_special_check
/def -F -t'*You wrestle back control of your mind*' = /kos_special_check
/def -F -'t*You feel you managed to shake off the terror*' = /kos_special_check

/def -F -mregexp -t'^>? ?You do not feel ready to kattack again\.$' = \
    /set kattacking 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set kattacking 1

/def -F -PCred -mregexp -t'^You are too busy casting a spell to kattack\.$' = \
    /repeat -2 1 /send kattack

/def -F -PCred -mregexp -t'^You stop preparing your kattack\.$' = \
    /repeat -2 1 /send kattack

/def -F -mregexp -t'^You are too stunned to kattack\!$' = \
    /repeat -2 1 /send kattack

/def -F -mregexp -t'^[A-Za-z]+ .* moves behind you, and .* enemies move to attack you\!$' = \
    /kos_special_check

/def -F -mregexp -t'^You .* pull yourself onto the' = \
    /set mounted 1 %;\
    /kmsg mounted

/def -F -mregexp -t'^You easily slip out of the saddle, and dismount' = \
    /set mounted 0 %;\
    /kmsg dismounted

/def -F -mregexp -t'^You wield' = \
    /if (drac_disarm) \
        /set drac_disarm 0 %;\
        /kos_special_check %;\
    /endif

/def -F -PCred -mregexp -t'^A Fire\.' = \
    /echo %;\
    /echo -aCred < fire! > %;\
    /echo

/def -F -PCred -mregexp -t'^The grass catches fire\.' = \
    /echo %;\
    /echo -aCred < fire! > %;\
    /echo

/def -F -PBCwhite -mregexp -t'^A .* page arrives jogging and hands you a message' = \
    /echo %;\
    /echo -aCcyan < Messenger! > %;\
    /echo

/def -F -PCcyan -mregexp -t'^Though (.*) seems to be holding .* own against .*' rescue_success = \
    /kmsg %P1 rescued

/def -F -PCred -mregexp -t'^You are not yet ready to rescue again\.$' rescue_repeat = \
    /repeat -2 1 rr

/def -F -PCred -mregexp -t'^You fail to rescue (.*)\.$' rescue_fail = \
    /if (rescue_target =~ none) \
        /set rescue_target %{P1} %;\
    /endif %;\
    rr

/def -F -PCcyan -mregexp -t'>? ?([A-Z][a-z]*) is not under attack\.$ rescue_unnecessary = \
    /if (rescue_target =~ none) \
        /set rescue_target %{P1} %;\
    /endif

; hilites

/def -F -PCcyan -mregexp -t'You feign an attack with your .*, distracting .*'
/def -F -PCcyan -mregexp -t'(The .*|.*) swings clumsily, leaving an opening .*'
/def -F -PCcyan -mregexp -t'You skillfully parry .* attack, .*'
/def -F -PCcyan -mregexp -t'Dodging .* blow, you move in to .*'
/def -F -PCcyan -mregexp -t'You duck a fierce swing from .*, leaving .*'
/def -F -PCcyan -mregexp -t'You duck a fierce swing from .*, leaving .*'
/def -F -PCred -mregexp -t'^A spark of fire comes flying in\.$'
/def -F -PBCwhite -mregexp -t'^The spark burns out\.$'

; progress

/def -F -PBCwhite -mregexp -t'^You feel your deeds and actions of late have left you .*'
/def -F -mregexp -t'very recently promoted, and not deserving' = \
    /kmsg progress: 0/5
/def -F -mregexp -t'recently promoted, and not yet ready' = \
    /kmsg progress: 1/5
/def -F -mregexp -t'an established member of your current rank' = \
    /kmsg progress: 2/5
/def -F -mregexp -t'one who will soon be worthy of promotion' = \
    /kmsg progress: 3/5
/def -F -mregexp -t'deserving of a promotion' = \
    /kmsg progress 4/5

/def -F -PBCwhite -mregexp -t'^Your prestige has raised to enough to let you get a higher rank\. Congratulations\.' = \
    /kmsg Rank increased!
