;
; School of High Magic
;

; variables

/set magic_missile_name Arcanis vaes
/set acid_arrow_name Uveluca Vaes
/set fire_dart_name Api Anak
/set ballistic_spray_name Tonash ingis
/set prismatic_spray_name Bakmada Ingis
/set minors_rotation arrowmissile
/set spellcasting 1
/set magic_missile 1
/set acid_arrow 1
/set fire_dart 1
/set ballistic_spray 1
/set prismatic_spray 1
/set supkeep 1
/set component_cointainer sash

; aliases

/alias x /spellcasting_check
/alias scomps \
    /send open %component_container %;\
    /send get %{*} from %component_container %;\
    /send close %component_container
/alias smissile /send cast arcanisvaes
/alias sarrow /send cast uvelucavaes
/alias sdart /send cast apianak
/alias sbspray /send cast tonashingis
/alias spspray /send cast bakmadaingis
/alias stell /send cast suacovisp %{*}
/alias sshield scomps chokecherry %; /send cast arcanfethos
/alias slight /send cast talkarpas
/alias srotation \
    /set minors_rotation $[strcat({1}, {2}, {3})] %;\
    /e @ spell rotation set to: %{1}, %{2}, %{3}

; definitions

/def spellcasting = \
    /if (spellcasting) \
        /set spellcasting 0 %;\
        /e @ spellcasting OFF %;\
    /else \
        /set spellcasting 1 %;\
        /e @ spellcasting ON %;\
    /endif

/def supkeep = \
    /if (supkeep) \
        /set supkeep 0 %;\
        /e @ spell upkeep OFF %;\
    /else \
        /set supkeep 1 %;\
        /e @ spell upkeep ON %;\
    /endif

/def spellcasting_check = \
    /if (spellcasting) \
        /set spellcasting 0 %;\
        /cast_offensive_minor %;\
        /repeat -1 1 /set spellcasting 1 %;\
    /endif

/def cast_offensive_minor = \
    /if (regmatch('bspray', minors_rotation) & ballistic_spray) \
        sbspray %;\
    /elseif (regmatch('arrow', minors_rotation) & acid_arrow) \
        sarrow %;\
    /elseif (regmatch('dart', minors_rotation) & fire_dart) \
        sdart %;\
    /elseif (regmatch('missile', minors_rotation) & magic_missile) \
        smissile %;\
    /elseif (regmatch('pspray', minors_rotation) & prismatic_spray) \
        spspray %;\
    /else \
;       do nothing
    /endif

/def spell_msg = \
    /echo %;\
    /echo -aCcyan < %{*} > %;\
    /echo

; triggers

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set spellcasting 1

/def -F -mregexp -t'^Nobody here to cast the spell on\!$' = \
    /set spellcasting 1

/def -F -mregexp -t'^You have lost concentration while moving\!$' = \
    /set spellcasting 1

/def -F -mregexp -t'^You are too busy casting' = \
    /set spellcasting 1

/def -F -mregexp -t'^You are currently casting .*\!$' = \
    /set spellcasting 0 %;\
    /repeat -2 1 /set spellcasting 1

/def -F -mregexp -t'^You (attack|assist|turn to attack)' spell_attack = \
    /spellcasting_check

/def -F -mregexp -t'attacks you\!$' spell_defend = \
    /repeat -1 1 /spellcasting_check

/def -F -t'*breath return*' = /spellcasting_check
/def -F -t'*releases*grip on you*' = /spellcasting_check
/def -F -t'*recovered your senses*' = /spellcasting_check
/def -F -t'*You regain consciousness*' = /spellcasting_check
/def -F -t'*You are unable to attack your target*' = /spellcasting_check

/def -F -P1Ccyan -mregexp -t'^You close your eyes and begin reciting from memory powerful words of magic, \'(.*)\!\'' = \
    /if (magic_missile_name =~ {P1}) \
        /set magic_missile 0 %;\
    /elseif (acid_arrow_name =~ {P1}) \
        /set acid_arrow 0 %;\
    /elseif (fire_dart_name =~ {P1}) \
        /set fire_dart 0 %;\
    /elseif (ballistic_spray_name =~ {P1}) \
        /set ballistic_spray 0 %;\
    /elseif (prismatic_spray_name =~ {P1}) \
        /set prismatic_spray 0 %;\
    /endif

/def -F -PCcyan -mregexp -t'^You feel ready to conjure a magic missile again\.$' = \
    /set magic_missile 1
/def -F -PCcyan -mregexp -t'^You feel ready to cast prismatic spray again\.$' = \
    /set prismatic_spray 1
/def -F -PCcyan -mregexp -t'^You feel ready to conjure a spray of ballistics again\.$' = \
    /set ballistic_spray 1
/def -F -PCcyan -mregexp -t'^You feel ready to conjure a fire dart again\.$' = \
    /set fire_dart 1
/def -F -PCcyan -mregexp -t'^You feel ready to conjure an acid arrow again\.$' = \
    /set acid_arrow 1

/def -F -PCred -mregexp -t'^You fail in your efforts to invoke the spell\.$' = \
    /set spellcasting 1 %;\
    /spellcasting_check

/def -F -mregexp -t'^You feel ready to (cast|conjure)' = \
    /repeat -1 1 /spellcasting_check

/def -F -mregexp -t'^You point a crooked finger towards' = \
    /spellcasting_check

/def -F -mregexp -t'^You extend your hand with fingers fanning outwards' = \
    /spellcasting_check

/def -F -mregexp -t'^You thrust a closed fist towards' = \
    /spellcasting_check

/def -F -mregexp -t'^With a flick your wrist you make an arcane gesture towards' = \
    /spellcasting_check

/def -F -mregexp -t'^You hurl your open hands towards' = \
    /spellcasting_check

/def -F -PCcyan -mregexp -t'^A shimmering shield of magical force surrounds you, protecting you from damage\.$' = \
    /spell_msg shield: up

/def -F -PCcyan -mregexp -t'^You feel the magical wards maintaining your magic shield spell beginning to weaken\.$' = \
    /spell_msg shield: failing %;\
    /if (supkeep) \
        sshield %;\
    /endif

/def -F -PCcyan -mregexp -t'^The shimmering shield of magical force dissipates\.$' = \
    /spell_msg shield: down

/def -F -PCcyan -mregexp -t'^You renew the duration of the effect\!$' = \
    /spell_msg upkeep spell: renewed

; hilites

/def -F -P1Ccyan -mregexp -t'^>? ?You .* \'(.*)\!\''

/def -F -mregexp -t'^A bolt of magical energy slams in to' = \
    /spell_msg missile: painful crush
/def -F -mregexp -t'^Three bolts of magical energy violently smash into' = \
    /spell_msg missile: incredible damage
/def -F -mregexp -t'^Three bolts of magical energy explode in to' = \
    /spell_msg missile: grievous damage
/def -F -mregexp -t'^A bolt of magical energy hits .*, inflicting (.*)\.$' = \
    /spell_msg missile: %{P1}

/def -F -mregexp -t'^A beam of multicoloured light deflects' = \
    /spell_msg prismatic spray: tingling pain
/def -F -mregexp -t'^A beam of multicoloured light hits' = \
    /spell_msg prismatic spray: numbing pain
/def -F -mregexp -t'^A prismatic beam of light smashes in to' = \
    /spell_msg prismatic spray: radiating pain
/def -F -mregexp -t'^A prismatic beam of light explodes in to (.*) in a kaleidoscope of colours' = \
    /spell_msg prismatic spray: excruciating pain
/def -F -mregexp -t'fatally overloading (.*) senses with radiating pain' = \
    /spell_msg prismatic spray: fatal overload

/def -F -mregexp -t'^Charged projectiles slam in to' = \
    /spell_msg ballistic spray: painful crush
/def -F -mregexp -t'^Many summoned projectiles violently smash into' = \
    /spell_msg ballistic spray: incredible damage
/def -F -mregexp -t'^Many summoned projectiles explode in to' = \
    /spell_msg ballistic spray: grievous damage

/def -F -mregexp -t'^A fiery dart explodes in to' = \
    /spell_msg fire dart: painful burn
/def -F -mregexp -t'^A fiery dart explodes into' = \
    /spell_msg fire dart: incredible damage
/def -F -mregexp -t'^A seething bolt of fire explodes into' = \
    /spell_msg fire dart: fatal damage

/def -F -mregexp -t'^An acid arrow hits' = \
    /spell_msg acid arrow: mildly irritating
/def -F -mregexp -t'^An arrow of acid explodes in to' = \
    /spell_msg acid arrow: painful blistering
/def -F -mregexp -t'^A caustic acid arrow explodes into' = \
    /spell_msg acid arrow: painful burns
/def -F -mregexp -t'^A seething arrow of acid explodes in to' = \
    /spell_msg acid arrow: dissolution
