;
; Gladiators
;

; variables

/set charging 1
/set berserking 0
/set charge_miss 0
/set charge_no_damage 0
/set charge_weak 0
/set charge_moderate 0
/set charge_severe 0
/set charge_kill 0
/set total_charges 0

; aliases

/alias x /send charge %{*}
/alias c /send berserk
/alias gstat /gstat
/alias greset /greset

; definitions

/def charging = \
    /if (charging) \
        /set charging 0 %;\
        /e @ automatic charge OFF %;\
    /else \
        /set charging 1 %;\
        /e @ automatic charge ON %;\
    /endif

/def charge_check = \
    /if (charging) \
        /set charging 0 %;\
        /send charge %;\
        /repeat -2 1 /set charging 1 %;\
    /endif

/def berserking = \
    /if (berserking) \
        /set berserking 0 %;\
        /e @ automatic berserk OFF %;\
    /else \
        /set berserking 1 %;\
        /e @ automatic berserk ON %;\
    /endif

/def berserk_check = \
    /if (berserking) \
        /set berserking 0 %;\
        /send berserk %;\
        /repeat -2 1 /set berserking 1 %;\
    /endif

/def charge_msg = \
	/echo %;\
        /echo -aCcyan < charge: %{*} > %;\
	/echo %;\
    /send health all

/def berserk_msg = \
	/echo %;\
        /echo -aCcyan < berserk: %{*} > %;\
	/echo

; triggers

/def -F -mregexp -t'^Hail, oh Gladiator\!' glad_login = \
    /repeat -1 1 /send wear cuirasse

/def -F -mregexp -t'^You (attack|assist|turn to attack)' charge_attack = \
    /charge_check

/def -F -mregexp -t'attacks you\!$' charge_defend = \
    /repeat -1 1 /charge_check

/def -F -PBCwhite -mregexp -t'^You prepare to execute the lethal attack\.$' charge_prepare = \
    /if (charging) \
        /set charging 0 %;\
        /repeat -2 1 /set charging 1 %;\
    /endif

/def -F -mregexp -t'^Your breathing becomes heavier\.$' berserk_prepare = \
    /if (berserking) \
        /set berserking 0 %;\
        /repeat -2 1 /set berserking 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to crush your foe again\.$' = \
    /charge_check

/def -F -mregexp -t'^You can now enter a berserked state again\.$' = \
    /berserk_check

/def -F -mregexp -t'^You are foaming at the mouth as it is\.$' = \
    /set berserking 1

/def -F -mregexp -t'(^You|^> You) do not feel ready to charge again\.$' = \
    /set charging 1

/def -F -mregexp -t'^The target of your attack seems to have slipped away\.$' = \
    /set charging 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set charging 1

/def -F -mregexp -t'^You decide to abandon' = \
    /set charging 1

/def -F -mregexp -t'^You abandon your charge as your foe is no longer present\.$' = \
    /set charging 1

/def -F -mregexp -t'^But you aren\'t in battle with anyone\!$' = \
    /set charging 1

/def -F -mregexp -t'^You are now hunted' = \
    /set charging 1

/def -F -t'*breath return*' = /send charge
/def -F -t'*releases*grip on you*' = /send charge
/def -F -t'*recovered your senses*' = /send charge
/def -F -t'*regain consciousness*' = /send charge
/def -F -t'*recover your balance again*' = /send charge
/def -F -t'*no longer feel paralyzed*' = /send charge
/def -F -t'*unable to attack your target*' = /repeat -2 1 /send charge
/def -F -t'*wrestle back control of your mind*' = /send charge
/def -F -t'*you managed to shake off the terror*' = /send charge

; statistics

/def greset = \
    /set charge_miss 0 %;\
    /set charge_no_damage 0 %;\
    /set charge_weak 0 %;\
    /set charge_moderate 0 %;\
    /set charge_severe 0 %;\
    /set charge_kill 0 %;\
    /set total_charges 0

/def count_charge_ratio = \
    /if ({*} = 0) \
      /set charge_ratio 0 %;\
    /else \
      /set charge_ratio $[100-(((total_charges-{*})*100)/total_charges)] %;\
    /endif

/def prepare_charge_graph = \
    /if (charge_ratio = 0) \
        /set charge_graph .............................. %;\
    /elseif ((charge_ratio >0) & (charge_ratio < 4)) \
        /set charge_graph #............................. %;\
    /elseif ((charge_ratio > 3) & (charge_ratio < 7)) \
        /set charge_graph ##............................ %;\
    /elseif ((charge_ratio > 6) & (charge_ratio < 11)) \
        /set charge_graph ###........................... %;\
    /elseif ((charge_ratio > 10) & (charge_ratio < 14)) \
        /set charge_graph ####.......................... %;\
    /elseif ((charge_ratio > 13) & (charge_ratio < 17)) \
        /set charge_graph #####......................... %;\
    /elseif ((charge_ratio > 16) & (charge_ratio < 21)) \
        /set charge_graph ######........................ %;\
    /elseif ((charge_ratio > 20) & (charge_ratio < 24)) \
        /set charge_graph #######....................... %;\
    /elseif ((charge_ratio > 23) & (charge_ratio < 27)) \
        /set charge_graph ########...................... %;\
    /elseif ((charge_ratio > 26) & (charge_ratio < 31)) \
        /set charge_graph #########..................... %;\
    /elseif ((charge_ratio > 30) & (charge_ratio < 34)) \
        /set charge_graph ##########.................... %;\
    /elseif ((charge_ratio > 33) & (charge_ratio < 37)) \
        /set charge_graph ###########................... %;\
    /elseif ((charge_ratio > 36) & (charge_ratio < 41)) \
        /set charge_graph ############.................. %;\
    /elseif ((charge_ratio > 40) & (charge_ratio < 44)) \
        /set charge_graph #############................. %;\
    /elseif ((charge_ratio > 43) & (charge_ratio < 47)) \
        /set charge_graph ##############................ %;\
    /elseif ((charge_ratio > 46) & (charge_ratio < 51)) \
        /set charge_graph ###############............... %;\
    /elseif ((charge_ratio > 50) & (charge_ratio < 54)) \
        /set charge_graph ################.............. %;\
    /elseif ((charge_ratio > 53) & (charge_ratio < 57)) \
        /set charge_graph #################............. %;\
    /elseif ((charge_ratio > 56) & (charge_ratio < 61)) \
        /set charge_graph ##################............ %;\
    /elseif ((charge_ratio > 60) & (charge_ratio < 64)) \
        /set charge_graph ###################........... %;\
    /elseif ((charge_ratio > 63) & (charge_ratio < 67)) \
        /set charge_graph ####################.......... %;\
    /elseif ((charge_ratio > 66) & (charge_ratio < 71)) \
        /set charge_graph #####################......... %;\
    /elseif ((charge_ratio > 70) & (charge_ratio < 74)) \
        /set charge_graph ######################........ %;\
    /elseif ((charge_ratio > 73) & (charge_ratio < 77)) \
        /set charge_graph #######################....... %;\
    /elseif ((charge_ratio > 76) & (charge_ratio < 81)) \
        /set charge_graph ########################...... %;\
    /elseif ((charge_ratio > 80) & (charge_ratio < 84)) \
        /set charge_graph #########################..... %;\
    /elseif ((charge_ratio > 83) & (charge_ratio < 87)) \
        /set charge_graph ##########################.... %;\
    /elseif ((charge_ratio > 86) & (charge_ratio < 91)) \
        /set charge_graph ###########################... %;\
    /elseif ((charge_ratio > 90) & (charge_ratio < 94)) \
        /set charge_graph ############################.. %;\
    /elseif ((charge_ratio > 93) & (charge_ratio < 97)) \
        /set charge_graph #############################. %;\
    /elseif ((charge_ratio > 96) & (charge_ratio < 101)) \
        /set charge_graph ############################## %;\
    /else \
        /echo -aBCred *** ERROR: value out of range *** %;\
    /endif

/def -F -t'*You strike * with your*' = /set total_charges=$[total_charges+1]
/def -F -t'*You miss * completely*' = \
    /set total_charges $[total_charges+1] %;\
    /set charge_miss $[charge_miss+1] %;\
    /charge_msg miss
/def -F -t'*looks amazed by such a feeble attack*' = \
    /set charge_no_damage $[charge_no_damage+1] %;\
    /charge_msg no damage
/def -F -t'*looks dazed*' = \
    /set charge_weak $[charge_weak+1] %;\
    /charge_msg weak
/def -F -t'*looks injured*' = \
    /set charge_moderate $[charge_moderate+1] %;\
    /charge_msg moderate
/def -F -t'*looks hurt*' = \
    /set charge_moderate $[charge_moderate+1] %;\
    /charge_msg moderate
/def -F -t'*looks rather hurt*' = \
    /set charge_moderate $[charge_moderate+1] %;\
    /charge_msg moderate
/def -F -t'*looks severely hurt*' = \
    /set charge_severe $[charge_severe+1] %;\
    /charge_msg severe
/def -F -t'*looks on the verge of collapse*' = \
    /set charge_severe $[charge_severe+1] %;\
    /charge_msg severe
/def -F -t'*Bones break and blood splatters as you devastate*' = \
    /set total_charges $[total_charges+1] %;\
    /set charge_kill $[charge_kill+1] %;\
    /charge_msg kill

/def gstat = \
    /if (total_charges > 0) \
        /let charge_hit $[total_charges-charge_miss] %;\
    /else \
        /let charge_hit 0 %;\
    /endif %;\
    /echo -aBCcyan @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %;\
    /echo -aBCcyan @@@@ ---------------------------------------------- @@@@ %;\
    /echo -aBCcyan @@@@ |     C H A R G E    S T A T I S T I C S     | @@@@ %;\
    /echo -aBCcyan @@@@ ---------------------------------------------- @@@@ %;\
    /echo -aBCcyan @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ Total charges                                   : %total_charges %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /count_charge_ratio %charge_hit %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ Hit  $[pad(charge_ratio, 3)]%%       %charge_graph  : %charge_hit %;\
    /count_charge_ratio %charge_miss %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ Miss $[pad(charge_ratio, 3)]%%       %charge_graph  : %charge_miss %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /count_charge_ratio %charge_no_damage %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ No damage       %charge_graph  : %charge_no_damage %;\
    /count_charge_ratio %charge_weak %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ Weak            %charge_graph  : %charge_weak %;\
    /count_charge_ratio %charge_moderate %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ Moderate        %charge_graph  : %charge_moderate %;\
    /count_charge_ratio %charge_severe %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ Severe          %charge_graph  : %charge_severe %;\
    /count_charge_ratio %charge_kill %;\
    /prepare_charge_graph %;\
    /echo -aBCcyan @ Death blow      %charge_graph  : %charge_kill %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ Statistics generated $[ftime("%a %b %d %X %Y", time())] %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\

; messages

/def -F -PBCwhite -mregexp -t'^Your breathing becomes heavier\.$' = \
    /berserk_msg windup
/def -F -PBCwhite -mregexp -t'^You start slavering at the mouth\.$' = \
    /berserk_msg start
/def -F -PBCwhite -mregexp -t'^You begin to calm down and return to a normal frame of mind\.$' = \
    /berserk_msg end
/def -F -PCred -mregexp -t'^You feel (amazed by such a feeble attack|dazed|injured|hurt|rather hurt|severely hurt|on the verge of collapse)'
