;
; Pirates of Bloodsea
;

; variables

/set pbashing 1

; aliases

/alias z /send pbash %{*}
/alias ploot \
    /send drop monkey %;\
    /send collect

; definitions

/def pbashing = \
    /if (pbashing) \
        /set pbashing 0 %;\
        /e @ automatic pbash OFF %;\
    /else \
        /set pbashing 1 %;\
        /e @ automatic pbash ON %;\
    /endif

/def pbash_check = \
    /if (pbashing) \
        /set pbashing 0 %;\
        /send pbash %;\
        /repeat -2 1 /set pbashing 1 %;\
    /endif

/def pbash_message = \
    /echo %;\
    /echo -aCcyan < pbash: %{*} > %;\
    /echo %;\
    /send health all

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' pbash_attack = \
    /pbash_check

/def -F -mregexp -t'attacks you\!$' pbash_defend = \
    /repeat -1 1 /pbash_check

/def -F -PBCwhite -mregexp -t'^You start to consider using your bottle for something other than drinking\.' pbash_prepare = \
    /if (pbashing) \
        /set pbashing 0 %;\
        /repeat -2 1 /set pbashing 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to hit someone with your bottle again\.' = \
    /pbash_check

/def -F -mregexp -t'^Whom do you wish to target\?$' = \
    /set pbashing 1

/def -F -mregexp -t'^(You|> You) are already preparing to pbash\.$' = \
    /set pbashing 1

/def -F -mregexp -t'^(You|> You) do not feel ready to hit someone with your bottle again\.$' = \
    /set pbashing 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set pbashing 1

/def -F -mregexp -t'^The target of your bottle is no longer here\.$' = \
    /set pbashing 1

/def -F -mregexp -t'^You do not have a bottle to use for the attack\.$' = \
    /if (pbashing) \
        /pbashing %;\
    /endif

/def -F -t'*breath return*' = /send pbash
/def -F -t'*releases his grip on you*' = /send pbash
/def -F -t'*recovered your senses*' = /send pbash
/def -F -t'*cannot gather your concentration*' = /repeat -3 1 /send pbash
/def -F -t'*You regain consciousness*' = /send pbash
/def -F -t'*You recover your balance again*' = /send pbash
/def -F -t'*You no longer feel paralyzed*' = /send pbash
/def -F -t'*You are unable to attack your target*' = /send pbash

/def -F -mregexp -t'^Raising your bottle, you charge straight at' = \
    /pbash_message miss
/def -F -mregexp -t'^Holding your bottle tight, you swing it at' = \
    /pbash_message weak
/def -F -mregexp -t'^In an effort to distract .*, you toss your bottle up' = \
    /pbash_message moderate
/def -F -mregexp -t'^Snapping out of your drunken haze, you lunge at' = \
    /pbash_message moderate
/def -F -mregexp -t'^As .* moves in to strike you, you take a swig' = \
    /pbash_message moderate
/def -F -mregexp -t'^With a bloodthirsty cry of: .* you bring your bottle' = \
    /pbash_message severe
/def -F -mregexp -t'^Grabbing your bottle, you hit .* hard over the head\.$' = \
    /pbash_message severe
