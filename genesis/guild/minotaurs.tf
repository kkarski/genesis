;
; Minotaurs
;

; variables

/set goring 1
/set gore_miss 0
/set gore_no_damage 0
/set gore_weak 0
/set gore_moderate 0
/set gore_severe 0
/set gore_kill 0
/set total_gores 0

; aliases

/alias z /send gore %{*}
/alias mstat /mstat
/alias mreset /mreset

; definitions

/def goring = \
    /if (goring) \
        /set goring 0 %;\
        /e @ automatic goring OFF %;\
    /else \
        /set goring 1 %;\
        /e @ automatic goring ON %;\
    /endif

/def gore_check = \
    /if (goring) \
        /set goring 0 %;\
        /send gore %;\
        /repeat -2 1 /set goring 1 %;\
    /endif

/def gore_msg = \
    /echo %;\
    /echo -aCcyan < gore: %{*} > %;\
    /echo %;\
    /send health all

; triggers

/def -F -mregexp -t'^You finally recover' mino_login = \
    /repeat -1 1 /send wear thick ring

/def -F -mregexp -t'^You (attack|assist|turn to attack)' gore_attack = \
    /gore_check

/def -F -mregexp -t'attacks you\!$' gore_defend = \
    /repeat -1 1 /gore_check

/def -F -PBCwhite -mregexp -t'^You (prepare to gore|raise your horned head and roar) .*' gore_prepare = \
    /if (goring) \
        /set goring 0 %;\
        /repeat -2 1 /set goring 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to gore your foes again\!$' = \
    /gore_check

/def -F -mregexp -t'(^You|^> You) do not feel ready to gore again\.$' = \
    /set goring 1

/def -F -mregexp -t'^The target of your attack seems to have slipped away\.$' = \
    /set goring 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set goring 1

/def -F -t'*breath return*' = /send gore
/def -F -t'*releases*grip on you*' = /send gore
/def -F -t'*recovered your senses*' = /send gore
/def -F -t'*You regain consciousness*' = /send gore
/def -F -t'*You recover your balance again*' = /send gore
/def -F -t'*You are no longer paralyzed*' = /send gore
/def -F -t'*You are unable to attack your target*' = /repeat -2 1 /send gore

; messages

/def -F -mregexp -t'^You charge past .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_miss $[gore_miss+1] %;\
    /gore_msg miss
/def -F -mregexp -t'^You snarl in frustration as .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_no_damage $[gore_no_damage+1] %;\
    /gore_msg no damage
/def -F -mregexp -t'^You snort in annoyance as you just graze .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_weak $[gore_weak+1] %;\
    /gore_msg weak
/def -F -mregexp -t'^You lightly rake your .* along .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_weak $[gore_weak+1] %;\
    /gore_msg weak
/def -F -mregexp -t'^You thrust your .* deeply into .*\.$' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_moderate $[gore_moderate+1] %;\
    /gore_msg moderate
/def -F -mregexp -t'^You viciously whip your .* across .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_moderate $[gore_moderate+1] %;\
    /gore_msg moderate
/def -F -mregexp -t'^You impale .*\'s .* with your .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_moderate $[gore_moderate+1] %;\
    /gore_msg moderate
/def -F -mregexp -t'^With a roar, you thrust your .* repeatedly into .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_severe $[gore_severe+1] %;\
    /gore_msg severe
/def -F -mregexp -t'^You bellow victoriously as you smash .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_severe $[gore_severe+1] %;\
    /gore_msg severe
/def -F -mregexp -t'^You thrust your .* deeply into .*, .*' = \
    /set total_gores $[total_gores+1] %;\
    /set gore_kill $[gore_kill+1] %;\
    /gore_msg kill

; statistics

/def mreset = \
    /set gore_miss 0 %;\
    /set gore_no_damage 0 %;\
    /set gore_weak 0 %;\
    /set gore_moderate 0 %;\
    /set gore_severe 0 %;\
    /set gore_kill 0 %;\
    /set total_gores 0

/def count_gore_ratio = \
    /if ({*} == 0) \
      /set gore_ratio 0 %;\
    /else \
      /set gore_ratio $[100-(((total_gores-{*})*100)/total_gores)] %;\
    /endif

/def prepare_gore_graph = \
    /if (gore_ratio = 0) \
        /set gore_graph .............................. %;\
    /elseif ((gore_ratio >0) & (gore_ratio < 4)) \
        /set gore_graph #............................. %;\
    /elseif ((gore_ratio > 3) & (gore_ratio < 7)) \
        /set gore_graph ##............................ %;\
    /elseif ((gore_ratio > 6) & (gore_ratio < 11)) \
        /set gore_graph ###........................... %;\
    /elseif ((gore_ratio > 10) & (gore_ratio < 14)) \
        /set gore_graph ####.......................... %;\
    /elseif ((gore_ratio > 13) & (gore_ratio < 17)) \
        /set gore_graph #####......................... %;\
    /elseif ((gore_ratio > 16) & (gore_ratio < 21)) \
        /set gore_graph ######........................ %;\
    /elseif ((gore_ratio > 20) & (gore_ratio < 24)) \
        /set gore_graph #######....................... %;\
    /elseif ((gore_ratio > 23) & (gore_ratio < 27)) \
        /set gore_graph ########...................... %;\
    /elseif ((gore_ratio > 26) & (gore_ratio < 31)) \
        /set gore_graph #########..................... %;\
    /elseif ((gore_ratio > 30) & (gore_ratio < 34)) \
        /set gore_graph ##########.................... %;\
    /elseif ((gore_ratio > 33) & (gore_ratio < 37)) \
        /set gore_graph ###########................... %;\
    /elseif ((gore_ratio > 36) & (gore_ratio < 41)) \
        /set gore_graph ############.................. %;\
    /elseif ((gore_ratio > 40) & (gore_ratio < 44)) \
        /set gore_graph #############................. %;\
    /elseif ((gore_ratio > 43) & (gore_ratio < 47)) \
        /set gore_graph ##############................ %;\
    /elseif ((gore_ratio > 46) & (gore_ratio < 51)) \
        /set gore_graph ###############............... %;\
    /elseif ((gore_ratio > 50) & (gore_ratio < 54)) \
        /set gore_graph ################.............. %;\
    /elseif ((gore_ratio > 53) & (gore_ratio < 57)) \
        /set gore_graph #################............. %;\
    /elseif ((gore_ratio > 56) & (gore_ratio < 61)) \
        /set gore_graph ##################............ %;\
    /elseif ((gore_ratio > 60) & (gore_ratio < 64)) \
        /set gore_graph ###################........... %;\
    /elseif ((gore_ratio > 63) & (gore_ratio < 67)) \
        /set gore_graph ####################.......... %;\
    /elseif ((gore_ratio > 66) & (gore_ratio < 71)) \
        /set gore_graph #####################......... %;\
    /elseif ((gore_ratio > 70) & (gore_ratio < 74)) \
        /set gore_graph ######################........ %;\
    /elseif ((gore_ratio > 73) & (gore_ratio < 77)) \
        /set gore_graph #######################....... %;\
    /elseif ((gore_ratio > 76) & (gore_ratio < 81)) \
        /set gore_graph ########################...... %;\
    /elseif ((gore_ratio > 80) & (gore_ratio < 84)) \
        /set gore_graph #########################..... %;\
    /elseif ((gore_ratio > 83) & (gore_ratio < 87)) \
        /set gore_graph ##########################.... %;\
    /elseif ((gore_ratio > 86) & (gore_ratio < 91)) \
        /set gore_graph ###########################... %;\
    /elseif ((gore_ratio > 90) & (gore_ratio < 94)) \
        /set gore_graph ############################.. %;\
    /elseif ((gore_ratio > 93) & (gore_ratio < 97)) \
        /set gore_graph #############################. %;\
    /elseif ((gore_ratio > 96) & (gore_ratio < 101)) \
        /set gore_graph ############################## %;\
    /else \
        /echo -aBCred *** ERROR: value out of range *** %;\
    /endif

/def mstat = \
    /if (total_gores > 0) \
        /let gore_hit $[total_gores-gore_miss] %;\
    /else \
        /let gore_hit 0 %;\
    /endif %;\
    /echo -aBCcyan @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %;\
    /echo -aBCcyan @@@@ ---------------------------------------------- @@@@ %;\
    /echo -aBCcyan @@@@ |       G O R E    S T A T I S T I C S       | @@@@ %;\
    /echo -aBCcyan @@@@ ---------------------------------------------- @@@@ %;\
    /echo -aBCcyan @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ Total gores                                     : %total_gores %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /count_gore_ratio %gore_hit %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ Hit  $[pad(gore_ratio, 3)]%%       %gore_graph  : %gore_hit %;\
    /count_gore_ratio %gore_miss %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ Miss $[pad(gore_ratio, 3)]%%       %gore_graph  : %gore_miss %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /count_gore_ratio %gore_no_damage %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ No damage       %gore_graph  : %gore_no_damage %;\
    /count_gore_ratio %gore_weak %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ Weak            %gore_graph  : %gore_weak %;\
    /count_gore_ratio %gore_moderate %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ Moderate        %gore_graph  : %gore_moderate %;\
    /count_gore_ratio %gore_severe %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ Severe          %gore_graph  : %gore_severe %;\
    /count_gore_ratio %gore_kill %;\
    /prepare_gore_graph %;\
    /echo -aBCcyan @ Death blow      %gore_graph  : %gore_kill %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @ %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ Statistics generated $[ftime("%a %b %d %X %Y", time())] %;\
    /echo -aBCcyan @ %;\
    /echo -aBCcyan @ ---------------------------------------------------- @
