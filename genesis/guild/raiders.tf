;
; Raiders
;

; variables

/set wounding 1
/set mounted 0

; aliases

/alias z /send wound %{*}
/alias rsummon /send blow whistle
/alias rdismiss /send dismiss worg
/alias rfeed /send feed worg with %{*}

; macros

/def rmsg = \
    /echo %;\
    /echo -aCcyan < $[strip_attr({*})] > %;\
    /echo

/def wound_msg = \
    /rmsg wound: %{*} %;\
    /send health all

/def wounding = \
    /if (wounding) \
        /set wounding 0 %;\
        /rmsg automatic wounding OFF %;\
    /else \
        /set wounding 1 %;\
        /rmsg automatic wounding ON %;\
    /endif

/def wound_check = \
    /if (wounding) \
        /set wounding 0 %;\
        /send wound %;\
        /repeat -2 1 /set wounding 1 %;\
    /endif

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' raider_attack = \
    /wound_check

/def -F -mregexp -t'attacks you\!$' raider_defend = \
    /repeat -1 1 /wound_check

/def -F -mregexp -t'^You feel ready to cause a bleeding wound again\.$' = \
    /wound_check

/def -F -mregexp -t'^Your muscles tense eagerly as you prepare to cause a bleeding wound\.$' raider_prepare = \
    /if (wounding) \
        /set wounding 0 %;\
        /repeat -2 1 /set wounding 1 %;\
    /endif

/def -F -mregexp -t'^The target of your bleeding wound attack seems to have slipped away\.$' = \
    /set wounding 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set wounding 1

/def -F -mregexp -t'^You have lost concentration while moving\!$' = \
    /set wounding 1

/def -F -mregexp -t'^(You|> You) are not yet ready to cause a bleeding wound on your enemy\. Patience\!$' = \
    /set wounding 1

/def -F -mregexp -t'^You strike out furiously' = \
    /wound_msg miss

/def -F -mregexp -t'you are unable to cause a wound to .*\.$' = \
    /wound_msg unable to cause a wound

/def -F -mregexp -t'^A wound opens on .*' = \
    /wound_msg opened

/def -F -mregexp -t'^The wound on .* rips open further' = \
    /wound_msg deepened

/def -F -mregexp -t'^A .* worg prowls in\.$' = \
    /rmsg worg arrived %;\
    /send mount worg

/def -F -mregexp -t'^Your .* worg refuses to go there\.$' = \
    /rmsg worg refuses to go there %;\
    /send dismount %;\
    rdismiss

/def -F -mregexp -t'^You mount .* worg\.$' = \
    /set mounted 1 %;\
    /rmsg mounted

/def -F -mregexp -t'^You dismount .* worg\.$' = \
    /set mounted 0 %;\
    /rmsg dismounted

/def -F -mregexp -t'^You dismiss .* worg\.$' = \
    /set mounted 0 %;\
    /rmsg worg dismissed

/def -F -t'*breath return*' = /send wound
/def -F -t'*releases*grip on you*' = /send wound
/def -F -t'*recovered your senses*' = /send wound
/def -F -t'*regain consciousness*' = /send wound
/def -F -t'*recover your balance again*' = /send wound
/def -F -t'*no longer feel paralyzed*' = /send wound
/def -F -t'*unable to attack your target*' = /repeat -2 1 /send wound
/def -F -t'*wrestle back control of your mind*' = /send wound
/def -F -t'*you managed to shake off the terror*' = /send wound

; hilites
