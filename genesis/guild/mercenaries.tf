;
; Mercenaries
;

; variables

/set mattacking 1

; aliases

/alias x /send mattack %{*}

; definitions

/def mattacking = \
    /if (mattacking) \
        /set mattacking 0 %;\
        /e @ automatic mattack OFF %;\
    /else \
        /set mattacking 1 %;\
        /e @ automatic mattack ON %;\
    /endif

/def mattack_check = \
    /if (mattacking) \
        /set mattacking 0 %;\
        /send mattack %;\
        /repeat -2 1 /set mattacking 1 %;\
    /endif

/def mattack_msg = \
        /echo %;\
        /echo -aCcyan < mattack: %{*} > %;\
        /echo %;\
        /send health all

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' mattack_attack = \
    /mattack_check

/def -F -mregexp -t'attacks you\!$' mattack_defend = \
    /repeat -1 1 /mattack_check

/def -F -PBCwhite -mregexp -t'^(You prepare to mattack\.$|Sizing up your foe, you spring into battle\!$)' mattack_prepare = \
    /if (mattacking) \
        /set mattacking 0 %;\
        /repeat -1 1 /set mattacking 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to use Mercenary tactics again\!$' = \
    /mattack_check

/def -F -mregexp -t'(^You|^> You) do not feel ready to mattack again\.$' = \
    /set mattacking 1

/def -F -mregexp -t'^The target of your attack seems to have slipped away\.$' = \
    /set mattacking 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set mattacking 1

/def -F -t'*breath return*' = /send mattack
/def -F -t'*releases*grip on you*' = /send mattack
/def -F -t'*recovered your senses*' = /send mattack
/def -F -t'*You regain consciousness*' = /send mattack
/def -F -t'*You recover your balance again*' = /send mattack
/def -F -t'*You no longer feel paralyzed*' = /send mattack
/def -F -t'*You are unable to attack your target*' = /send mattack
/def -F -t'*wrestle back control of your mind*' = /send mattack
/def -F -t'*you managed to shake off the terror*' = /send mattack

/def mattack_severity = \
    /if ({*} =~ "masterful" | {*} =~ "terrifying" | {*} =~ "uncanny") \
        /mattack_msg severe %;\
    /elseif ({*} =~ "skillful" | {*} =~ "deft" | {*} =~ "expert") \
        /mattack_msg moderate %;\
    /else \
        /mattack_msg weak %;\
    /endif

; messages

/def -F -P1BCwhite -mregexp -t'^You misjudge .* and (your attack goes astray)\.$' = \
    /mattack_msg miss
/def -F -mregexp -t'^Sensing your opportunity, you lunge with (.*) precision' = \
    /mattack_severity %{P1}
/def -F -P1BCwhite -mregexp -t'appears (amused|mostly unphased|mildly annoyed|\
    slightly alarmed|rather alarmed|hurt|badly damaged|quite badly damaged|\
    terribly wounded|mortally wounded|ushered to the brink of death) by your attack\.$'
/def -F -P2BCwhite -mregexp -t'(your blade |)tears a (lethal gash) .*\!$'
/def -F -P1BCwhite -mregexp -t'(pinned to the ground) by your'
/def -F -PBCwhite -mregexp -t'force of your blow crushes'
/def -F -PBCwhite -mregexp -t'devastating chop'
/def -F -PBCwhite -mregexp -t'burying your .* into the soft flesh'
/def -F -PBCwhite -mregexp -t'breaking the body'
/def -F -PBCwhite -mregexp -t'deadly force'
/def -F -PBCwhite -mregexp -t'behedding'
/def -F -PBCwhite -mregexp -t'ramming the blade clean through'
/def -F -PBCwhite -mregexp -t'cleaving the skull'
/def -F -PBCwhite -mregexp -t'blade passess cleanly through flesh and bone'

/def -F -mregexp -t'^Brimming with sudden and terrible wrath, you rear up' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^You crouch low, and then with great exertion spring upwards' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^Filled with sudden wrath, you charge at' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^Mustering all your skill and strength, you burst through' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^Filled with a sudden inspiration of prowess, you twirl' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^Rage fills your throat, and with a sudden burst of skill' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^With uncharacteristic skill and speed, you thrust' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^Feeling the lust of battle rise, you dash' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'^Judging your opponent carefully, you spin swiftly' = \
    /mattack_msg CRITICAL KILL

/def -F -mregexp -t'barrels blindly toward you' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^Spinning in a sudden deft maneuver, you sink your' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^You heave mightily, bringing the blade of your' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^Seeing an opening in .*\'s defences, you (feint|stab)' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^Gripping your .* tightly, you lunge' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^Seizing on the opportunity of the moment, you fling' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^Noticing a mistake in .*\'s stance, you lunge' = \
    /mattack_msg CRITICAL

/def -F -mregexp -t'^Anger takes you suddenly, and you grab' = \
    /mattack_msg CRITICAL
