;
; Cabal of Hiddukel
;

; variables

/set backstabbing 1
/set cheapshotting 1

; aliases

/alias z /send cheapshot %{*}
/alias zz /send backstab %{*}

; definitions

/def backstabbing = \
    /if (backstabbing) \
        /set backstabbing 0 %;\
        /e @ automatic backstab OFF %;\
    /else \
        /set backstabbing 1 %;\
        /e @ automatic backstab ON %;\
    /endif

/def cheapshotting = \
    /if (cheapshotting) \
        /set cheapshotting 0 %;\
        /e @ automatic cheapshot OFF %;\
    /else \
        /set cheapshotting 1 %;\
        /e @ automatic cheapshot ON %;\
    /endif

/def backstab_check = \
    /if (backstabbing & team & leader !~ "none") \
        /set backstabbing 0 %;\
        /send backstab %;\
        /repeat -3 1 /set backstabbing 1 %;\
    /endif

/def cheapshot_check = \
    /if (cheapshotting) \
        /set cheapshotting 0 %;\
        /send cheapshot %;\
        /repeat -3 1 /set cheapshotting 1 %;\
    /endif

/def backstab_msg = \
    /echo %;\
    /echo -aCcyan < backstab: %{*} > %;\
    /echo

/def cheapshot_msg = \
    /echo %;\
    /echo -aCCyan < cheap shot: %{*} > %;\
    /echo

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' cheapshot_attack = \
    /backstab_check %;\
    /cheapshot_check

/def -F -mregexp -t'attacks you\!$' cheapshot_defend = \
    /repeat -1 1 /cheapshot_check

/def -F -PBCwhite -mregexp -t'^You feel ready to perform another backstab\.$' = \
    /backstab_check

/def -F -PBCwhite -mregexp -t'^You feel able to cheap shot again\.$' = \
    /cheapshot_check

/def -F -mregexp -t'(^You|^> You)\'re not ready to backstab again\.$' = \
    /set backstabbing 1

/def -F -mregexp -t'^You lack the proper weapon to perform a backstab\.$' = \
    /set backstabbing 0

/def -F -mregexp -t'^You require a knife suitable to impale with to perform a backstab\.$' = \
    /set backstabbing 0

/def -F -mregexp -t'(^You|^> You)\'re not ready to perform another cheap shot again\.$' = \
    /set cheapshotting 1

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /set cheapshotting 1

/def -F -mregexp -t'^Your target appears to have slipped away\.$' = \
    /set backstabbing 1

/def -F -t'*breath return*' = /send cheapshot%;/backstab_check
/def -F -t'*releases his grip on you*' = /send cheapshot%;/backstab_check
/def -F -t'*recovered your senses*' = /send cheapshot%;/backstab_check
/def -F -t'*You regain consciousness*' = /send cheapshot%;/backstab_check
/def -F -t'*You are unable to attack your target*' = /send cheapshot%;/backstab_check
/def -F -t'*You failed to seize the opportunity to interrupt*' = /repeat -1 1 /send cheapshot

; hilites

/WHITE ^You look for an opportunity to backstab .*
/WHITE ^You prepare yourself to perform a backstab\.$
/WHITE ^You (circle around|sneak up behind) .*\.$
/WHITE ^You (swing your right elbow,|swing your left elbow,|move in close and swiftly knee|sidestep .* and sweep|drop to one knee and deliver a punch) .*\.$

/def -F -P1BCwhite -mregexp -t'^You move in close behind .* and thrust your .* (weakly|forcefully|deeply|fatally) into .* back\.$' bs_success = \
    /backstab_msg %{P1}

/def -F -mregexp -t'^You move in close behind .* and thrust your .*(knife|dagger|blade|tooth) into .* back\.$' bs_success_2 = \
    /backstab_msg successfully

/def -F -mregexp -t'^You lunge at .* with your .*, but miss and reveal your intent\!$' bs_miss = \
    /backstab_msg miss

/def -F -PBCwhite -mregexp -t'^You successfully interrupt .*' cs_success = \
    /cheapshot_msg success

/def -F -PBCWhite -mregexp -t'^(The .*|[^ ]+) .*has recovered from your cheap shot\.$' cs_recovered = \
    /cheapshot_msg recovered
