;
; Blacksmiths
;

; variables

/set weapons_to_sharpen 1
/set auto_forging 0
/set auto_forging_count 0
/set auto_forging_item none

; aliases

/alias ins /send smglance

/alias sh \
    /send sharpen wielded weapon %;\
    /if (weapons_to_sharpen > 1) \
        /send sharpen second wielded weapon %;\
    /endif

/alias weapons \
    /set weapons_to_sharpen %{1-1} %;\
    /echo %;\
    /e < weapons to sharpen: %weapons_to_sharpen > %;\
    /echo

/alias sharpen_forever \
    sh %;\
    /repeat -00:03:00 1 %auto_sharpen_command

/alias autosharpen \
    /if ({*} !~ "on") \
        /echo %;\
        /echo -aCcyan < automatic weapon sharpening off > %;\
        /echo %;\
        /unset auto_sharpen_command %;\
    /else \
        /echo %;\
        /echo -aCcyan < automatic weapon sharpening on > %;\
        /echo %;\
        /set auto_sharpen_command sharpen_forever %;\
        sharpen_forever %;\
    /endif %;\

/alias autoforge \
    /if ({*} =~ "") \
        /set auto_forging 0 %;\
        /echo %;\
        /echo -aCcyan < automatic forging off > %;\
        /echo %;\
    /else \
        /set auto_forging 1 %;\
        /set auto_forging_item %{*} %;\
        /echo %;\
        /echo -aCcyan < auto forging %auto_forging_item > %;\
        /echo %;\
        /send get tools %;\
        /send smpump bellow %;\
        /send forge %auto_forging_item %;\
    /endif

; triggers

/def -F -mregexp -t'^You have finished forging .*\.$' forging_done = \
    /set auto_forging_count $[auto_forging_count + 1] %;\
    /echo %;\
    /echo -aCcyan < %auto_forging_item: %auto_forging_count > %;\
    /echo %;\
    /if (auto_forging) \
        /send smpump bellow %;\
        /send forge %auto_forging_item %;\
    /endif

/def -F -mregexp -t'^You do not have the proper materials to forge' = \
    /echo %;\
    /echo -aCcyan < no more materials > %;\
    /echo %;\
    autoforge

/def -F -mregexp -t'^This .* doesn\'t seem very effective\.$' = \
    /e PEN 0/5
/def -F -mregexp -t'^This .* might do some damage with a lucky hit\.$' = \
    /e PEN 1/5
/def -F -mregexp -t'^This .* seems quite effective\.$' = \
    /e PEN 2/5
/def -F -mregexp -t'^This .* could most likely kill with a single swipe\.$' = \
    /e PEN 3/5
/def -F -mregexp -t'^This .* is extremely powerful and more lethal than many of the famous weapons\.$' = \
    /e PEN 4/5
/def -F -mregexp -t'^This .* is an extremely powerful artifact\.$' = \
    /e PEN 5/5

/def -F -mregexp -t'^It is almost impossible to get a good strike with this .*\.$' = \
    /e HIT 0/5
/def -F -mregexp -t'^It is difficult to use this .* properly\.$' = \
    /e HIT 1/5
/def -F -mregexp -t'^It feels good when you wield this .*' = \
    /e HIT 2/5
/def -F -mregexp -t'^You should have no trouble handling this .*' = \
    /e HIT 3/5
/def -F -mregexp -t'^Even an apprentice warrior could kill easily with this .*\.$' = \
    /e HIT 4/5
/def -F -mregexp -t'^With this .* almost every strike could be a killing blow\.$' = \
    /e HIT 5/5

/def -F -mregexp -t'^This armour wouldn\'t even protect you from a very weak blow\.$' = \
    /e AC 0/5
/def -F -mregexp -t'^This armour could perhaps reduce damage a little\.$' = \
    /e AC 1/5
/def -F -mregexp -t'^This seems to be an armour of average quality\.$' = \
    /e AC 2/5
/def -F -mregexp -t'^This armour would protect you very well\.$' = \
    /e AC 3/5
/def -F -mregexp -t'^This is an extremely well-made armour which would protect you very well\.$' = \
    /e AC 4/5
/def -F -mregexp -t'^Virtually nothing could harm you if you wore this armour\.$' = \
    /e AC 5/5
