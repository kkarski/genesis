;
; Warlocks
;

; variables

/set waupkeep 1
/set waheal 0
/set waheal_target none
/set attempted_spell none

; aliases

/alias cast \
    /set attempted_spell %{*} %;\
    /send cast %attempted_spell

/alias home /send cast urmhol

/alias wastore \
    oh %;\
    ph ingredients %;\
    ph herbs %;\
    ch

/alias waretrieve \
    oh %;\
    gh ingredients %;\
    gh herbs %;\
    ch

/alias wacreate \
    waretrieve %;\
    /send wacreate %{*} %;\
    wastore

/alias wagather \
    /set waingredients 1 %;\
    /send cut heart from corpse %;\
    /send cut skull from corpse %;\
    /send cut kneecap from corpse %;\
    /repeat -1 1 wastore %;\
    /repeat -2 1 /set waingredients 0

; macros

/def recast = \
    /if (waupkeep) \
        cast %{*} %;\
    /endif

/def waupkeep = \
    /if (waupkeep) \
        /set waupkeep 0 %;\
        /e @ warlock spell upkeep OFF %;\
    /else \
        /set waupkeep 1 %;\
        /e @ warlock spell upkeep ON %;\
    /endif

/def waheal = \
    /if (waheal) \
        /set waheal 0 %;\
        /if (waheal_target !~ none) \
            /set waheal_target none %;\
        /endif %;\
        /e @ automatic healing OFF %;\
    /else \
        /set waheal 1 %;\
        /if ({*} !~ "") \
            /set waheal_target %{*} %;\
            /e @ automatic healing of %waheal_target ON %;\
        /else \
            /e @ automatic healing ON %;\
        /endif %;\
    /endif

/def flare_charm = \
    oh %;\
    gh %attempted_spell %;\
    ch %;\
    /send flare %attempted spell

/def wamessage = \
    /echo %;\
    /e < %{*} > %;\
    /echo

; triggers

/def -F -mregexp -t'^You are physically (.*) and mentally' = \
    /if ({P1} !~ "feeling very well" & {P1} !~ "feeling well" & {P1} !~ "sore") \
        /if (waheal) \
            waheal %;\
        /endif %;\
    /endif

/def -F -PCcyan -mregexp -t'^You move quickly through time and space to your destination\!$' = \
    /wamessage WARLOCK GUILDHALL

/def -F -mregexp -t'^>? ?You cut a (heart|skull|kneecap) from the corpse' = \
    /if (waingredients) \
        /send wascatter %{P1} %;\
    /endif

/def -F -mregexp -t'^You can now cast (.*)\$.' = \
    /wamessage %{P1} charges restored %;\
    cast %{P1}

/def -F -PCred -mregexp -t'^Sorry, but you are not properly prepared to cast the spell\!$' = \
    /flare_charm

/def -F -PCred -mregexp -t'^You need the proper preparations for .*\!$' = \
    /flare_charm

/def -F -PCcyan -mregexp -t'^You renew the duration of (.*)\!$' = \
    /wamessage %{P1} up

/def -F -PCcyan -mregexp -t'^You feel less burdened as the weight of your belongings eases dramatically\.$' = \
    /wamessage Urhuew up

/def -F -PCcyan -mregexp -t'^You feel Urhuew is starting to expire\.$' = \
    /wamessage Urhuew expiring %;\
    /recast urhuew

/def -F -PCcyan -mregexp -t'^You feel more taxed as the effect of the spell Urhuew ends\.$' = \
    /wamessage Urhuew down %;\
    /recast urhuew

/def -F -PCcyan -mregexp -t'^You grimace slightly as your body cracks and snaps into a physically enhanced being\.$' = \
    /wamessage Urhagash up

/def -F -PCcyan -mregexp -t'^You sense your Urhagash spell is beginning to fade\.$' = \
    /wamessage Urhagash expiring %;\
    /recast urhagash

/def -F -PCcyan -mregexp -t'^You feel yourself shrinking as Urhagash wears off\. Painful\.$' = \
    /wamessage Urhagash down %;\
    /recast urhagash

; hilites

/def -F -PCcyan -mregexp -t'^As you surreptitiously whisper the long forgotten words, .*\!$'
/def -F -PCcyan -mregexp -t'^You rip open a tear in the fabric of space, and enter\.$'
