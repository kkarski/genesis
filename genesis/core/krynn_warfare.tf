;
; Krynn Warfare System
;

; variables

/set auto_conquer 0
/set conquer_attempted 0

; aliases

/alias kw k _warfare_npc_
/alias ca /send conquer area
/alias autoconquer /auto_conquer

; macros

/def warfare_msg = \
    /e %;\
    /e < $[strip_attr({*})] > %;\
    /e

/def auto_conquer = \
    /if (auto_conquer) \
        /set auto_conquer 0 %;\
        /warfare_msg automatic conquering OFF %;\
    /else \
        /set auto_conquer 1 %;\
        /warfare_msg automatic conquering ON %;\
    /endif

/def conquer_check = \
    /if (!conquer_attempted) \
        /send conquer area %;\
        /set conquer_attempted 1 %;\
        /repeat -5 1 /set conquer_attempted 0 %;\
    /endif

; triggers

/def -F -mregexp -t'^You aren\'t fighting anyone\!$' = \
    /if (auto_conquer) \
        /conquer_check %;\
    /endif

/def -F -mregexp -t'There are still signs of a (.*) of enemies remaining\.$' = \
    /warfare_msg remaining: %{P1}

/def -F -mregexp -t'^You conquer the (.*) in the name of' = \
    /warfare_msg %{P1} conquered!

/def -F -mregexp -t'^You just recently tried to conquer this area\!' = \
    /set conquer_attempted 1 %;\
    /repeat -2 1 /set conquer_attempted 0

; hilites

/def -F -P1BCwhite -mregexp -t'^You cannot conquer the (.*) as it'
