;
; Colouring and Notifications
;

; settings

/set messaging 0

; definitions

/def GREEN = /def -F -PCgreen -mregexp -t'%{*}'
/def WHITE = /def -F -PBCwhite -mregexp -t'%{*}'
/def RED = /def -F -PCred -mregexp -t'%{*}'
/def YELLOW = /def -F -PCyellow -mregexp -t'%{*}'
/def BLUE = /def -F -PCblue -mregexp -t'%{*}'
/def CYAN = /def -F -PCcyan -mregexp -t'%{*}'
/def MAGENTA = /def -F -PCmagenta -mregexp -t'%{*}'

/def e = /echo -aCcyan %{*}

/def messaging = \
    /if (messaging) \
        /set messaging 0 %;\
        /e @ special messages OFF %;\
    /else \
        /set messaging 1 %;\
        /e @ special messages ON %;\
    /endif

; general

/def -F -P2BCwhite -mregexp -t'is wielding (a|an) (.*) in .*\.$'
/def -F -P3BCwhite -mregexp -t'is wielding .* in (his|her|its) .*  and (a|an) (.*) in (his|her|its).*\.$'

/WHITE ^([^ ]+|.*) gives you .*\.$

/MAGENTA ^Your .* increases .*\.$

/def -F -PBCwhite -mregexp -t'^You feel ready to teleport again\.$' = \
    /echo %;\
    /e < teleport ready > %;\
    /echo

; condition

/hilite *physically*and mentally*

/GREEN ^You feel much better\.$
/GREEN ^You feel healthier\.$
/GREEN ^You feel healthier and less tired\.$
/GREEN ^You feel mentally healthier\.$
/GREEN ^You feel mentally stronger\.$
/GREEN ^You feel less tired\.$
/GREEN ^You feel mentally stronger, healthier and less tired\.$
/RED ^You have been poisoned\!$
/RED ^You feel less healthy\.$
/RED ^You feel much less healthy\.$
/RED ^You feel mentally less healthy\.$
/RED ^You feel mentally much less healthy\.$
/RED ^You feel more tired.\$
/RED ^You feel much more tired.\$
/RED ^The .* took the full brunt of that last hit .*
/RED ^You feel less resistant\.$

/WHITE ^(He|She|It) seems to be .*\.$
/WHITE ^You .*channel your energies through your .*\.$

/RED ^You feel a trickle of blood down your .*\.$
/GREEN ^The blood trickling down your .* dries up\.$

; hits

/def -F -P1BCwhite -mregexp -t'^You (barely scrape|scratch|rake|seriously cut|cut deeply|cut|rip into|fiercely rive)'
/def -F -P1Cred -mregexp -t'(barely scrapes|scratches|rakes|seriously cuts|cuts deeply|cuts|rips into|fiercely rives) your'

/def -F -P1BCwhite -mregexp -t'^You (lightly bruise|batter|pound|beat|seriously beat|soundly beat|hammer|brutally pummel)'
/def -F -P1Cred -mregexp -t'(lightly bruises|batters|pounds|beats|seriously beats|soundly beats|hammers|brutally pummels) your'

/def -F -P1BCwhite -mregexp -t'^You (just nick|graze|jab|seriously stab|stab deeply|stab|lance through|viciously impale)'
/def -F -P1Cred -mregexp -t'(just nicks|grazes|jabs|seriously stabs|stabs deeply|stabs|lances through|viciously impales) your'

/def -F -P1BCwhite -mregexp -t'^You (scrape|hurt|barely wound|wound|injure|smash|massacre)'
/def -F -P1Cred -mregexp -t'(scrapes|hurts|barely wounds|wounds|injures|smashes|massacres) your'

/def -F -P1BCwhite -mregexp -t'^You (barely touch|gouge|nick|slice|pierce|impale|drive|lacerate|almost sever|almost sunder|nearly sever|nearly sunder|bury|slash|tickle|tear) (into|open|your|the)'
/def -F -P1Cred -mregexp -t'(barely touches|almost sunders|gouge|tickles|tears|slices|almost severs|nearly severs|nearly sunders|lacerates|drives|impales|nicks|slashes|buries) (its .*|)(into |open |)your'

/def -F -P1BCwhite -mregexp -t'and do (.* damage)\.$'
/def -F -P1Cred -mregexp -t'and does (.* damage)\.$'

; exits

/def -F -P1BCwhite -mregexp -t'(east|north|south|west|southeast|southwest|northeast|northwest)( and|\,|\.)'
/def -F -P1BCwhite -mregexp -t'\, (east|north|south|west|southeast|southwest|northeast|northwest)$'

/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)north\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)east\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)south\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)west\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)northeast\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)southeast\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)northwest\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)southwest\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)up\.$
/WHITE ^(The .*|[^ ]+) (leaves|panics and flees|stomps off,|skips away,) (|.*)down\.$

/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)north\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)east\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)south\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)west\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)northeast\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)southeast\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)northwest\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)southwest\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)up\.$
/WHITE ^(The .*|[^ ]+) (arrives|stomps in|skips in,) (|.*)down\.$

; weapon and armour condition

/def -F -P1BCwhite -mregexp -t'It looks like it is (in prime condition)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (a little worn down)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (in a fine condition)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (a little touched by battle)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (a little scarred by battle)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (touched by battle)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (scarred by battle)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (in a very bad shape)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (very scarred by battle)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (in urgent need of repair)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (in big need of a smith)\.$'
/def -F -P1BCwhite -mregexp -t'It looks like it is (going to break any second)\.$'

/WHITE This object seems to be able to last a while

/WHITE intact and without any rips

/RED ^The .* breaks\!\!\!$

/WHITE Some runes have been etched into the blade, near the hilt\.

/MAGENTA (unusual|peculiar|exotic)

; communication

/def -PBCyellow -F -mregexp -t'^(The .*|[^ ]+) .*(admits|agrees|ahoys|announces|\
	answers|asks|aquiesces|babbles|barks|bellows|blurts|booms|breathes|bubbles|\
	bursts into song|carols|caroons|chants|chatters|chirps|chuckles|claims|\
	communicates|conceeds|confesses|conveys|complains|coos|cries|cries out|cries out with fervor|croons|declares|\
	decrees|delineates|eerie voice|enunciates|explains|garbles|giggles|grates|grins|groans|growls|gruffs|grumbles|grunts|hints|\
	hisses|howls|hrums|implies|indicates|in Draco|inquires|insinuates|intones|\
	jabbers|laments|laughs|lilts|mentions|mimes|moans|mumbles|murmurs|trance-like tone|\
	notes|observes|old Solamnian|pleads|ponders|pouts|puffs|pukes|queries|rasps|\
	remarks|replies|resonates|retorts|reverberates|roars|rumbles|says|screams|snarls|with a lilting accent|\
	shouts|shrieks|sighs|signals|sings|sings with a musical breath|scoffs|smiles|\
	smirks|snaps|snarls|sneers|snickers|sniffs|snivels|snorts|sobs|\
    speaks|speaks in a hushed voice|speaks in a hollow, distant voice|lilting language|\
	secret kender tongue|squeaks|suggests|thinks|thunders|trills|twitters|utters|\
	utters in the black tongue|ululates|wails|warbles|whines|whispers|wonders|yells).*:'

/def -PBCyellow -F -mregexp -t'^(The .*|[^ ]+) .*(admits|agrees|ahoys|announces|\
	answers|asks|aquiesces|babbles|barks|bellows|blurts|booms|breathes|bubbles|\
	bursts into song|carols|caroons|chants|chatters|chirps|chuckles|claims|\
	communicates|conceeds|confesses|conveys|complains|coos|cries|cries out|cries out with fervor|croons|declares|\
	decrees|delineates|eerie voice|enunciates|explains|garbles|giggles|grates|grins|groans|growls|gruffs|grumbles|grunts|hints|\
	hisses|howls|hrums|implies|indicates|in Draco|inquires|insinuates|intones|\
	jabbers|laments|laughs|lilts|mentions|mimes|moans|mumbles|murmurs|trance-like tone|\
	notes|observes|old Solamnian|pleads|ponders|pouts|puffs|pukes|queries|rasps|\
	remarks|replies|resonates|retorts|reverberates|roars|rumbles|says|snarls|spits|with a lilting accent|\
	shouts|shrieks|sighs|signals|sings|sings with a musical breath|scoffs|smiles|\
	smirks|snaps|snarls|sneers|snickers|sniffs|snivels|snorts|sobs|\
    speaks|speaks in a hushed voice|speaks in a hollow, distant voice|lilting language|\
	secret kender tongue|squeaks|suggests|thinks|thunders|trills|twitters|utters|\
	utters in the black tongue|ululates|wails|warbles|whines|whispers|wonders|yells).* to .*:'

/def -PBCyellow -F -mregexp -t'^(The .*|[^ ]+) .*(asks you|in your ear):'

/def -PBCyellow -F -mregexp -t'message from %{P2}'

/def -PBCyellow -F -mregexp -t'^The voice .* invades your mind saying:'

/def -PBCyellow -F -mregexp -t'^As if from behind .* whispering:'

/def -PBCyellow -F -mregexp -t'^A soft wind blows around your ears, carrying the voice of .* whispering:'

/def -PBCyellow -F -mregexp -t'^You (say|ask|whisper|.*)(| .*):'

/def -PBCyellow -F -mregexp -t'^You (tell|replied to).*:'

/def -PBCyellow -F -mregexp -t'^(The .*|[^ ]+) .*tells you:'

/def -PBCyellow -F -mregexp -t'^\<.*\> .*:'

/def -PBCyellow -F -mregexp -t'^Your .* paddle (vibrates|dances) .*\.'

/def -F -PCred -mregexp -t'^([A-Za-z]+) (loses touch with reality|passes into limbo)\.$' = \
    /echo %;\
    /echo -aCred < %{P1} disconnected! > %;\
    /echo

/def -F -PBCwhite -mregexp -t'^([A-Za-z]+) gets in touch with reality again\.$' = \
    /echo %;\
    /echo -aCcyan < %{P1} reconnected > %;\
    /echo

/def -F -PCgreen -mregexp -t'^The dark silver longsword thrums with power and the diamonds flare as new life courses through your body\.$'

; health

/def -F -p1 -PCgreen -mregexp -t'^(The .*|[^ ]+) .*is feeling very well\.$'
/def -F -p1 -PCgreen -mregexp -t'^(The .*|[^ ]+) .*is feeling well\.$'
/def -F -p1 -PCgreen -mregexp -t'^(The .*|[^ ]+) .*is sore\.$'
/def -F -p1 -PCgreen -mregexp -t'^(The .*|[^ ]+) .*is slightly hurt\.$'
/def -F -p1 -PCgreen -mregexp -t'^(The .*|[^ ]+) .*is somewhat hurt\.$'
/def -F -p1 -PCyellow -mregexp -t'^(The .*|[^ ]+) .*is aching\.$'
/def -F -p1 -PCyellow -mregexp -t'^(The .*|[^ ]+) .*is hurt\.$'
/def -F -p1 -PCyellow -mregexp -t'^(The .*|[^ ]+) .*is suffering\.$'
/def -F -p1 -PCyellow -mregexp -t'^(The .*|[^ ]+) .*is very hurt\.$'
/def -F -p1 -PCyellow -mregexp -t'^(The .*|[^ ]+) .*is in a bad shape\.$'
/def -F -p1 -PCred -mregexp -t'^(The .*|[^ ]+) .*is in agony\.$'
/def -F -p1 -PCred -mregexp -t'^(The .*|[^ ]+) .*is in a very bad shape\.$'
/def -F -p1 -PCred -mregexp -t'^(The .*|[^ ]+) .*is terribly hurt\.$'
/def -F -p1 -PCred -mregexp -t'^(The .*|[^ ]+) .*is barely alive\.$'
/def -F -p1 -PCred -mregexp -t'^(The .*|[^ ]+) .*is at death\'s door\.$'
