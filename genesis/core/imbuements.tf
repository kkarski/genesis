;
; Imbuements
;

; settings

/set imbstr none
/set statimbue none
/set slayimbue none
/set elemimbue none
/set spellimbue none
/set weaponimbue none
/set auraimbue none
/set modimbue none
/set resistimbue none
/set skillimbue none
/set miscimbue none

; imbuements

/def -F -mregexp -t'(A|An) (faint|pronounced|intense) smell of (musk|vanilla|alcohol|lavender|sage|cinnamon) emanates' sub_statimbue = \
    /if ({P3} =~ "musk") \
        /set statimbue STR %;\
    /elseif ({P3} =~ "cinnamon") \
        /set statimbue DEX %;\
    /elseif ({P3} =~ "alcohol") \
        /set statimbue CON %;\
    /elseif ({P3} =~ "lavender") \
        /set statimbue INT %;\
    /elseif ({P3} =~ "sage") \
        /set statimbue WIS %;\
    /elseif ({P3} =~ "vanilla") \
        /set statimbue DIS %;\
    /else \
        /set statimbue UNKNOWN %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P2} %{statimbue}

/def -F -mregexp -t'(Beryl|Bone|Diamond|Granite|Ivory|Mithril|Onyx|Peridot|Quartz|Sapphire|Topaz) formations along .* surface emit a (.*) glow' sub_slayimbue = \
    /if ({P1} =~ "Beryl") \
        /set slayimbue ELF %;\
    /elseif ({P1} =~ "Bone") \
        /set slayimbue UNDEAD %;\
    /elseif ({P1} =~ "Diamond") \
        /set slayimbue DRAGON %;\
    /elseif ({P1} =~ "Granite") \
        /set slayimbue OGRE %;\
    /elseif ({P1} =~ "Ivory") \
        /set slayimbue MINOTAUR %;\
    /elseif ({P1} =~ "Mithril") \
        /set slayimbue DWARF %;\
    /elseif ({P1} =~ "Onyx") \
        /set slayimbue GOBLIN %;\
    /elseif ({P1} =~ "Peridot") \
        /set slayimbue GNOME %;\
    /elseif ({P1} =~ "Quartz") \
        /set slayimbue HALFLING %;\
    /elseif ({P1} =~ "Sapphire") \
        /set slayimbue HUMAN %;\
    /elseif ({P1} =~ "Topaz") \
        /set slayimbue TROLL %;\
    /else \
        /set slayimbue UNKNOWN %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P2} SLAY-%{slayimbue}

/def -F -mregexp -t'(It glistens with |Drops of |It is enveloped by |)(a |an |A|An|)(faint|Faint|faintly|pronounced|Pronounced|pronouncedly|intense|Intense|intensely) (clusters of frost cling|disturbance in the air|hissing sound issues|fluctuation in|acid sheen|odor of decay issues|ripples of blue electricty play along|green moisture cling|white flames)' sub_elemimbue = \
    /if ({P4} =~ "clusters of frost cling") \
        /set elemimbue COLD %;\
    /elseif ({P4} =~ "disturbance in the air") \
        /set elemimbue AIR %;\
    /elseif ({P4} =~ "hissing sound issues") \
        /set elemimbue HEAT %;\
    /elseif ({P4} =~ "fluctuation in") \
        /set elemimbue EARTH %;\
    /elseif ({P4} =~ "acid sheen") \
        /set elemimbue ACID %;\
    /elseif ({P4} =~ "odor of decay issues") \
        /set elemimbue DEATH %;\
    /elseif ({P4} =~ "white flames") \
        /set elemimbue ELEMENTAL %;\
    /elseif ({P4} =~ "ripples of blue electricty play along") \
        /set elemimbue ELECTRICITY %;\
    /elseif ({P4} =~ "green moisture cling") \
        /set elemimbue POISON %;\
    /else \
        /set elemimbue UNKNOWN %;\
    /endif %;\
    /if ({P3} =~ "Faint" | {P3} =~ "faintly") \
        /set imbstr faint %;\
    /elseif ({P3} =~ "Pronounced" | {P3} =~ "pronouncedly") \
        /set imbstr pronounced %;\
    /elseif ({P3} =~ "Intense" | {P3} =~ "intensely") \
        /set imbstr intense %;\
    /else \
        /set imbstr %{P3} %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{imbstr} DAMAGE-%{elemimbue}

/def -F -mregexp -t'Its surface has (a|an) (.*) (opalesque|grey|brown|blue|azure|rose) tint' sub_spellimbue = \
    /if ({P3} =~ "opalesque") \
        /set spellimbue WATER %;\
    /elseif ({P3} =~ "grey") \
        /set spellimbue DEATH %;\
    /elseif ({P3} =~ "brown") \
        /set spellimbue EARTH %;\
    /elseif ({P3} =~ "blue") \
        /set spellimbue LIFE %;\
    /elseif ({P3} =~ "azure") \
        /set spellimbue AIR %;\
    /elseif ({P3} =~ "rose") \
        /set spellimbue FIRE %;\
    /else \
        /set spellimbue UNKNOWN_SPELLS %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P2} %{spellimbue} SPELLS

/def -F -mregexp -t'It is pulsing with the (.*) aura of a (stallion|lion|spider|boar|badger|monkey|falcon)' sub_weaponimbue = \
    /if ({P2} =~ "stallion") \
        /set weaponimbue POLEARM %;\
    /elseif ({P2} =~ "lion") \
        /set weaponimbue SWORD %;\
    /elseif ({P2} =~ "spider") \
        /set weaponimbue KNIFE %;\
    /elseif ({P2} =~ "boar") \
        /set weaponimbue CLUB %;\
    /elseif ({P2} =~ "badger") \
        /set weaponimbue AXE %;\
    /elseif ({P2} =~ "monkey") \
        /set weaponimbue UNARMED COMBAT %;\
    /elseif ({P2} =~ "falcon") \
        /set weaponimbue MISSILES %;\
    /else \
        /set weaponimbue UNKNOWN WEAPON %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P1} %{weaponimbue}

/def -F -mregexp -t'(A|An) (.*) aura of (light|darkness|crimson energy) surrounds this object' sub_auraimbue = \
    /if ({P3} =~ "light") \
        /set auraimbue LIGHT %;\
        /substitute -aCcyan -p Imbuement: %{P2} %{auraimbue} %;\
    /elseif ({P3} =~ "darkness") \
        /set auraimbue DARKNESS %;\
        /substitute -aCcyan -p Imbuement: %{P2} %{auraimbue} %;\
    /elseif ({P3} =~ "crimson energy") \
        /set auraimbue SPELLCRAFT %;\
        /substitute -aCcyan -p Imbuement: %{P2} %{auraimbue} %;\
    /else \
        /set auraimbue UNKNOWN %;\
        /substitute -aCcyan -p Imbuement: %{P2} %{auraimbue} AURA %;\
    /endif

/def -F -mregexp -t'It exudes (a|an) (faint|pronounced|intense) aura of (well-being|viciousness|malevolence)' sub_modimbue = \
    /if ({P3} =~ "well-being") \
        /set modimbue ARMOUR %;\
    /elseif ({P3} =~ "viciousness") \
        /set modimbue ACCURACY %;\
    /elseif ({P3} =~ "malevolence") \
        /set modimbue DAMAGE %;\
    /else \
        /set modimbue UNKNOWN %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P2} %{modimbue}

/def -F -mregexp -t'(It gives you|You get|Being near this object gives you|A|An|For some odd reason, this object gives you|The presence of this object causes|Being around this object gives you) (a |an |A |An |)(faint|pronounced|intense) (feeling of unease|goosebumps being around this object|urge to sneeze|dull feeling comes over you, looking at this object|tickle in your throat|ringing in your ears|itching sensation|feeling of nausea as you stare at it|feeling of drowsiness comes over you as peer at it|hair-raising sensation grips you as you examine this object|feeling of calm comes over you in the presence of this object)' sub_resistimbue = \
    /if ({P4} =~ "feeling of unease") \
        /set resistimbue LIFE %;\
    /elseif ({P4} =~ "goosebumps being around this object") \
        /set resistimbue COLD %;\
    /elseif ({P4} =~ "urge to sneeze") \
        /set resistimbue POISON %;\
    /elseif ({P4} =~ "dull feeling comes over you, looking at this object") \
        /set resistimbue MAGIC %;\
    /elseif ({P4} =~ "tickle in your throat") \
        /set resistimbue WATER %;\
    /elseif ({P4} =~ "ringing in your ears") \
        /set resistimbue AIR %;\
    /elseif ({P4} =~ "itching sensation") \
        /set resistimbue ACID %;\
    /elseif ({P4} =~ "feeling of calm comes over you in the presence of this object") \
        /set resistimbue FIRE %;\
    /elseif ({P4} =~ "feeling of nausea as you stare at it") \
        /set resistimbue DEATH %;\
    /elseif ({P4} =~ "feeling of drowsiness comes over you as peer at it") \
        /set resistimbue EARTH %;\
    /elseif ({P4} =~ "hair-raising sensation grips you as you examine this object") \
        /set resistimbue ELECTRICITY %;\
    /else \
        /set resistimbue UNKNOWN %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P3} RESIST %{resistimbue}

/def -F -mregexp -t'(It is emitting |It pulses with |You notice |It is surrounded by |)(a |an |A |An |)(faint|faintly|pronounced|pronouncedly|intense|intensely) (orange glow surrounds its surface|clicking sound is coming from this object|purring sound|silvery aura|purple gleam playing off its surface|swirling bands of yellow energy|twisting pattern dancing off its surface)' sub_skillimbue = \
    /if ({P4} =~ "orange glow surrounds its surface") \
        /set skillimbue AWARENESS %;\
    /elseif ({P4} =~ "clicking sound is coming from this object") \
        /set skillimbue BLINDFIGHTING %;\
    /elseif ({P4} =~ "purring sound") \
        /set skillimbue HIDE AND SNEAK %;\
    /elseif ({P4} =~ "silvery aura") \
        /set skillimbue TWO HANDED COMBAT %;\
    /elseif ({P4} =~ "purple gleam playing off its surface") \
        /set skillimbue PARRY %;\
    /elseif ({P4} =~ "swirling bands of yellow energy") \
        /set skillimbue DEFENCE %;\
    /elseif ({P4} =~ "twisting pattern dancing off its surface") \
        /set skillimbue ACROBAT %;\
    /else \
        /set skillimbue UNKNOWN %;\
    /endif %;\
    /if ({P3} =~ "faintly") \
        /set imbstr faint %;\
    /elseif ({P3} =~ "pronouncedly") \
        /set imbstr pronounced %;\
    /elseif ({P3} =~ "intensely") \
        /set imbstr intense %;\
    /else \
        /set imbstr %{P3} %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{imbstr} %{skillimbue}

/def -F -mregexp -t'(A|An) (faint|pronounced|intense) (feeling of security emanates from it|sparkle bewitches your senses as you peer at it|gleam of mithril seems to coat this object|whine is coming from this object|drone issues from this weapon)' sub_miscimbue = \
    /if ({P3} =~ "feeling of security emanates from it") \
        /set miscimbue HEALING %;\
    /elseif ({P3} =~ "sparkle bewitches your senses as you peer at it") \
        /set miscimbue DARKVISION %;\
    /elseif ({P3} =~ "gleam of mithril seems to coat this object") \
        /set miscimbue DURABILITY %;\
    /elseif ({P3} =~ "whine is coming from this object") \
        /set miscimbue QUICKNESS %;\
    /elseif ({P3} =~ "drone issues from this weapon") \
        /set miscimbue SLOW %;\
    /else \
        /set miscimbue UNKNOWN %;\
    /endif %;\
    /substitute -aCcyan -p Imbuement: %{P2} %{miscimbue}
