;
; Aliases
;

/set sailing 0

/alias auc \
    /send ask rommik %{1} %;\
    /send ask rommik %{2} %;\
    /send ask rommik anonymous %;\
    /send ask rommik %{3} %;\
    /send ask rommik yes
/alias int /send introduce %{*}
/alias h /send health all
/alias ht /send health team
/alias dr /send drink from bottle
/alias dw /send drink from waterskin

/alias sswap \
    /send unwield all %;\
    /send draw %;\
    /send sheathe unwielded sword

/alias fixme \
    /send sharpen wielded weapon %;\
    /send sharpen second wielded weapon %;\
    /send fix helm %;\
    /send fix shield %;\
    /send fix bracers %;\
    /send fix greaves %;\
    /send fix robe %;\
    /send fix pelt %;\
    /send fix cloak %;\
    /send fix medallion %;\
    /send fix chainmail %;\
    /send fix platemail %;\
    /send fix scale %;\
    /send fix gloves %;\
    /send fix boots

/alias tankup \
    op %;\
    gp shield %;\
    cp %;\
    /send unwield all %;\
    /send draw %;\
    /send wear shield %;\
    /send sheathe unwielded weapon

/alias dmgup \
    op %;\
    pp shield %;\
    cp %;\
    /send unwield all %;\
    /send draw %;\
    /send sheathe unwielded weapon

/alias sailing \
    /if (sailing) \
        /set sailing 0 %;\
        /echo -aCcyan << Sailing OFF >> %;\
    /else \
        /set sailing 1 %;\
        /echo -aCcyan << Sailing ON >> %;\
    /endif
/alias n \
    /if (sailing) \
        /send sail n %;\
    /else \
        /send n %;\
    /endif
/alias e \
    /if (sailing) \
        /send sail e %;\
    /else \
        /send e %;\
    /endif
/alias s \
    /if (sailing) \
        /send sail s %;\
    /else \
        /send s %;\
    /endif
/alias w \
    /if (sailing) \
        /send sail w %;\
    /else \
        /send w %;\
    /endif
/alias se \
    /if (sailing) \
        /send sail se %;\
    /else \
        /send se %;\
    /endif
/alias sw \
    /if (sailing) \
        /send sail sw %;\
    /else \
        /send sw %;\
    /endif
/alias ne \
    /if (sailing) \
        /send sail ne %;\
    /else \
        /send ne %;\
    /endif
/alias nw \
    /if (sailing) \
        /send sail nw %;\
    /else \
        /send nw %;\
    /endif

/alias glow /CYAN %{*}
