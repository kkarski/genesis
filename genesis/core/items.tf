;
; Items
;

; variables

/set imbuements 1
/set coins 1
/set gems 0
/set light 0
/set pack 0
/set lootnum 1
/set lootvar all
/set dewar 1
/set primary_pack pack
/set secondary_pack sash

; aliases

/alias g /send get all
/alias ga /send get armours
/alias gc /send get coins
/alias gg /send get gems
/alias gw /send get weapons
/alias gac /send get armours from corpse
/alias gcc /send get coins from corpse
/alias ggc /send get gems from corpse
/alias gwc /send get weapons from corpse
/alias gic /send get imbued items from corpse
/alias gdew \
    /send get purse from corpse %;\
    /send open purse %;\
    /send get all from purse %;\
    /send put purse in corpse

/alias la /send light all
/alias lt /send light torch
/alias ll /send light lamp
/alias ex /send extinguish all
/alias et /send extinguish torch
/alias el /send extinguish lamp
/alias dt /send drop burnt out torch

/alias op /send open %primary_pack
/alias cp /send close %primary_pack
/alias fp /send fill %primary_pack
/alias ep /send empty %primary_pack
/alias gp /send get %{*} from %primary_pack
/alias pp /send put %{*} in %primary_pack
/alias pack op %; fp %; cp
/alias unpack op %; ep %; cp
/alias osp /send open %secondary_pack
/alias csp /send close %secondary_pack
/alias fsp /send fill %secondary_pack
/alias esp /send empty %secondary_pack
/alias gsp /send get %{*} from %secondary_pack
/alias psp /send put %{*} in %secondary_pack

/alias sa \
    /if (pack) \
        unpack %;\
    /endif %;\
    /send sell all

/alias dep /send deposit %{*}
/alias wd /send withdraw %{*}
/alias min /send minimize coins
/alias dc dep gold%;min%;dep platinum

/alias bro /send drop broken items

/alias loot /loot %{*}

/alias shintor \
    /send say angani %;\
    /send say avenis %;\
    /send say hamitar %;\
    /send say layisin %;\
    /send say nathisia %;\
    /send say oeresti %;\
    /send say penditan %;\
    /send say perfal %;\
    /send say telebin %;\
    /send say terelin %;\
    /send say tungal

/alias bsa /send say alrandgulgur
/alias stygion /send say dilvardyn
/alias pure /send aralos

; definitions

/def loot = \
    /set lootnum %{1-1} %;\
    /set lootvar %{-1-all} %;\
    /e @ looting %lootvar from %lootnum corpse(s) %;\
    /if (lootnum == 1) \
        /send get %{lootvar} from corpse %;\
    /elseif (lootnum == 2) \
		/send get %{lootvar} from corpse %;\
        /send get %{lootvar} from second corpse %;\
    /elseif (lootnum == 3) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
    /elseif (lootnum == 4) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
    /elseif (lootnum == 5) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
    /elseif (lootnum == 6) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
    /elseif (lootnum == 7) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
		/send get %{lootvar} from seventh corpse %;\
    /elseif (lootnum == 8) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
		/send get %{lootvar} from seventh corpse %;\
		/send get %{lootvar} from eighth corpse %;\
    /elseif (lootnum == 9) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
		/send get %{lootvar} from seventh corpse %;\
		/send get %{lootvar} from eighth corpse %;\
		/send get %{lootvar} from ninth corpse %;\
    /else \
        /send get %{lootvar} from corpse %;\
    /endif %;\
	/if (pack) \
        pack %;\
    /endif

/def coins = \
    /if (coins) \
        /set coins 0 %;\
     	/e @ automatic coins looting OFF %;\
    /else \
        /set coins 1 %;\
		/e @ automatic coins looting ON %;\
    /endif

/def imbuements = \
    /if (imbuements) \
        /set imbuements 0 %;\
     	/e @ automatic imbuements looting OFF %;\
    /else \
        /set imbuements 1 %;\
		/e @ automatic imbuements looting ON %;\
    /endif

/def gems = \
    /if (gems) \
        /set gems 0 %;\
        /e @ automatic gems looting OFF %;\
    /else \
        /set gems 1 %;\
		/e @ automatic gems looting ON %;\
    /endif

/def pack = \
    /if (pack) \
        /set pack 0 %;\
        /e @ automatic packing OFF %;\
    /else \
        /set pack 1 %;\
        /e @ automatic packing ON %;\
    /endif

/def light = \
    /if (light) \
        /set light 0 %;\
        /e @ automatic light handling OFF %;\
    /else \
        /set light 1 %;\
        /e @ automatic light handling ON %;\
    /endif

/def dewar = \
    /if (dewar) \
        /set dewar 0 %;\
        /e @ special dewar looting OFF %;\
    /else \
        /set dewar 1 %;\
        /e @ special dewar looting ON %;\
    /endif

/def ireset = \
    /e @ resetting item variables %;\
    /set coins 1 %;\
    /set gems 0 %;\
    /set light 0 %;\
    /set pack 0 %;\
    /set lootnum 1 %;\
    /set lootvar all %;\
    /set dewar 1

; triggers

/def -F -mregexp -t'^You killed .* dewar\.$' dew_loot = \
    /if (dewar) \
        gdew %;\
    /endif

/def -F -mregexp -t'^You light .*\.$' l_light = \
    /if (!light) \
        /light %;\
    /endif

/def -F -mregexp -t'^You extinguish .*\.$' ex_light = \
    /if (light) \
        /light %;\
    /endif

/def -F -mregexp -t'^Your .* flickers briefly\. It seems to be running out of oil\.$' fill_lamp = \
    /if (light) \
        /send refill lamp with oil %;\
    /endif

/def -F -mregexp -t'^You fill .* with all the lamp-oil in the flask\.$' empty_flask = \
    /if (light) \
        /send drop empty flask %;\
    /endif

/def -F -mregexp -t'^You wear the .* *(ack|ag) \(open\) on your (back|right shoulder)\.$' wear_open_pack = \
    /if (!pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You wear the .* *(ack|ag) on your (back|right shoulder)\.$' wear_pack = \
    /if (!pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You already wear the .* *(ack|ag)\.$' pack_there = \
    /if (!pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You remove the .* *(ack|ag)\.$' remove_pack = \
    /if (pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You remove the .* *(ack|ag) \(open\)\.$' remove_open_pack = \
    /if (pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You get .* (unusual|peculiar|exotic)' keep_imbued = \
    /send exa imbued item %;\
    /send keep imbued items %;\
    /e @ keeping imbued items

/def -F -PCred -mregexp -t'^The .* breaks due to wear and tear\.$'
