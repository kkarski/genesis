;
; Herbing
;

; variables

/set herbing 1
/set hfind herbs
/set hcontainer pouch

; aliases

/alias hfind \
    /if ({*} =~ "") \
        /e @ currently searching for: %{hfind} %;\
    /else \
        /set hfind %{*} %;\
        /e @ currently searching for: %{hfind} %;\
    /endif

/alias hcontainer \
    /if ({*} =~ "") \
        /e @ current herb container: %{hcontainer} %;\
    /else \
        /set hcontainer %{*} %;\
        /e @ current herb container: %{hcontainer} %;\
    /endif

/alias hs /send search here for %{hfind}
/alias oh /send open %{hcontainer}
/alias ch /send close %{hcontainer}
/alias ph /send put %{*} in %{hcontainer}
/alias gh /send get %{*} from %{hcontainer}
/alias hc \
    oh %;\
    gh %{1} %;\
    ch %;\
    /if ({2} =~ "") \
        /send eat %{1} %;\
    /else \
        /send %{2} %{1} %;\
    /endif

/alias attanar hc attanar
/alias suranie hc suranie
/alias chicory hc chicory
/alias whitehorn hc whitehorn
/alias argil hc argil chew
/alias athly hc athly
/alias soapweed hc soapweed smear
/alias skunk \
    oh %;\
    gh skunk berry %;\
    ch %;\
    /send eat skunk berry

/alias pherbs \
    oh %;\
    ph herbs %;\
    ch

; hilites

/def -F -PCgreen -mregexp -t'^You feel the nutritional benefits .*'
/def -F -PCred -mregexp -t'^The effects are wearing off\.$'
/def -F -PCred -mregexp -t'^Your vision flickers with a black halo'
/def -F -PCred -mregexp -t'^Your ability to discern objects in the dark has diminished\.$'

; definitions

/def -F -PCgreen -mregexp -t'^You feel ready to consume another herb\.$' herb_tick = \
    /echo %;\
    /herb_msg HERB COOLDOWN %;\
    /echo

/def herbing = \
    /if (herbing) \
        /set herbing 0 %;\
        /e @ automatic herb searching OFF %;\
    /else \
        /set herbing 1 %;\
        /e @ automatic herb searching ON %;\
    /endif

/def -F -mregexp -t'^You find .*\!$' herb_found = \
    /if (herbing) \
        oh %;\
        ph herbs %;\
        ch %;\
        hs %;\
    /endif

/def herb_msg = \
    /echo -aCcyan < %{*} >

; triggers

/def -F -mregexp -t'^You find a pale lavender flower\!$' = \
    /herb_msg Lobelia

/def -F -mregexp -t'^You find a broad golden leaf\!$' = \
    /herb_msg Oede

/def -F -mregexp -t'^You find a long gnarled root\!$' = \
    /herb_msg Sussur

/def -F -mregexp -t'^You find a pale white lichen\!$' = \
    /herb_msg Angel Tears

/def -F -mregexp -t'^You find a rough black bean\!$' = \
    /herb_msg Ranindir

/def -F -mregexp -t'^You find a big rotting bean\!$' = \
    /herb_msg Bellarnon

/def -F -mregexp -t'^You find a large glossy mushroom\!$' = \
    /herb_msg Marymoor

/def -F -mregexp -t'^You find a fluffy red moss\!$' = \
    /herb_msg Firemoss

/def -F -mregexp -t'^You find an ordinary delicate grass\!$' = \
    /herb_msg Adildra

/def -F -mregexp -t'^You find a petite velvety fruit\!$' = \
    /herb_msg Illavina

/def -F -mregexp -t'^You find a cluster of orange mushrooms\!$' = \
    /herb_msg Sun Stone

/def -F -mregexp -t'^You find a smelly brown fungus\!$' = \
    /herb_msg Ripplebark

/def -F -mregexp -t'^You find a glowing blue mushroom\!$' = \
    /herb_msg Glowcap

/def -F -mregexp -t'^You find a bitter red fruit\!$' = \
    /herb_msg Bloodfruit

/def -F -mregexp -t'^You find a narrow dry fungus\!$' = \
    /herb_msg Carolden

/def -F -mregexp -t'^You find a large prickly bark\!$' = \
    /herb_msg Osarni

/def -F -mregexp -t'^You find a small dry bulb\!$' = \
    /herb_msg Maicari

/def -F -mregexp -t'^You find a large blue leaf\!$' = \
    /herb_msg Selevon

/def -F -mregexp -t'^You find a brown bark\!$' = \
    /herb_msg Lebannen

/def -F -mregexp -t'^You find a lovely white blossom\!$' = \
    /herb_msg Vallenwood Blossom

/def -F -mregexp -t'^You find an air-filled warty alga\!$' = \
    /herb_msg Fucus

/def -F -mregexp -t'^You find a pinch of golden dust\!$' = \
    /herb_msg Corn Pollen

/def -F -mregexp -t'^You find a bit of strange moss\!$' = \
    /herb_msg Wilcol

/def -F -mregexp -t'^You find a black feathery leaf\!$' = \
    /herb_msg Darfern

/def -F -mregexp -t'^You find a black fiberous root\!$' = \
    /herb_msg Blackroot

/def -F -mregexp -t'^You find a black flower\!$' = \
    /herb_msg Aconite

/def -F -mregexp -t'^You find a black green-spotted mushroom\!$' = \
    /herb_msg Nightshade

/def -F -mregexp -t'^You find a black red-pitted vine\!$' = \
    /herb_msg Vinerot

/def -F -mregexp -t'^You find a black shiny berry\!$' = \
    /herb_msg Black Currant / Chokecherry

/def -F -mregexp -t'^You find a black velvety flower\!$' = \
    /herb_msg Belladonna

/def -F -mregexp -t'^You find a black-speckled orange flower\!$' = \
    /herb_msg Tiger Lily

/def -F -mregexp -t'^You find a blood red moss\!$' = \
    /herb_msg Vampiric Moss

/def -F -mregexp -t'^You find a blue flower\!$' = \
    /herb_msg Sebre

/def -F -mregexp -t'^You find a blue leaf\!$' = \
    /herb_msg Humbleleaf

/def -F -mregexp -t'^You find a blue mushroom\!$' = \
    /herb_msg Moosho

/def -F -mregexp -t'^You find a bright red fruit\!$' = \
    /herb_msg Tomato

/def -F -mregexp -t'^You find a bright yellow flower\!$' = \
    /herb_msg Marigold / Tansy

/def -F -mregexp -t'^You find a bright yellow leaf\!$' = \
    /herb_msg Kuko

/def -F -mregexp -t'^You find a bright-red sticky weed\!$' = \
    /herb_msg Bloodweed

/def -F -mregexp -t'^You find a broad flowery umbel\!$' = \
    /herb_msg Marjoram

/def -F -mregexp -t'^You find a broad silver leaf\!$' = \
    /herb_msg Sterling leaf

/def -F -mregexp -t'^You find a broad white leaf\!$' = \
    /herb_msg Culkas

/def -F -mregexp -t'^You find a brown bean\!$' = \
    /herb_msg Coffee Bean

/def -F -mregexp -t'^You find a brown broad-headed mushroom\!$' = \
    /herb_msg Astaldo / Tyelka

/def -F -mregexp -t'^You find a brown grass\!$' = \
    /herb_msg Pot

/def -F -mregexp -t'^You find a brown patch of moss\!$' = \
    /herb_msg Ungolestel

/def -F -mregexp -t'^You find a brown seaweed\!$' = \
    /herb_msg Nilgu

/def -F -mregexp -t'^You find a brown tree mushroom\!$' = \
    /herb_msg Fungiarbo

/def -F -mregexp -t'^You find a brown twisted root\!$' = \
    /herb_msg Gargleroot

/def -F -mregexp -t'^You find a bunch of leaves\!$' = \
    /herb_msg Rushwash

/def -F -mregexp -t'^You find a bunch of purple aromatic flowers\!$' = \
    /herb_msg Lilac

/def -F -mregexp -t'^You find a bunch of slender green leaves\!$' = \
    /herb_msg Chives

/def -F -mregexp -t'^You find a bunch of small hardy blood-red flowers\!$' = \
    /herb_msg Seregon

/def -F -mregexp -t'^You find a cluster of blood-red flowers\!$' = \
    /herb_msg Amaranth

/def -F -mregexp -t'^You find a coral-like grey lichen\!$' = \
    /herb_msg Cladina

/def -F -mregexp -t'^You find a cup-shaped flower\!$' = \
    /herb_msg Saffron

/def -F -mregexp -t'^You find a dark green leaf\!$' = \
    /herb_msg Spinach

/def -F -mregexp -t'^You find a dark spotted root\!$' = \
    /herb_msg Argil

/def -F -mregexp -t'^You find a delicate pale blue flower\!$' = \
    /herb_msg Elidross

/def -F -mregexp -t'^You find a delicate red-brown alga\!$' = \
    /herb_msg Sargassum

/def -F -mregexp -t'^You find a delicate yellow flower\!$' = \
    /herb_msg Calendula

/def -F -mregexp -t'^You find a dirty root\!$' = \
    /herb_msg Dragonroot

/def -F -mregexp -t'^You find a dry purplish fruit\!$' = \
    /herb_msg Beach Plum

/def -F -mregexp -t'^You find a dusty white leaf\!$' = \
    /herb_msg Solinaith

/def -F -mregexp -t'^You find an elongated slender leaf\!$' = \
    /herb_msg Beaked Sedge

/def -F -mregexp -t'^You find an emerald green leaf\!$' = \
    /herb_msg Columbine

/def -F -mregexp -t'^You find an enchanting shoe-shaped flower\!$' = \
    /herb_msg Lady's Slipper

/def -F -mregexp -t'^You find a few green leaves\!$' = \
    /herb_msg Hajir

/def -F -mregexp -t'^You find a forked root\!$' = \
    /herb_msg Ginseng

/def -F -mregexp -t'^You find a frosty red berry\!$' = \
    /herb_msg Frosty Raspberry

/def -F -mregexp -t'^You find a fuzzy rusty leaf\!$' = \
    /herb_msg Labrador Tea

/def -F -mregexp -t'^You find a ghostly black flower\!$' = \
    /herb_msg Narcissus

/def -F -mregexp -t'^You find a gleaming silver berry\!$' = \
    /herb_msg Silverberry / Stiltberry

/def -F -mregexp -t'^You find a globular yellow alga\!$' = \
    /herb_msg Sea Bubble

/def -F -mregexp -t'^You find a glowing yellow mushroom\!$' = \
    /herb_msg Glowshroom

/def -F -mregexp -t'^You find a gnarled dark-green vine\!$' = \
    /herb_msg Athly

/def -F -mregexp -t'^You find a golden flower\!$' = \
    /herb_msg Alfirin

/def -F -mregexp -t'^You find a gray fuzzy leaf\!$' = \
    /herb_msg Frostheal / Madwort

/def -F -mregexp -t'^You find a green berry\!$' = \
    /herb_msg Huckleberry

/def -F -mregexp -t'^You find a green floweret\!$' = \
    /herb_msg Broccoli

/def -F -mregexp -t'^You find a green leaf\!$' = \
    /herb_msg Parsley / Angurth / Athelas / Jewelweed

/def -F -mregexp -t'^You find a green rose\!$' = \
    /herb_msg Pawnrose

/def -F -mregexp -t'^You find a green sea fern\!$' = \
    /herb_msg Emmelti

/def -F -mregexp -t'^You find a green straw\!$' = \
    /herb_msg Evendim Grass

/def -F -mregexp -t'^You find a green trefoil\!$' = \
    /herb_msg Satilia

/def -F -mregexp -t'^You find a green wreath\!$' = \
    /herb_msg Bayberry / Luvico

/def -F -mregexp -t'^You find a green-topped black carrot\!$' = \
    /herb_msg Black Carrot

/def -F -mregexp -t'^You find a green-topped orange carrot\!$' = \
    /herb_msg Orange Carrot

/def -F -mregexp -t'^You find a greenish brown moss\!$' = \
    /herb_msg Sphagnum

/def -F -mregexp -t'^You find a grey bark\!$' = \
    /herb_msg Grey Willow

/def -F -mregexp -t'^You find a grey gnarled bark\!$' = \
    /herb_msg Beach Willow

/def -F -mregexp -t'^You find a grey mushroom\!$' = \
    /herb_msg Ylpoe

/def -F -mregexp -t'^You find a gritty segmented alga\!$' = \
    /herb_msg Coralline Red Alga

/def -F -mregexp -t'^You find a gritty slender stalk\!$' = \
    /herb_msg Horsetail

/def -F -mregexp -t'^You find a handful of aromatic pale purplish flowers\!$' = \
    /herb_msg Catnip

/def -F -mregexp -t'^You find a handful of feathery leaves\!$' = \
    /herb_msg Dill

/def -F -mregexp -t'^You find a handful of fine threadlike leaves\!$' = \
    /herb_msg Fennel

/def -F -mregexp -t'^You find a handful of fuzzy dark-green leaves\!$' = \
    /herb_msg Nettle

/def -F -mregexp -t'^You find a handful of gray-green leaves\!$' = \
    /herb_msg Lavender

/def -F -mregexp -t'^You find a handful of green berries\!$' = \
    /herb_msg Hemlock

/def -F -mregexp -t'^You find a handful of grey moss\!$' = \
    /herb_msg Pindan

/def -F -mregexp -t'^You find a handful of narrow sharply toothed leaves\!$' = \
    /herb_msg Mint

/def -F -mregexp -t'^You find a handful of pungent narrow leaves\!$' = \
    /herb_msg Thyme

/def -F -mregexp -t'^You find a handful of purple flowers\!$' = \
    /herb_msg Foxglove

/def -F -mregexp -t'^You find a handful of small bitter leaves\!$' = \
    /herb_msg Tarragon

/def -F -mregexp -t'^You find a handful of small brown aromatic seeds\!$' = \
    /herb_msg Caraway

/def -F -mregexp -t'^You find a handful of small dark green leaves\!$' = \
    /herb_msg Basil

/def -F -mregexp -t'^You find a handful of small fine leaves\!$' = \
    /herb_msg Chervil

/def -F -mregexp -t'^You find a handful of small pale blue flowers\!$' = \
    /herb_msg Rosemary

/def -F -mregexp -t'^You find a handful of small purple berries\!$' = \
    /herb_msg Skunk Berries

/def -F -mregexp -t'^You find a handful of small red fruits\!$' = \
    /herb_msg Pepper

/def -F -mregexp -t'^You find a handful of small stiff aromatic leaves\!$' = \
    /herb_msg Savory

/def -F -mregexp -t'^You find a handful of stalked ovate leaves\!$' = \
    /herb_msg Oregano

/def -F -mregexp -t'^You find a handful of thin grey weeds\!$' = \
    /herb_msg Strangleweed

/def -F -mregexp -t'^You find a handful of thin-shelled nuts\!$' = \
    /herb_msg Peanuts

/def -F -mregexp -t'^You find a handful of tiny grey mushrooms\!$' = \
    /herb_msg Fordinfa

/def -F -mregexp -t'^You find a handful of tubular two-lipped flowers\!$' = \
    /herb_msg Sage

/def -F -mregexp -t'^You find a handful of white and pale green slender-stalked flowers\!$' = \
    /herb_msg Niphredil

/def -F -mregexp -t'^You find a handful of white star-shaped flowers\!$' = \
    /herb_msg Ithilgil

/def -F -mregexp -t'^You find a handful of yellow star-shaped flowers\!$' = \
    /herb_msg Elanor

/def -F -mregexp -t'^You find a hard enbracted nut\!$' = \
    /herb_msg Hazelnut

/def -F -mregexp -t'^You find a large blue flower\!$' = \
    /herb_msg Blue Gentian Flower

/def -F -mregexp -t'^You find a large fan-like mushroom\!$' = \
    /herb_msg Oyster Mushroom

/def -F -mregexp -t'^You find a large purple berry\!$' = \
    /herb_msg Mindleech

/def -F -mregexp -t'^You find a large red berry\!$' = \
    /herb_msg Strawberry

/def -F -mregexp -t'^You find a large scaly bulb\!$' = \
    /herb_msg Daffodil

/def -F -mregexp -t'^You find a licorice-scented pliable root\!$' = \
    /herb_msg Sarsaparilla

/def -F -mregexp -t'^You find a light green bean\!$' = \
    /herb_msg Lima Bean

/def -F -mregexp -t'^You find a lilac bulb\!$' = \
    /herb_msg Moly

/def -F -mregexp -t'^You find a linear green leaf\!$' = \
    /herb_msg Eel Grass

/def -F -mregexp -t'^You find a linear keeled leaf\!$' = \
    /herb_msg Smooth Camas

/def -F -mregexp -t'^You find a little black berry\!$' = \
    /herb_msg Blackberry / Black Raspberry

/def -F -mregexp -t'^You find a little black potato\!$' = \
    /herb_msg Black Potato

/def -F -mregexp -t'^You find a little blue berry\!$' = \
    /herb_msg Saskatoon / Blueberry / Attanar

/def -F -mregexp -t'^You find a little green bean\!$' = \
    /herb_msg Green Bean

/def -F -mregexp -t'^You find a little green onion\!$' = \
    /herb_msg Green Onion

/def -F -mregexp -t'^You find a little green-topped orange carrot\!$' = \
    /herb_msg Little Orange Carrot

/def -F -mregexp -t'^You find a little red bean\!$' = \
    /herb_msg Kidney Bean

/def -F -mregexp -t'^You find a little red berry\!$' = \
    /herb_msg Holly Berry / Red Raspberry / Wild Strawberry

/def -F -mregexp -t'^You find a little red potato\!$' = \
    /herb_msg Red Potato

/def -F -mregexp -t'^You find a long rough leaf\!$' = \
    /herb_msg Lamia

/def -F -mregexp -t'^You find a long tangled vine\!$' = \
    /herb_msg Ronwath

/def -F -mregexp -t'^You find a long triangular leaf\!$' = \
    /herb_msg Angelica

/def -F -mregexp -t'^You find a mild smelly white rose-tinged bulb\!$' = \
    /herb_msg Garlic

/def -F -mregexp -t'^You find a narrow rotting fern\!$' = \
    /herb_msg Lorea

/def -F -mregexp -t'^You find an oily drooping leaf\!$' = \
    /herb_msg Poison Ivy

/def -F -mregexp -t'^You find an orange berry\!$' = \
    /herb_msg Lore

/def -F -mregexp -t'^You find an oval-shaped leaf\!$' = \
    /herb_msg Wintergreen Leaf

/def -F -mregexp -t'^You find a pale purplish flower\!$' = \
    /herb_msg Crocus

/def -F -mregexp -t'^You find a pale thick root\!$' = \
    /herb_msg Marshmallow

/def -F -mregexp -t'^You find a pale white flower\!$' = \
    /herb_msg Morgurth

/def -F -mregexp -t'^You find a pale white mushroom\!$' = \
    /herb_msg Angeltear

/def -F -mregexp -t'^You find a pale whitish-blue flower\!$' = \
    /herb_msg Goats Rue

/def -F -mregexp -t'^You find a pale yellow flower\!$' = \
    /herb_msg Hensbane

/def -F -mregexp -t'^You find a patch of crimson lichen\!$' = \
    /herb_msg Bloodcreep

/def -F -mregexp -t'^You find a petite two-petalled flower\!$' = \
    /herb_msg Veronica

/def -F -mregexp -t'^You find a piece of brown lichen\!$' = \
    /herb_msg Oakmoss

/def -F -mregexp -t'^You find a piece of pale green lichen\!$' = \
    /herb_msg Tuo

/def -F -mregexp -t'^You find a piece of white bark\!$' = \
    /herb_msg Aspen Bark

/def -F -mregexp -t'^You find a pink twinned flower\!$' = \
    /herb_msg Honeysuckle

/def -F -mregexp -t'^You find a pinkish flower\!$' = \
    /herb_msg Yarrow

/def -F -mregexp -t'^You find a pitch black root\!$' = \
    /herb_msg Darkroot

/def -F -mregexp -t'^You find a prickly green fruit\!$' = \
    /herb_msg Wild Cucumber

/def -F -mregexp -t'^You find a puffy yellow mushroom\!$' = \
    /herb_msg Chantrelle

/def -F -mregexp -t'^You find a purple blade of grass\!$' = \
    /herb_msg Witch Grass

/def -F -mregexp -t'^You find a purple frond\!$' = \
    /herb_msg Lukilvor

/def -F -mregexp -t'^You find a purple hoary flower\!$' = \
    /herb_msg Locoweed

/def -F -mregexp -t'^You find a purple mushroom\!$' = \
    /herb_msg Numbis

/def -F -mregexp -t'^You find a purple-green shaggy moss\!$' = \
    /herb_msg Blung

/def -F -mregexp -t'^You find a red berry\!$' = \
    /herb_msg Beatha / Cranberry / Hawthorn / Ramira / Oreste

/def -F -mregexp -t'^You find a red black-spotted mushroom\!$' = \
    /herb_msg Fungari

/def -F -mregexp -t'^You find a red flower\!$' = \
    /herb_msg Gnarglefrix / Pendick-Flower / Rutgeldam

/def -F -mregexp -t'^You find a red heart-shaped flower\!$' = \
    /herb_msg Lothore

/def -F -mregexp -t'^You find a red leaf\!$' = \
    /herb_msg Redwood Leaf

/def -F -mregexp -t'^You find a red mushroom\!$' = \
    /herb_msg Coccinea

/def -F -mregexp -t'^You find a red oily vine\!$' = \
    /herb_msg Sumac

/def -F -mregexp -t'^You find a red weed\!$' = \
    /herb_msg Redweed

/def -F -mregexp -t'^You find a red yellow-spotted root\!$' = \
    /herb_msg Flameroot

/def -F -mregexp -t'^You find a resinous amber sap\!$' = \
    /herb_msg Green Pine / Uaine Pine

/def -F -mregexp -t'^You find a ribbon-like linear leaf\!$' = \
    /herb_msg Bur Reed

/def -F -mregexp -t'^You find a river weed\!$' = \
    /herb_msg Adillyp Weed

/def -F -mregexp -t'^You find a rotting black mushroom\!$' = \
    /herb_msg Skullcap

/def -F -mregexp -t'^You find a round veined leaf\!$' = \
    /herb_msg Nasturtium

/def -F -mregexp -t'^You find a scratchy green leaf\!$' = \
    /herb_msg Seaside Grass

/def -F -mregexp -t'^You find a serrated leaf\!$' = \
    /herb_msg Dandelion

/def -F -mregexp -t'^You find a shadowy green onion\!$' = \
    /herb_msg Shadow Onion

/def -F -mregexp -t'^You find a sharp short leaf\!$' = \
    /herb_msg Chicory

/def -F -mregexp -t'^You find a sheet-like green alga\!$' = \
    /herb_msg Sea Lettuce

/def -F -mregexp -t'^You find a showy purple flower\!$' = \
    /herb_msg Fireweed

/def -F -mregexp -t'^You find a silvery leaf\!$' = \
    /herb_msg Curugwath

/def -F -mregexp -t'^You find a silvery weed\!$' = \
    /herb_msg Cinquefoil

/def -F -mregexp -t'^You find a simple delicate fern\!$' = \
    /herb_msg Onoclea

/def -F -mregexp -t'^You find a sinuous green-brown alga\!$' = \
    /herb_msg Elkhorn Kelp / Bull Kelp

/def -F -mregexp -t'^You find a slender green leaf\!$' = \
    /herb_msg Surf Grass

/def -F -mregexp -t'^You find a slender luminous vine\!$' = \
    /herb_msg Mittikna

/def -F -mregexp -t'^You find a slimy black mushroom\!$' = \
    /herb_msg Drudgeworth

/def -F -mregexp -t'^You find a slimy brown alga\!$' = \
    /herb_msg Brown Slime

/def -F -mregexp -t'^You find a slimy green alga\!$' = \
    /herb_msg Green Slime / Laminaria

/def -F -mregexp -t'^You find a slimy red alga\!$' = \
    /herb_msg Nethra

/def -F -mregexp -t'^You find a slippery brown moss\!$' = \
    /herb_msg Sea Moss

/def -F -mregexp -t'^You find a slippery brown vine\!$' = \
    /herb_msg Sea Kelp

/def -F -mregexp -t'^You find a small aromatic clove\!$' = \
    /herb_msg Wild Garlic

/def -F -mregexp -t'^You find a small black berry\!$' = \
    /herb_msg Bilberry

/def -F -mregexp -t'^You find a small green wrinkled berry\!$' = \
    /herb_msg Laranthas

/def -F -mregexp -t'^You find a small green-yellow mushroom\!$' = \
    /herb_msg Handasse

/def -F -mregexp -t'^You find a small greenish flower bud\!$' = \
    /herb_msg Clove

/def -F -mregexp -t'^You find a small golden translucent fruit\!$' = \
    /herb_msg Annalda

/def -F -mregexp -t'^You find a small light brown bean\!$' = \
    /herb_msg Broad Bean

/def -F -mregexp -t'^You find a small orange pumpkin\!$' = \
    /herb_msg Pumpkin

/def -F -mregexp -t'^You find a small pear-shaped fruit\!$' = \
    /herb_msg Bergamot

/def -F -mregexp -t'^You find a small purple berry\!$' = \
    /herb_msg Lianor Berry

/def -F -mregexp -t'^You find a small quatrefoil leaf\!$' = \
    /herb_msg Four-Leaf Clover

/def -F -mregexp -t'^You find a small red berry\!$' = \
    /herb_msg Suranie

/def -F -mregexp -t'^You find a small red flower\!$' = \
    /herb_msg Loyol

/def -F -mregexp -t'^You find a small trifoliate leaf\!$' = \
    /herb_msg Three-Leaf Clover

/def -F -mregexp -t'^You find a small tuberous root\!$' = \
    /herb_msg Monkshood

/def -F -mregexp -t'^You find a small white berry\!$' = \
    /herb_msg Whitehorn

/def -F -mregexp -t'^You find a small white flower\!$' = \
    /herb_msg Simbelmyne

/def -F -mregexp -t'^You find a small wrinkled root\!$' = \
    /herb_msg Glimmer Root

/def -F -mregexp -t'^You find a smooth purplish fruit\!$' = \
    /herb_msg Wild Plum

/def -F -mregexp -t'^You find a snow-white mushroom\!$' = \
    /herb_msg Amanita

/def -F -mregexp -t'^You find a soft translucent mushroom\!$' = \
    /herb_msg Blade

/def -F -mregexp -t'^You find a spongelike fungus\!$' = \
    /herb_msg Morel

/def -F -mregexp -t'^You find a sprig of green leaves\!$' = \
    /herb_msg Bee Balm

/def -F -mregexp -t'^You find a stiff dark green leaf\!$' = \
    /herb_msg Laurel

/def -F -mregexp -t'^You find a stiff narrow leaf\!$' = \
    /herb_msg Soapweed

/def -F -mregexp -t'^You find a stinking buttercup\!$' = \
    /herb_msg Hellebore Stinkwort

/def -F -mregexp -t'^You find a strip of grey bark\!$' = \
    /herb_msg White Cedar / Aromatic Cedar

/def -F -mregexp -t'^You find a striped purple mushroom\!$' = \
    /herb_msg Crucicrescent

/def -F -mregexp -t'^You find a strong-smelling gnarled root\!$' = \
    /herb_msg Licorice

/def -F -mregexp -t'^You find a succulent rotund leaf\!$' = \
    /herb_msg Salt Wort

/def -F -mregexp -t'^You find a sweet-smelling golden flower\!$' = \
    /herb_msg Lissuin

/def -F -mregexp -t'^You find a tall reedlike marsh plant\!$' = \
    /herb_msg Cattail

/def -F -mregexp -t'^You find a tall yellowish leaf\!$' = \
    /herb_msg Cord Grass

/def -F -mregexp -t'^You find a thick brown root\!$' = \
    /herb_msg Cattail / Breadroot

/def -F -mregexp -t'^You find a thick large leaf\!$' = \
    /herb_msg Borage

/def -F -mregexp -t'^You find a thin dark-brown straw\!$' = \
    /herb_msg Strim

/def -F -mregexp -t'^You find a thin reddish root\!$' = \
    /herb_msg Streyroot

/def -F -mregexp -t'^You find a thin trailing vine\!$' = \
    /herb_msg Carlith

/def -F -mregexp -t'^You find a tiny black bean\!$' = \
    /herb_msg Black Bean

/def -F -mregexp -t'^You find a tiny black mushroom\!$' = \
    /herb_msg Dajla

/def -F -mregexp -t'^You find a tiny black root\!$' = \
    /herb_msg Drakeroot

/def -F -mregexp -t'^You find a translucent red berry\!$' = \
    /herb_msg Myrtleberry

/def -F -mregexp -t'^You find a trifoil leaf\!$' = \
    /herb_msg Shamrock

/def -F -mregexp -t'^You find a tuberous rootstock\!$' = \
    /herb_msg Ginger

/def -F -mregexp -t'^You find a twisted brown root\!$' = \
    /herb_msg Khuz

/def -F -mregexp -t'^You find a twisted gnarly root\!$' = \
    /herb_msg Mandrake

/def -F -mregexp -t'^You find a twisted thorned vine\!$' = \
    /herb_msg Tangleshoot

/def -F -mregexp -t'^You find an unfurling green frond\!$' = \
    /herb_msg Fiddlehead

/def -F -mregexp -t'^You find an upright green stalk\!$' = \
    /herb_msg Asparagus

/def -F -mregexp -t'^You find a veined oval leaf\!$' = \
    /herb_msg Bunchberry

/def -F -mregexp -t'^You find a velvety purple flower\!$' = \
    /herb_msg Morning Glory

/def -F -mregexp -t'^You find a white blossom\!$' = \
    /herb_msg White Hallow

/def -F -mregexp -t'^You find a white red-eyed bean\!$' = \
    /herb_msg Redeye

/def -F -mregexp -t'^You find a white root\!$' = \
    /herb_msg Tilhiyin

/def -F -mregexp -t'^You find a wide black mushroom\!$' = \
    /herb_msg Chartess

/def -F -mregexp -t'^You find a woody twining vine\!$' = \
    /herb_msg Common Hop

/def -F -mregexp -t'^You find a yellow drooping flower\!$' = \
    /herb_msg Maroea

/def -F -mregexp -t'^You find a yellow flower\!$' = \
    /herb_msg Wildfeather / Vamene

/def -F -mregexp -t'^You find a yellow fruit\!$' = \
    /herb_msg Paramol

/def -F -mregexp -t'^You find a yellow-red flower\!$' = \
    /herb_msg Sparkweed

/def -F -mregexp -t'^You find a yellowish twisted tuber\!$' = \
    /herb_msg Yam
