;
; Scrying
;

; definitions

/def scry_msg = \
    /echo %;\
    /echo -aCred <<< SCRY: %* >>> %;\
    /echo %;\
    /if (team) \
        /send whisper to team Scried by %*! %;\
    /endif

; triggers

/def -F -PBCwhite -mregexp -t'^For a very brief moment, an alien presence enters your mind' warlock_scry = \
    /scry_msg Warlock
    
/def -F -PBCwhite -mregexp -t'^You briefly feel an (alien|intruding) presence (nearby|seeking you out), but the feeling passes' pot_scry = \
    /scry_msg Priest of Takhisis

/def -F -PBCwhite -mregexp -t'^You sense an intruding presence in your mind' wohs_scry = \
    /scry_msg High Sorcery

/def -F -PBCwhite -mregexp -t'^You feel you are being watched' crystal_scry = \
    /scry_msg scrying crystal
