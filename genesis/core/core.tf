/load ~/scripts/genesis/core/hilites.tf
/load ~/scripts/genesis/core/aliases.tf
/load ~/scripts/genesis/core/items.tf
/load ~/scripts/genesis/core/travel.tf
/load ~/scripts/genesis/core/team.tf
/load ~/scripts/genesis/core/combat.tf
/load ~/scripts/genesis/core/herbs.tf
/load ~/scripts/genesis/core/imbuements.tf
/load ~/scripts/genesis/core/scries.tf
/load ~/scripts/genesis/core/krynn_warfare.tf
/load ~/scripts/genesis/robots/kill_scripts/kill_script.tf

/def find = /recall -t$["%H:%M:%S"] -w -mglob 0- *%{*}*

/def -F -mregexp -t'^Your tummy is a bit upset\!$' zodiac_gem = /send excuse

/set vitals_action vitals
/set drink_action drink from bottle

/alias vitals_forever \
    /send %vitals_action %;\
    /repeat -00:05:00 1 %vitals_command

/alias viton \
    /if ({*} !~ "") \
        /set vitals_action %{*} %;\
    /endif %;\
    /set vitals_command vitals_forever %;\
    vitals_forever

/alias vitoff /unset vitals_command

/alias drink_forever \
    /send %drink_action %;\
    /repeat -00:03:00 1 %drink_command

/alias drinkon \
    /if ({*} !~ "") \
        /set drink_action %{*} %;\
    /endif %;\
    /set drink_command drink_forever %;\
    drink_forever

/alias drinkoff /unset drink_command
