;
; Travel
;

; variables

/set boarding 1

; aliases

/alias t /send enter tent
/alias wg /send whistle for griffon
/alias mg /send mount griffon %; hold on
/alias sd /send slide down
/alias dh /send downhill
/alias uh /send uphill
/alias kn /send knock on gate
/alias sr /send swim river
/alias hp /send hit pipe
/alias bo /send buy tickets for team %; board
/alias ds /send disembark
/alias eb /send enter bushes

; definitions

/def boarding = \
    /if (boarding) \
        /set boarding 0 %;\
     	/e @ automatic boarding OFF %;\
    /else \
        /set boarding 1 %;\
        /e @ automatic boarding ON %;\
    /endif

; triggers

/def -Fp1 -mregexp -t'navigates in\.$' autoboard = \
    /if (boarding) \
        /repeat -2 1 bo %;\
    /endif

/def -Fp1 -mregexp -t'just came in\.$' autoboard_old = \
    /if (boarding) \
        /repeat -2 1 bo %;\
    /endif

/def -Fp1 -mregexp -t'^A fierce roc lands in a swirl of dust\.$' autoboard_roc = \
    /if (boarding) \
        /repeat -2 1 bo %;\
    /endif

/def -Fp1 -mregexp -t'^A wooden boat is (thrown on the beach|jammed up against the float) by some great force\.$' board_boat = \
    /if (boarding) \
        /send board boat %;\
    /endif

/def -Fp1 -mregexp -t'^A wooden boat drifts into view and lands gently on the beach\.$' board_boat2 = \
    /if (boarding) \
        /send board boat %;\
    /endif

/def -Fp1 -mregexp -t'Disembark\!' autodisembark = \
    /if (boarding) \
        /send disembark %;\
    /endif
/def -Fp1 -mregexp -t'^The boat (drifts into the beach|bumps up against the float) and stops\.$' autodisembark2 = \
    /if (boarding) \
        /send disembark %;\
    /endif

/def -Fp1 -mregexp -t'^You hardly keep your balance as the boat is thrown on the sandy beach\.$' autodisembark3 = \
    /if (boarding) \
        /send disembark %;\
    /endif

/def -Fp1 -mregexp -t'^You hardly keep your balance as the boat is thrown against the wooden float\.$' autodisembark4 = \
    /if (boarding) \
        /send disembark %;\
    /endif

/def -Fp1 -mregexp -t'^The basket arrives from' board_basket = \
    /if (boarding) \
        /send enter basket %;\
    /endif

/def -Fp1 -mregexp -t'^The platform shudders .* its destination\.$' leave_basket = \
    /if (boarding) \
        /send out %;\
    /endif

/def -Fp1 -mregexp -t'^A large covered wagon rumbles in\.$' sanc_wag = \
    /if (boarding) \
        /send pay driver %;\
        /send board wagon %;\
    /endif

/def -Fp1 -mregexp -t'^The wagon stops as you reach your destination\.$' sanc_wag_2 = \
    /if (boarding) \
        /send out %;\
    /endif

/def -F -mregexp -t'^Knock on what\? The gates\?$' enter_gates = \
    /send knock on gates

/def -F -mregexp -t'^The wooden door is closed\.$' door_closed = \
    /send open door
