;
; Combat
;

; variables

/set atk 0
/set atkvictim none
/set kills 0
/set player_killing 0
/set drac_disarm 0

; aliases

/alias comp /send compare stats with %{*}
/alias ee /send exa enemy

/alias k \
    /if ({*} =~ none) \
        /send kill %atkvictim %;\
    /else \
        /send kill %{*} %;\
        /set atkvictim %{*} %;\
    /endif

/alias ka \
    k %{*} %;\
    /send kill second %atkvictim %;\
    /send kill third %atkvictim %;\
    /send kill fourth %atkvictim %;\
    /send kill fifth %atkvictim

/alias bc /e @ your bodycount is at %kills

/alias pk \
    /if (!player_killing & ({*} =~ "on")) \
        /send nick assist assist! %;\
        /send nick kill kill! %;\
        /set player_killing 1 %;\
        /e @ player-killing ON %;\
    /else \
        /send unnick assist %;\
        /send unnick kill %;\
        /set player_killing 0 %;\
        /e @ player-killing OFF %;\
    /endif

; definitions

/def atk = \
    /if (atk) \
        /set atk 0 %;\
        /e @ auto-killing OFF %;\
    /else \
        /set atk 1 %;\
        /e @ auto-killing ON %;\
    /endif

/def creset = \
    /e @ resetting combat variables %;\
    /set atk 0 %;\
    /set atkvictim none %;\
    /set kills 0

; triggers

/def -F -mregexp -t'attacks you\!$' defend_msg = \
    /echo -aCred <<< ATTACK >>>

/def -F -PCred -mregexp -t'>? ?([A-Z][a-z]*) killed .*\.$' kill_check = \
    /if ({P1} =~ "You") \
        /set kills $[kills+1] %;\
        /e @ your bodycount has reached %kills %;\
        /if (coins) \
            /send get coins from corpse %;\
        /endif %;\
        /if (gems) \
            /send get gems from corpse %;\
        /endif %;\
        /if (imbuements) \
            /send get imbued items from corpse %;\
        /endif %;\
        /if (atk) \
            k %atkvictim %;\
        /endif %;\
    /endif

/def -F -t'*gives you*vial*' vial_drop = drop vials

/def -F -PCred -mregexp -t'^You feel your .* get incredibly hot and burn your hands\! You quickly unwield it\.$' = \
    /set drac_disarm 1 %;\
    /echo %;\
    /echo -aCred < disarmed! > %;\
    /echo %;\
    /repeat -10 1 /send wield all

/def -F -PCred -mregexp -t'^The .* is still magically superheated and it cannot be wielded\.$' = \
    /repeat -5 1 /send wield all

/def -F -PCwhite -mregexp -t'^Your .* no longer feels magically superheated, and you can wield it again\.$' = \
    /if (drac_disarm) \
        /send wield all %;\
    /endif

/def -F -mregexp -t'^You wield' = \
    /if (drac_disarm) \
        /repeat -2 1 /set drac_disarm 0 %;\
/endif

/def -F -PCred -mregexp -t'^You feel your mind being dominated .*\.$' = \
    /echo %;\
    /echo -aCred < stunned! > %;\
    /echo %;\

/def -F -PCred -mregexp -t'^Your mins is being dominated .*\.$' = \
    /echo %;\
    /echo -aCred < stunned! > %;\
    /echo %;\
