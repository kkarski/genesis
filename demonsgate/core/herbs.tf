;
; Herbing
;

; variables

/set herbing 1
/set hfind herbs
/set hcontainer pouch

; aliases

/alias hfind \
    /if ({*} =~ "") \
        /e @ currently searching for: %{hfind} %;\
    /else \
        /set hfind %{*} %;\
        /e @ currently searching for: %{hfind} %;\
    /endif

/alias hcontainer \
    /if ({*} =~ "") \
        /e @ current herb container: %{hcontainer} %;\
    /else \
        /set hcontainer %{*} %;\
        /e @ current herb container: %{hcontainer} %;\
    /endif

/alias hs /send search here for %{hfind}
/alias oh /send open %{hcontainer}
/alias ch /send close %{hcontainer}
/alias ph /send put %{*} in %{hcontainer}
/alias gh /send get %{*} from %{hcontainer}
/alias hc \
    oh %;\
    gh %{1} %;\
    ch %;\
    /if ({#} > 1) \
        /send %{2} %{1} %;\
    /else \
        /send eat %{1} %;\
    /endif

; definitions

/def herbing = \
    /if (herbing) \
        /set herbing 0 %;\
        /e @ automatic herb searching OFF %;\
    /else \
        /set herbing 1 %;\
        /e @ automatic herb searching ON %;\
    /endif

/def -F -mregexp -t'^You find .*\!$' herb_found = \
    /if (herbing) \
        oh %;\
        ph herbs %;\
        ch %;\
        hs %;\
    /endif

/def herb_msg = \
    /echo -aCcyan < %{*} >

; triggers

/def -F -mregexp -t'^You find a golden berry\!$' = \
    /herb_msg Astorax

/def -F -mregexp -t'^You find a red berry\!$' = \
    /herb_msg Bear Berry / Tarhelimno

/def -F -mregexp -t'^You find a dark\-brown hard mushroom\!$' = \
    /herb_msg Blaays

/def -F -mregexp -t'^You find a blade of thin sharp grass\!$' = \
    /herb_msg Bladegrass

/def -F -mregexp -t'^You find a black feathery leaf\!$' = \
    /herb_msg Darfern

/def -F -mregexp -t'^You find a green straw\!$' = \
    /herb_msg Dark Straw

/def -F -mregexp -t'^You find a bunch of jagged leaves\!$' = \
    /herb_msg Draenar

/def -F -mregexp -t'^You find a red petal\!$' = \
    /herb_msg Inspirations

/def -F -mregexp -t'^You find a grainy russet nut\!$' = \
    /herb_msg Malion

/def -F -mregexp -t'^You find a bright blue stem\!$' = \
    /herb_msg Marata

/def -F -mregexp -t'^You find a small white flower\!$' = \
    /herb_msg White Nightstar

/def -F -mregexp -t'^You find a yellow mushroom\!$' = \
    /herb_msg Oak Hat

/def -F -mregexp -t'^You find a green leaf\!$' = \
    /herb_msg Ribleaf

/def -F -mregexp -t'^You find a dark brown root\!$' = \
    /herb_msg Soran

/def -F -mregexp -t'^You find a brown mushroom\!$' = \
    /herb_msg Tarrinian Brownbell

/def -F -mregexp -t'^You find a yellow berry\!$' = \
    /herb_msg Tarrinian Sunshine
