;
; Aliases
;

/alias int /send introduce %{*}
/alias h /send health all
/alias ht /send health team
/alias dr /send drink from bottle
/alias fixme \
    /send sharpen wielded weapon %;\
    /send sharpen second wielded weapon %;\
    /send fix helm %;\
    /send fix shield %;\
    /send fix bracers %;\
    /send fix greaves %;\
    /send fix robe %;\
    /send fix pelt %;\
    /send fix cloak %;\
    /send fix medallion %;\
    /send fix chainmail %;\
    /send fix platemail %;\
    /send fix scale %;\
    /send fix boots

/alias glow /CYAN %{*}
