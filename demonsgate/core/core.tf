/load ~/scripts/demonsgate/core/hilites.tf
/load ~/scripts/demonsgate/core/aliases.tf
/load ~/scripts/demonsgate/core/items.tf
/load ~/scripts/demonsgate/core/travel.tf
/load ~/scripts/demonsgate/core/team.tf
/load ~/scripts/demonsgate/core/combat.tf
/load ~/scripts/demonsgate/core/herbs.tf

/def find = /recall -t$["%H:%M:%S"] -w -mglob 0- *%{*}*

/set vitals_action vitals
/set drink_action drink from bottle

/alias vitals_forever \
    /send %vitals_action %;\
    /repeat -00:05:00 1 %vitals_command

/alias viton \
    /if ({*} !~ "") \
        /set vitals_action %{*} %;\
    /endif %;\
    /set vitals_command vitals_forever %;\
    vitals_forever

/alias vitoff /unset vitals_command

/alias drink_forever \
    /send %drink_action %;\
    /repeat -00:03:00 1 %drink_command

/alias drinkon \
    /if ({*} !~ "") \
        /set drink_action %{*} %;\
    /endif %;\
    /set drink_command drink_forever %;\
    drink_forever

/alias drinkoff /unset drink_command
