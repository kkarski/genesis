;
; Teams
;

; variables

/set assisted 0
/set assisting 0
/set team 0
/set leader_ask 0
/set leader none
/set members none

; aliases

/alias a /send assist %{*}
/alias j /send whisper to team %{*}
/alias lask /leader_ask

; definitions

/def assisting = \
    /if (assisting) \
        /set assisting 0 %;\
        /e @ automatic assisting OFF %;\
    /else \
        /set assisting 1 %;\
        /e @ automatic assisting ON %;\
    /endif

/def check_assist = \
    /if (assisting & !assisted) \
        /set assisted 1 %;\
        /repeat -2 1 /send assist %;\
        /repeat -2 1 /set assisted 0 %;\
    /endif

/def leader_ask = \
    /if (leader_ask) \
        /set leader_ask 0 %;\
        /e @ leader ask OFF %;\
    /else \
        /set leader_ask 1 %;\
        /e @ leader ask ON %;\
    /endif

/def team = \
    /if (team) \
        /set team 0 %;\
        /e @ team status OFF %;\
    /else \
        /set team 1 %;\
        /e @ team status ON %;\
    /endif

/def leader = \
    /set leader %{*} %;\
    /if (leader =~ "") \
        /set assisted 0 %;\
        /set assisting 0 %;\
        /set leader none %;\
        /if (team) \
            /team %;\
        /endif %;\
        /if (!boarding) \
            /boarding %;\
        /endif %;\
    /else \
        /set leader %{*} %;\
        /if (atk) \
            /atk %;\
        /endif %;\
        /if (!team) \
            /team %;\
        /endif %;\
        /if (!assisting) \
            /assisting %;\
        /endif %;\
        /if (boarding) \
            /boarding %;\
        /endif %;\
    /endif %;\
    /e @ team leader is now: %leader

/def treset = \
    /e @ resetting team variables %;\
    /set assisted 0 %;\
    /set assisting 0 %;\
    /set team 0 %;\
    /set leader none

; triggers

/def -F -mregexp -t'^Your leader is now ([A-Za-z -]*)\.$' joined = \
     /leader %{P1}

/def -F -mregexp -t'^You are now led by ([A-Za-z -]*)\.$' leader_change = \
     /leader %{P1}

/def -F -mregexp -t'^You make ([A-Za-z -]*) the leader of your team\.$' leader_change_2 = \
     /leader %{P1}

/def -F -mregexp -t'>? ?([A-Z][a-z]*) joins your team\.$' someone_joined = \
    /if (!team) \
        /team %;\
    /endif

/def -F -mregexp -t'^You leave the team of .*\.$' left_team = \
    /if (leader !~ "none") \
        /set leader none %;\
        /set assisted 0 %;\
        /set assisting 0 %;\
        /if (team) \
            /team %;\
        /endif %;\
        /if (!boarding) \
            /boarding %;\
        /endif %;\
    /endif

/def -F -mregexp -t'makes you the leader of (his|her|its) team\.$' team_leading = \
    /if (leader !~ "none") \
        /set assisted 0 %;\
        /set leader none %;\
    /endif %;\
    /if (!boarding) \
        /boarding %;\
    /endif

/def -F -mregexp -t'forces you to leave (his|her|its) team\.$' team_forced = \
    /if (leader !~ "none") \
        /set leader none %;\
        /set assisted 0 %;\
        /set assisting 0 %;\
        /if (team) \
            /team %;\
        /endif %;\
        /if (!boarding) \
            /boarding %;\
        /endif %;\
    /endif

/def -F -mregexp -t'^You disband your team\.$' team_disband_me = \
    /treset %;\
    /if (!boarding) \
        /boarding %;\
    /endif

/def -F -mregexp -t'disbands (his|her|its) team and forces you to leave\.$' team_disband = \
    /set leader none %;\
    /set assisted 0 %;\
    /set assisting 0 %;\
    /if (team) \
        /team %;\
    /endif %;\
    /if (!boarding) \
        /boarding %;\
    /endif %;\
    
/def -F -mregexp -t'^You assist' you_assist = \
    /set assisted 1 %;\
    /repeat -3 1 /set assisted 0

/def -F -mregexp -t'(attacks|turns to attack|assists|rises from the shadows|springs forward in a sudden attack)' team_assist = \
    /if (team) \
        /check_assist %;\
    /endif

/def -F -mregexp -t'died\.$' team_killed = \
    /if (team) \
        /repeat -3 1 /check_assist %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) asks you: (.*)' leader_asks = \
    /if (leader_ask & (leader =~ {P1})) \
        /send %{P2} %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) asks you: smith' leader_smith = \
    /if (leader_ask & (leader =~ {P1})) \
        fixme %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) asks you: swap' leader_swap = \
    /if (leader_ask & (leader =~ {P1})) \
        /send unwield all %;\
        /send draw %;\
        /send wield all %;\
        /send sheathe unwielded weapon %;\
    /endif

/def -F -mregexp -t'^Suddenly you notice that (.*) is not here anymore\!$' team_kabal_curtain = \
    /if (team & (leader =~ {P1})) \
        /send enter curtain %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) swims up and rubs the left eye' team_thalassia = \
    /if (team & (leader =~ {P1})) \
        /send rub left eye %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) swims up to the gates\.$'  team_thalassia2 = \
    /if (team & (leader =~ {P1})) \
        /send enter gates %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*swimming across the river\.$' team_river = \
    /if (team & (leader =~ {P1})) \
        /send swim river %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) vanishes into a hidden exit somewhere\.$' team_tunnel = \
    /if (team & (leader =~ {P1})) \
        /send exa bushes %;\
        /send enter tunnel %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*climbing into the swift sailing ship\.$' team_ship = \
    /if (team & (leader =~ {P1})) \
        /send board ship %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs out onto the boulders nearing the falls\.$' team_waterfall = \
    /if (team & (leader =~ {P1})) \
        /send climb boulder %;\
        /send enter waterfall %;\
        /send climb up %;\
        /send climb up %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) jumps into the falls and disappears into the water\!$' team_waterfall2 = \
    /if (team & (leader =~ {P1})) \
        /send enter waterfall %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs up the cliff' team_climb = \
    /if (team & (leader =~ {P1})) \
        /send climb up %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) crawls into the crack\.$' team_crack = \
    /if (team & (leader =~ {P1})) \
        /send crawl into crack %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*into the bushes\.$' team_bush = \
    /if (team & (leader =~ {P1})) \
        /send enter bushes %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) gets down on all fours and climbs into the bushes\.$' team_bush2 = \
    /if (team & (leader =~ {P1})) \
        /send enter bushes %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*through the bush\.$' team_bush3 = \
    /if (team & (leader =~ {P1})) \
        /send enter bush %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) wades his way through the bushes\.$' team_angmar = \
    /if (team & (leader =~ {P1})) \
        /send enter bushes %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) walks through the gap' team_gap = \
    /if (team & (leader =~ {P1})) \
        /send enter gap %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*climbing up the big rock\.$' team_rock = \
    /if (team & (leader =~ {P1})) \
        /send climb rock %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) falls into a dark hole and disappears' team_barrow = \
    /if (team & (leader =~ {P1})) \
        /send dig %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*climbing up the swing\.$' team_swing = \
    /if (team & (leader =~ {P1})) \
        /send climb swing %;\
        /send climb trunk %;\
        /send jump roof %;\
        /send crawl west tree %;\
        /send jump west tree %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs the ladder and disappears' team_ladder = \
    /if (team & (leader =~ {P1})) \
        /send climb ladder %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) pushes on the doors' team_doors = \
    /if (team & (leader =~ {P1})) \
        /send push door %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*up the mizzen-mast' team_mast = \
    /if (team & (leader =~ {P1})) \
        /send climb mizzen-mast %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) boards the basket' team_basket1 = \
    /if (team & (leader =~ {P1})) \
        /send enter basket %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaps out of the basket' team_basket2 = \
    /if (team & (leader =~ {P1})) \
        /send out %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*entering the tent\.$' team_tent = \
    /if (team & (leader =~ {P1})) \
        /send enter tent %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves by climbing up the cliff\.$' team_cliff = \
    /if (team & (leader =~ {P1})) \
        /send climb cliff %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) scrambles up the cliff\.$' team_cliff2 = \
    /if (team & (leader =~ {P1})) \
        /send climb cliff %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) skillfully climbs down the sheer rock cliff' team_cliff3 = \
    /if (team & (leader =~ {P1})) \
        /send unwield all %;\
        /send climb down %;\
        /send wield all %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) ascends the cliff' team_cliff4 = \
    /if (team & (leader =~ {P1})) \
        /send unwield all %;\
        /send climb cliff %;\
        /send wield all %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*climbing up the face of the cliff\.$' team_cliff5 = \
    /if (team & (leader =~ {P1})) \
        /send climb up %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*climbing down the face of the cliff\.$' team_cliff6 = \
    /if (team & (leader =~ {P1})) \
        /send climb down %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) enters a small opening\.$' team_opening = \
    /if (team & (leader =~ {P1})) \
        /send enter opening %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) crawls under the rock\.$' team_opening2 = \
    /if (team & (leader =~ {P1})) \
        /send squeeze through opening %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*into the (outside world|dark cave)\.$' team_icewall = \
    /if (team & (leader =~ {P1})) \
        /send push through snow %;\
        /send pass carefully through stalagmites %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*through the fissure\.$' team_icewall2 = \
    /if (team & (leader =~ {P1})) \
        /send pass carefully through stalagmites %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) slides down in the opening between the sharp stones, and disappears into the darkness\.$' team_terel_castle = \
    /if (team & (leader =~ {P1})) \
        /send enter hole %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs out of the narrow hole in the ceiling\.$' team_terel_castle2 = \
    /if (team & (leader =~ {P1})) \
        /send enter hole %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs up the trunk of the oak\.$' quali_oak_up = \
    /if (team & (leader =~ {P1})) \
        /send climb oak %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs down to the ground\.$' quali_oak_down = \
    /if (team & (leader =~ {P1})) \
        /send climb down %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*climbing into (.*)\.$' bloodsea_board = \
    /if (team & (leader =~ {P1})) \
        /send board %{P2} %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) grabs a rope .* and swings across to the (.*),' bloodsea_board_2 = \
    /if (team & (leader =~ {P1})) \
        /send board %{P2} %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) climbs the ladder\.$' bloodsea_ladder_up = \
    /if (team & (leader =~ {P1})) \
        /send climb ladder %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*down the ladder and into the waters of Horned Bay\.$' bloodsea_ladder_down = \
    /if (team & (leader =~ {P1})) \
        /send climb down %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) starts to search forest\.$' mithas_hut = \
    /if (team & (leader =~ {P1})) \
        /repeat -5 1 /send search forest %;\
    /endif

/def -F -mregexp -t'^You manage to find a small path through the undergrowth leading south, towards a small wooden hut\.$' mithas_hut_2 = \
    /if (team) \
        /send south %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) .* into the surf and swims out into the Bloodsea of Istar\.$' bloodsea_swim_south = \
    /if (team & (leader =~ {P1})) \
        /send swim south %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) .* swims to the beach\.$' bloodsea_swim_north = \
    /if (team & (leader =~ {P1})) \
        /send swim north %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) .*enters a dark opening in the side of the tower\.$' bloodsea_tower = \
    /if (team & (leader =~ {P1})) \
        /send enter tower %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) leaves .*into the dwarven' dwarven_tomb = \
    /if (team & (leader =~ {P1})) \
        /send enter tomb %;\
    /endif

/def -F -mregexp -t'>? ?([A-Z][a-z]*) .*enters the doorway' frost_giant = \
    /if (team & (leader =~ {P1})) \
        /send enter doorway %;\
    /endif
