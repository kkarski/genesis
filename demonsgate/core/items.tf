;
; Items
;

; variables

/set coins 1
/set gems 0
/set light 0
/set pack 0
/set lootnum 1
/set lootvar all

; aliases

/alias g /send get all
/alias ga /send get armours
/alias gc /send get coins
/alias gg /send get gems
/alias gw /send get weapons
/alias gac /send get armours from corpse
/alias gcc /send get coins from corpse
/alias ggc /send get gems from corpse
/alias gwc /send get weapons from corpse

/alias la /send light all
/alias lt /send light torch
/alias ll /send light lamp
/alias ex /send extinguish all
/alias et /send extinguish torch
/alias el /send extinguish lamp
/alias dt /send drop burnt out torch

/alias op /send open pack
/alias cp /send close pack
/alias fp /send fill pack
/alias ep /send empty pack
/alias gp /send get %{*} from pack
/alias pp /send put %{*} in pack
/alias pack op %; fp %; cp
/alias unpack op %; ep %; cp

/alias sa \
    /if (pack) \
        unpack %;\
    /endif %;\
    /send sell all

/alias dep /send deposit %{*}
/alias wd /send withdraw %{*}
/alias min /send minimize coins
/alias dc dep gold%;min%;dep platinum

/alias bro /send drop broken items

/alias loot /loot %{*}

; definitions

/def loot = \
    /set lootnum %{1-1} %;\
    /set lootvar %{-1-all} %;\
    /e @ looting %lootvar from %lootnum corpse(s) %;\
    /if (lootnum == 1) \
        /send get %{lootvar} from corpse %;\
    /elseif (lootnum == 2) \
		/send get %{lootvar} from corpse %;\
        /send get %{lootvar} from second corpse %;\
    /elseif (lootnum == 3) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
    /elseif (lootnum == 4) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
    /elseif (lootnum == 5) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
    /elseif (lootnum == 6) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
    /elseif (lootnum == 7) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
		/send get %{lootvar} from seventh corpse %;\
    /elseif (lootnum == 8) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
		/send get %{lootvar} from seventh corpse %;\
		/send get %{lootvar} from eighth corpse %;\
    /elseif (lootnum == 9) \
		/send get %{lootvar} from corpse %;\
		/send get %{lootvar} from second corpse %;\
		/send get %{lootvar} from third corpse %;\
		/send get %{lootvar} from fourth corpse %;\
		/send get %{lootvar} from fifth corpse %;\
		/send get %{lootvar} from sixth corpse %;\
		/send get %{lootvar} from seventh corpse %;\
		/send get %{lootvar} from eighth corpse %;\
		/send get %{lootvar} from ninth corpse %;\
    /else \
        /send get %{lootvar} from corpse %;\
    /endif %;\
	/if (pack) \
        pack %;\
    /endif

/def coins = \
    /if (coins) \
        /set coins 0 %;\
     	/e @ automatic coins looting OFF %;\
    /else \
        /set coins 1 %;\
		/e @ automatic coins looting ON %;\
    /endif

/def gems = \
    /if (gems) \
        /set gems 0 %;\
        /e @ automatic gems looting OFF %;\
    /else \
        /set gems 1 %;\
		/e @ automatic gems looting ON %;\
    /endif

/def pack = \
    /if (pack) \
        /set pack 0 %;\
        /e @ automatic packing OFF %;\
    /else \
        /set pack 1 %;\
        /e @ automatic packing ON %;\
    /endif

/def light = \
    /if (light) \
        /set light 0 %;\
        /e @ automatic light handling OFF %;\
    /else \
        /set light 1 %;\
        /e @ automatic light handling ON %;\
    /endif

/def ireset = \
    /e @ resetting item variables %;\
    /set coins 1 %;\
    /set gems 0 %;\
    /set light 0 %;\
    /set pack 0 %;\
    /set lootnum 1 %;\
    /set lootvar all %;\
    /set trader 1

; triggers

/def -F -mregexp -t'^You light .*\.$' l_light = \
    /if (!light) \
        /light %;\
    /endif

/def -F -mregexp -t'^You extinguish .*\.$' ex_light = \
    /if (light) \
        /light %;\
    /endif

/def -F -mregexp -t'^Your .* flickers briefly\. It seems to be running out of oil\.$' fill_lamp = \
    /if (light) \
        /send refill lamp with oil %;\
    /endif

/def -F -mregexp -t'^You fill .* with all the lamp-oil in the flask\.$' empty_flask = \
    /if (light) \
        /send drop empty flask %;\
    /endif

/def -F -mregexp -t'^You wear the .* *(ack|ag) \(open\) on your (back|right shoulder)\.$' wear_open_pack = \
    /if (!pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You wear the .* *(ack|ag) on your (back|right shoulder)\.$' wear_pack = \
    /if (!pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You already wear the .* *(ack|ag)\.$' pack_there = \
    /if (!pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You remove the .* *(ack|ag)\.$' remove_pack = \
    /if (pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You remove the .* *(ack|ag) \(open\)\.$' remove_open_pack = \
    /if (pack) \
        /pack %;\
    /endif

/def -F -mregexp -t'^You get .* broken' drop_broken = \
    /send drop broken items %;\
    /e @ dropping broken items
