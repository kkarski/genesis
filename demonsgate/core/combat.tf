;
; Combat
;

; variables

/set atk 0
/set atkvictim none
/set kills 0
/set player_killing 0

; aliases

/alias comp /send compare stats with %{*}
/alias ee /send exa enemy

/alias k \
    /if ({*} =~ none) \
        /send kill %atkvictim %;\
    /else \
        /send kill %{*} %;\
        /set atkvictim %{*} %;\
    /endif

/alias ka \
    k %{*} %;\
    /send kill second %atkvictim %;\
    /send kill third %atkvictim %;\
    /send kill fourth %atkvictim %;\
    /send kill fifth %atkvictim

/alias bc /e @ your bodycount is at %kills

; definitions

/def atk = \
    /if (atk) \
        /set atk 0 %;\
        /e @ auto-killing OFF %;\
    /else \
        /set atk 1 %;\
        /e @ auto-killing ON %;\
    /endif

/def creset = \
    /e @ resetting combat variables %;\
    /set atk 0 %;\
    /set atkvictim none %;\
    /set kills 0

; triggers

/def -F -mregexp -t'attacks you\!$' defend_msg = \
    /echo -aCred <<< ATTACK >>>

/def -F -PCred -mregexp -t'>? ?([A-Z][a-z]*) killed .*\.$' kill_check = \
    /if ({P1} =~ "You") \
        /set kills $[kills+1] %;\
        /e @ your bodycount has reached %kills %;\
        /if (looter & (coins | gems)) \
            /send lc loot %;\
        /else \
            /if (coins) \
                /send get coins from corpse %;\
            /endif %;\
            /if (gems) \
                /send get gems from corpse %;\
            /endif %;\
        /endif %;\
        /if (atk) \
            k %atkvictim %;\
        /endif %;\
    /endif
