;
; Travel
;

; variables

/set boarding 1

; aliases

/alias t /send enter tent
/alias kn /send knock on gate
/alias sr /send swim river
/alias bo buy ticket %; /send board ship
/alias ds /send disembark
/alias eb /send enter bushes

; definitions

/def boarding = \
    /if (boarding) \
        /set boarding 0 %;\
     	/e @ automatic boarding OFF %;\
    /else \
        /set boarding 1 %;\
        /e @ automatic boarding ON %;\
    /endif

; triggers

/def -Fp1 -mregexp -t'navigates in\.$' autoboard = \
    /if (boarding) \
        /repeat -2 1 bo %;\
    /endif

/def -Fp1 -mregexp -t'^A (large trading vessel|fast passenger sloop) arrives' autoboard_2 = \
    /if (boarding) \
        /repeat -2 1 bo %;\
    /endif

/def -Fp1 -mregexp -t'just came in\.$' autoboard_old = \
    /if (boarding) \
        /repeat -2 1 bo %;\
    /endif

/def -Fp1 -mregexp -t'Disembark\!' autodisembark = \
    /if (boarding) \
        /send disembark %;\
    /endif

/def -Fp1 -mregexp -t'disembark\!\!' autodisembark_2 = \
    /if (boarding) \
        /send disembark %;\
    /endif

/def -F -mregexp -t'^Knock on what\? The gates\?$' enter_gates = \
    /send knock on gates

/def -F -mregexp -t'^The.*door is closed\.$' door_closed = \
    /send open door
