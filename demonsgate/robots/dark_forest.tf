;
; Herbing in Dark Forest
;

/set df_script 0
/set df_step 0
/set df_herbs_found 0

; aliases

/alias df_reset \
    /e @ resetting Dark Forest script %;\
    /set df_script 0 %;\
    /set df_step 0 %;\
    /set df_herbs_found 0

/alias df_start \
    /set df_script 1 %;\
    nw %; n %; n %; n %; n %; n %; e%;\
    df_continue

/alias df_end \
    /e @ Dark Forest end (found %df_herbs_found herbs) %;\
    /repeat -1 1 df_reset %;\
    
/alias df_continue \
    /e @ step %df_step of 57 %;\
    /if (df_step == 0) \
        /e @ Dark Forest start %;\
        hs %;\
    /elseif (df_step == 1) \
        s %;\
        ne %;\
        hs %;\
    /elseif (df_step == 2) \
        s %;\
        ne %;\
        hs %;\
    /elseif (df_step == 3) \
        se %;\
        hs %;\
    /elseif (df_step < 8) \
        w %;\
        hs %;\
    /elseif (df_step == 8) \
        sw %;\
        hs %;\
    /elseif (df_step < 16) \
        e %;\
        hs %;\
    /elseif (df_step == 16) \
        nw %;\
        hs %;\
    /elseif (df_step == 17) \
        s %;\
        s %;\
        hs %;\
    /elseif (df_step < 25) \
        w %;\
        hs %;\
    /elseif (df_step == 25) \
        s %;\
        hs %;\
    /elseif (df_step < 36) \
        e %;\
        hs %;\
    /elseif (df_step == 36) \
        nw %;\
        hs %;\
    /elseif (df_step == 37) \
        se %;\
        se %;\
        hs %;\
    /elseif (df_step < 47) \
        w %;\
        hs %;\
    /elseif (df_step == 47) \
        e %;\
        e %;\
        s %;\
        hs %;\
    /elseif (df_step < 54) \
        e %;\
        hs %;\
    /elseif (df_step == 54) \
        w %;\
        w %;\
        w %;\
        sw %;\
        hs %;\
    /elseif (df_step < 57) \
        w %;\
        hs %;\
    /elseif (df_step == 57) \
        nw %;\
        /repeat -1 1 df_end %;\
    /else \
        df_reset %;\
    /endif

; triggers

/def -F -mregexp -t'^You search everywhere, but find no herbs\.$' df_no_herbs = \
    /if (df_script == 1) \
        /set df_step $[df_step+1] %;\
        /repeat -1 1 df_continue %;\
    /endif

/def -F -mregexp -t'^Your search reveals nothing special\.$' df_no_herbs_2 = \
    /if (df_script == 1) \
        /set df_step $[df_step+1] %;\
        /repeat -1 1 df_continue %;\
    /endif

/def -F -mregexp -t'^You find .*\!$' df_herb_found = \
    /if (df_script == 1) \
        /set df_herbs_found $[df_herbs_found+1] %;\
    /endif
