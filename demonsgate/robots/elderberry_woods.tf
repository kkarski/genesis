;
; Herbing in Elderberry Woods
;

/set ew_script 0
/set ew_step 0
/set ew_herbs_found 0

; aliases

/alias ew_reset \
    /e @ resetting Elderberry Woods script %;\
    /set ew_script 0 %;\
    /set ew_step 0 %;\
    /set ew_herbs_found 0

/alias ew_start \
    /set ew_script 1 %;\
    n %; nw %; nw %; w %; nw %; w %;\
    ew_continue

/alias ew_end \
    /e @ Elderberry Woods end (found %ew_herbs_found herbs) %;\
    /repeat -1 1 ew_reset %;\
    
/alias ew_continue \
    /e @ step %ew_step of 58 %;\
    /if (ew_step == 0) \
        /e @ Elderberry Woods start %;\
        hs %;\
    /elseif (ew_step == 1) \
        ne %;\
        hs %;\
    /elseif (ew_step == 2) \
        s %;\
        hs %;\
    /elseif (ew_step < 8) \
        e %;\
        hs %;\
    /elseif (ew_step == 8) \
        se %;\
        hs %;\
    /elseif (ew_step < 14) \
        w %;\
        hs %;\
    /elseif (ew_step == 14) \
        e %;\
        se %;\
        hs %;\
    /elseif (ew_step < 18) \
        e %;\
        hs %;\
    /elseif (ew_step == 18) \
        s %;\
        hs %;\
    /elseif (ew_step < 21) \
        w %;\
        hs %;\
    /elseif (ew_step == 21) \
        s %;\
        e %;\
        e %;\
        hs %;\
    /elseif (ew_step < 32) \
        w %;\
        hs %;\
    /elseif (ew_step == 32) \
        n %;\
        hs %;\
    /elseif (ew_step < 39) \
        e %;\
        hs %;\
    /elseif (ew_step == 39) \
        w %;\
        nw %;\
        hs %;\
    /elseif (ew_step < 43) \
        w %;\
        hs %;\
    /elseif (ew_step == 43) \
        sw %;\
        s %;\
        se %;\
        hs %;\
    /elseif (ew_step < 52) \
        e %;\
        hs %;\
    /elseif (ew_step == 52) \
        w %;\
        sw %;\
        hs %;\
    /elseif (ew_step < 58) \
        w %;\
        hs %;\
    /elseif (ew_step == 58) \
        e %;\
        e %;\
        e %;\
        e %;\
        e %;\
        ne %;\
        n %;\
        /repeat -1 1 ew_end %;\
    /else \
        ew_reset %;\
    /endif

/def ew_next_step = \
    /if (ew_script == 1) \
        /set ew_step $[ew_step+1] %;\
        /repeat -1 1 ew_continue %;\
    /endif

; triggers

/def -F -mregexp -t'^You search everywhere, but find no herbs\.$' ew_no_herbs = \
    /ew_next_step

/def -F -mregexp -t'^Your search reveals nothing special\.$' ew_no_herb = \
    /ew_next_step

/def -F -mregexp -t'^You find .*\!$' ew_herb_found = \
    /if (ew_script == 1) \
        /set ew_herbs_found $[ew_herbs_found+1] %;\
    /endif
