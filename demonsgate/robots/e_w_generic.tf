;
; Generic Herbing East-West Script
;

/set combing_script 0
/set comb_herbs_found 0
/set e_w_dir none
/set n_s_dir none
/set opposite_dir none

; aliases

/alias combscript \
    /if ({*} =~ "") \
        combreset %;\
    /else \
        /set combing_script 1 %;\
        /set e_w_dir %{1} %;\
        /set n_s_dir %{2} %;\
        /check_opposite_dir %;\
        comb_continue %;\
    /endif

/alias combreset \
    /e @ found %comb_herbs_found herbs %;\
    /e @ resetting combing script %;\
    /set combing_script 0 %;\
    /set comb_herbs_found 0 %;\
    /set e_w_dir none %;\
    /set n_s_dir none %;\
    /set opposite_dir none

/alias comb_continue \
    /e @ going %e_w_dir %;\
    /send %{e_w_dir}

; macros

/def check_opposite_dir = \
    /if (e_w_dir =~ "east") \
        /set opposite_dir west %;\
    /else \
        /set opposite_dir east %;\
    /endif

/def reverse_dir = \
    /check_opposite_dir %;\
    /set e_w_dir %{opposite_dir} %;\
    /check_opposite_dir

; triggers

/def -F -mregexp -t'^There .* obvious exit?.\:' comb_found_exit = \
    /if (combing_script) \
        hs %;\
    /endif

/def -F -mregexp -t'^There is no obvious exit (.*)\.$' comb_no_exit = \
    /if (combing_script) \
        /if ({P1} =~ "east" | {P1} =~ "west") \
            /let tempdir $[strcat(substr({n_s_dir}, 0, 1), substr({e_w_dir}, 0,1))] %;\
            /e @ trying %tempdir %;\
            /send %{tempdir} %;\
            /repeat -3 1 /reverse_dir %;\
        /elseif ({P1} =~ "southeast" | {P1} =~ "northeast" | {P1} =~ "southwest" | {P1} =~ "northwest") \
            /let tempdir $[substr({n_s_dir}, 0, 1)] %;\
            /e @ trying %tempdir %;\
            /send %{tempdir} %;\
        /elseif ({P1} =~ "south" | {P1} =~ "north") \
            /let tempdir $[strcat(substr({n_s_dir}, 0, 1), substr({opposite_dir}, 0, 1))] %;\
            /e @ trying %tempdir %;\
            /send %{tempdir} %;\
        /endif %;\
    /endif
    

/def -F -mregexp -t'^You search everywhere, but find no herbs\.$' comb_no_herbs = \
    /if (combing_script) \
        /repeat -1 1 comb_continue %;\
    /endif

/def -F -mregexp -t'^Your search reveals nothing special\.$' comb_no_herbs_2 = \
    /if (combing_script) \
        /repeat -1 1 comb_continue %;\
    /endif

/def -F -mregexp -t'^You find .*\!$' comb_herb_found = \
    /if (combing_script) \
        /set comb_herbs_found $[comb_herbs_found+1] %;\
    /endif
