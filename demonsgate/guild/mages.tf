;
; Mage Guild
;

; variables

/set mupkeep 1
/set mcontainer pouch

; aliases

/alias mo /send open %mcontainer
/alias mc /send close %mcontainer
/alias mget \
    mo %;\
    /send get %{*} from %mcontainer %;\
    mc
/alias mput \
    mo %;\
    /send put %{*} in %mcontainer %;\
    mc

; triggers

/def -F -PCred -mregexp -t'^Your spell fails\.$'

/def -F -PCred -mregexp -t'^You don\'t have the required components for .* spell\.$'

/def -F -PCcyan -mregexp -t'^You start to concentrate upon the spell\.$'
