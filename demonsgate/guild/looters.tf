;
; Looters Club
;

; variables

/set looter 1

; aliases 

/alias lco lc open
/alias lcc lc close
/alias lput lco %; lc fill %; lcc
/alias lget lco %; lc empty %; lcc

/alias buy \
    /if (looter) \
        lget %;\
        /send buy %{*} %;\
        lput %;\
    /else \
        /send buy %{*} %;\
    /endif
