;
; Black Company
;

; variables

/set mattacking 1

; aliases

/alias x /send mattack %{*}

; definitions

/def mattacking = \
    /if (mattacking) \
        /set mattacking 0 %;\
        /e @ automatic mattack OFF %;\
    /else \
        /set mattacking 1 %;\
        /e @ automatic mattack ON %;\
    /endif

/def mattack_check = \
    /if (mattacking) \
        /set mattacking 0 %;\
        /send mattack %;\
        /repeat -2 1 /set mattacking 1 %;\
    /endif

/def mattack_msg = \
        /echo %;\
        /echo -aCcyan < mattack: %{*} > %;\
        /echo

; triggers

/def -F -mregexp -t'^You (attack|assist|turn to attack)' mattack_attack = \
    /mattack_check

/def -F -mregexp -t'attacks you\!$' mattack_defend = \
    /repeat -1 1 /mattack_check

/def -F -PBCwhite -mregexp -t'^You (quickly dodge a blow and prepare yourself for a counter attack|start to look for an opening in your opponents defences)\.$' mattack_prepare = \
    /if (mattacking) \
        /set mattacking 0 %;\
        /repeat -1 1 /set mattacking 1 %;\
    /endif

/def -F -mregexp -t'^You feel ready to attack again\.$' = \
    /mattack_check

/def -F -mregexp -t'^You are already preparing another attack\.$' = \
    /set mattacking 1

/def -F -mregexp -t'^Seeing that your target is gone you cancel your attack\.$' = \
    /set mattacking 1

/def -F -mregexp -t'^Attack whom\?$' = \
    /set mattacking 1

/def -F -t'*breath return*' = /send mattack
/def -F -t'*releases*grip on you*' = /send mattack
/def -F -t'*recovered your senses*' = /send mattack
/def -F -t'*You regain consciousness*' = /send mattack
/def -F -t'*You recover your balance again*' = /send mattack
/def -F -t'*You no longer feel paralyzed*' = /send mattack
/def -F -t'*You are unable to attack your target*' = /send mattack

; messages

/def -F -mregexp -t'but miss\.$' = \
    /mattack_msg miss
/def -F -mregexp -t'^You strike a mighty blow' = \
    /mattack_msg glance off
/def -F -mregexp -t'^You poke .* in (his|her|its)' = \
    /mattack_msg weak
/def -F -mregexp -t'^You swing your .* against' = \
    /mattack_msg weak
/def -F -mregexp -t'^You see a big gap' = \
    /mattack_msg moderate
/def -F -mregexp -t'^You grin evilly as you swing' = \
    /mattack_msg moderate
/def -F -mregexp -t'^With a nice (swing|thrust) of your' = \
    /mattack_msg moderate
/def -F -mregexp -t'^You make a quick (swing|thrust) with your' = \
    /mattack_msg severe
/def -F -mregexp -t'^You make a brutal chop' = \
    /mattack_msg severe
/def -F -mregexp -t'^With a strike that would have impressed your old teacher' = \
    /mattack_msg severe
/def -F -mregexp -t'^With a beautiful (swing|thrust) of your' = \
    /mattack_msg kill
/def -F -mregexp -t'^You deal a brutal blow with your' = \
    /mattack_msg kill
